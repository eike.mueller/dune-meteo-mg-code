dnl -*- autoconf -*-
# Macros needed to find geometricmultigrid and dependent libraries.  They are called by
# the macros in ${top_src_dir}/dependencies.m4, which is generated by
# "dunecontrol autogen"

# Additional checks needed to build geometricmultigrid
# This macro should be invoked by every module which depends on geometricmultigrid, as
# well as by geometricmultigrid itself
AC_DEFUN([GEOMETRICMULTIGRID_CHECKS])

# Additional checks needed to find geometricmultigrid
# This macro should be invoked by every module which depends on geometricmultigrid, but
# not by geometricmultigrid itself
AC_DEFUN([GEOMETRICMULTIGRID_CHECK_MODULE],
[
  DUNE_CHECK_MODULES([geometricmultigrid],[geometricmultigrid/geometricmultigrid.hh])
])
