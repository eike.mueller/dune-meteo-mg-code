/*** COPYRIGHT STATEMENT ************************************ 
 *
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2013]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  This code has been developed by
 *  - Andreas Dedner (University of Warwick)
 *  - Eike Mueller (University of Bath)
 *
 *** COPYRIGHT STATEMENT ************************************/

#ifndef GENERALPARAMETERS_HH
#define GENERALPARAMETERS_HH
/* ********************************************************** *
 *
 *  general parameters
 *
 * ********************************************************** */
#include "parameters.hh"
#include<dune/grid/io/file/vtk/common.hh>

class GeneralParameters : public Parameters {
 public:
  // Constructor
  GeneralParameters() :
                    Parameters("general"),
                    savetovtk_(false),
                    vtkoutputtype_(Dune::VTK::ascii)
                     {
      addKey("savetovtk",Bool);
      addKey("vtkoutputtype",String);
      addKey("timinglogfile",String);
    }

  //
  int readFile(const std::string filename) {
    int readSuccess = Parameters::readFile(filename);
    if (!readSuccess) {
      savetovtk_ = contents["savetovtk"]->getBool();
      timinglogfile_ = contents["timinglogfile"]->getString();
      std::string vtkType = contents["vtkoutputtype"]->getString();
      if (vtkType == "ascii") {
        vtkoutputtype_ = Dune::VTK::ascii;
      } else if (vtkType == "appendedbase64") {
        vtkoutputtype_ = Dune::VTK::appendedbase64;
      }
    }
    return readSuccess;
  }

  // Return data
  bool savetovtk() const { return savetovtk_; }
  std::string timinglogfile() const { return timinglogfile_; }
  Dune::VTK::OutputType vtkoutputtype() const { return vtkoutputtype_; }

 private:
  bool savetovtk_;
  std::string timinglogfile_;
  Dune::VTK::OutputType vtkoutputtype_;
};

#endif // GENERALPARAMETERS_HH
