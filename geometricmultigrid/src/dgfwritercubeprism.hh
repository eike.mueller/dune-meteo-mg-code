/*** COPYRIGHT STATEMENT ************************************ 
 *
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2013]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  This code has been developed by
 *  - Andreas Dedner (University of Warwick)
 *  - Eike Mueller (University of Bath)
 *
 *** COPYRIGHT STATEMENT ************************************/

#ifndef DUNE_DGFWRITERCUBEPRISM_HH
#define DUNE_DGFWRITERCUBEPRISM_HH

/* *************************************************************** *
 * Modification of the dgf writer which writes out cubes and prisms
 * *************************************************************** */

#include <fstream>
#include <vector>

#include <dune/grid/common/grid.hh>
#include <dune/geometry/referenceelements.hh>

namespace Dune
{

  template< class GV >
  class DGFWriterCubePrism
  {
    typedef DGFWriterCubePrism < GV > This;

  public:
    // gridviw type
    typedef GV GridView;
    // type of underlying hierarchical grid
    typedef typename GridView::Grid Grid;

    // dimension of the grid
    static const int dimGrid = GridView::dimension;

  private:
    typedef typename GridView::IndexSet IndexSet;
    typedef typename GridView::template Codim< 0 >::Iterator ElementIterator;
    typedef typename GridView::IntersectionIterator IntersectionIterator;
    typedef typename GridView::template Codim< dimGrid >::EntityPointer   VertexPointer;

    typedef typename IndexSet::IndexType Index;

    typedef GenericReferenceElement< typename Grid::ctype, dimGrid > RefElement;
    typedef GenericReferenceElements< typename Grid::ctype, dimGrid > RefElements;


  public:

/* *************************************************************** *
 * Constructor
 * *************************************************************** */
    DGFWriterCubePrism ( const GridView &gridView )
    : gridView_( gridView )
    {}

/* *************************************************************** *
 * Write gridview to a stream
 * *************************************************************** */
    void write ( std::ostream &gridout ) const;

/* *************************************************************** *
 * Write gridview to a file
 * *************************************************************** */
    void write ( const std::string &fileName ) const;

  private:
    GridView gridView_;
  };



  template< class GV >
  inline void DGFWriterCubePrism< GV >::write ( std::ostream &gridout ) const
  {
    // set the stream to full double precision
    gridout.setf( std::ios_base::scientific, std::ios_base::floatfield );
    gridout.precision( 16 );

    const IndexSet &indexSet = gridView_.indexSet();

    // write DGF header
    gridout << "DGF" << std::endl;

    const Index vxSize = indexSet.size( dimGrid );
    std::vector< Index > vertexIndex( vxSize, vxSize );

    gridout << "%" << " Elements = " << indexSet.size( 0 ) << "  |  Vertices = " << vxSize << std::endl;

    // write all vertices into the "vertex" block
    gridout << std::endl << "VERTEX" << std::endl;
    Index vertexCount = 0;
    typedef typename ElementIterator :: Entity  Element ;
    const ElementIterator end = gridView_.template end< 0 >();
    for( ElementIterator it = gridView_.template begin< 0 >(); it != end; ++it )
    {
      const Element& element = *it ;
      const int numCorners = element.template count< dimGrid > ();
      for( int i=0; i<numCorners; ++i )
      {
        const Index vxIndex = indexSet.subIndex( element, i, dimGrid );
        assert( vxIndex < vxSize );
        if( vertexIndex[ vxIndex ] == vxSize )
        {
          vertexIndex[ vxIndex ] = vertexCount++;
          gridout << element.geometry().corner( i ) << std::endl;
        }
      }
    }
    gridout << "#" << std::endl;
    if( vertexCount != vxSize )
      DUNE_THROW( GridError, "Index set reports wrong number of vertices." );

    // write all prisms to the "prism" block
    gridout << std::endl << "PRISM" << std::endl;
    for( ElementIterator it = gridView_.template begin< 0 >(); it != end; ++it )
    {
      const Element& element = *it ;
      if( ! element.type().isPrism() )
        continue;

      std::vector< Index > vertices( 2*dimGrid );
      for( size_t i = 0; i < vertices.size(); ++i )
        vertices[ i ] = vertexIndex[ indexSet.subIndex( element, i, dimGrid ) ];

      gridout << vertices[ 0 ];
      for( size_t i = 1; i < vertices.size(); ++i )
        gridout << " " << vertices[ i ];
      gridout << std::endl;
    }
    gridout << "#" << std::endl;

    // write all cubes to the "cube" block
    gridout << std::endl << "CUBE" << std::endl;
    for( ElementIterator it = gridView_.template begin< 0 >(); it != end; ++it )
    {
      const Element& element = *it ;
      if( !element.type().isCube() )
        continue;

      std::vector< Index > vertices( 1 << dimGrid );
      for( size_t i = 0; i < vertices.size(); ++i )
        vertices[ i ] = vertexIndex[ indexSet.subIndex( element, i, dimGrid ) ];

      gridout << vertices[ 0 ];
      for( size_t i = 1; i < vertices.size(); ++i )
        gridout << " " << vertices[ i ];
      gridout << std::endl;
    }
    gridout << "#" << std::endl;

    // write all boundaries to the "boundarysegments" block
    gridout << std::endl << "BOUNDARYSEGMENTS" << std::endl;
    for( ElementIterator it = gridView_.template begin< 0 >(); it != end; ++it )
    {
      const Element& element = *it ;
      if( !it->hasBoundaryIntersections() )
        continue;

      const RefElement &refElement = RefElements::general( element.type() );

      const IntersectionIterator iend = gridView_.iend( element ) ;
      for( IntersectionIterator iit = gridView_.ibegin( element ); iit != iend; ++iit )
      {
        if( !iit->boundary() )
          continue;

        const int boundaryId = iit->boundaryId();
        if( boundaryId < 0 )
        {
          std::cerr << "Warning: Ignoring nonpositive boundary id: "
                    << boundaryId << "." << std::endl;
          continue;
        }

        const int faceNumber = iit->indexInInside();
        const unsigned int faceSize = refElement.size( faceNumber, 1, dimGrid );
        std::vector< Index > vertices( faceSize );
        for( unsigned int i = 0; i < faceSize; ++i )
        {
          const int j = refElement.subEntity( faceNumber, 1, i, dimGrid );
          vertices[ i ] = vertexIndex[ indexSet.subIndex( element, j, dimGrid ) ];
        }
        gridout << boundaryId << "   " << vertices[ 0 ];
        for( unsigned int i = 1; i < faceSize; ++i )
          gridout << " " << vertices[ i ];
        gridout << std::endl;
      }
    }
    gridout << "#" << std::endl;

    // write the name into the "gridparameter" block
    gridout << std::endl << "GRIDPARAMETER" << std::endl;
    // gridout << "NAME " << gridView_.grid().name() << std::endl;
    gridout << "#" << std::endl;

    gridout << std::endl << "#" << std::endl;
  }


  template< class GV >
  inline void DGFWriterCubePrism< GV >::write ( const std::string &fileName ) const
  {
    std::ofstream gridout( fileName.c_str() );
    write( gridout );
  }

}

#endif // #ifndef DUNE_DGFWRITERCUBEPRISM_HH
