/*** COPYRIGHT STATEMENT ************************************ 
 *
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2013]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  This code has been developed by
 *  - Andreas Dedner (University of Warwick)
 *  - Eike Mueller (University of Bath)
 *
 *** COPYRIGHT STATEMENT ************************************/

#ifndef PARAMETERS_HH
#define PARAMETERS_HH

#include<string>
#include<regex.h>
#include<iostream>
#include<dune/common/mpicollectivecommunication.hh>

/* ************************************************************** *
 * Base class representing any parameter
 * ************************************************************** */
class Parameter {
 public:
  // Constructor
  Parameter(const std::string valueString) :
    valueString_(valueString) {}

  // Return in different data types
  virtual const double getDouble() const {
    std::cerr << " Invalid data type. " << std::endl;
    return 0.0;
  };
  virtual const int getInt() const {
    std::cerr << " Invalid data type. " << std::endl;
    return 0;
  };
  virtual const std::string getString() const {
    std::cerr << " Invalid data type. " << std::endl;
    return std::string("");
  };
  virtual const bool getBool() const {
    std::cerr << " Invalid data type. " << std::endl;
    return false;
  };
  // set data
  virtual void setValue(const std::string valueString) {
  }
  virtual void broadcast(Dune::CollectiveCommunication<MPI_Comm>& comm) {
  };
 private:
  std::string valueString_;
 protected:
  static const int master_rank=0;
};

/* ************************************************************** *
 * Class representing a double parameter
 * ************************************************************** */
class doubleParameter : public Parameter {
 public:
  doubleParameter(const std::string valueString) :
    Parameter(valueString) {
      value=atof(valueString.c_str());
  }
  virtual const double getDouble() const { return value; }
  virtual void setValue(const std::string valueString) {
    value=atof(valueString.c_str());
  }
  virtual void broadcast(Dune::CollectiveCommunication<MPI_Comm>& comm) {
    comm.broadcast(&value,1,master_rank);
  }
 private:
  double value;
};

/* ************************************************************** *
 * Class representing an integer parameter
 * ************************************************************** */
class intParameter : public Parameter {
 public:
  intParameter(const std::string valueString) :
    Parameter(valueString) {
      value=atoi(valueString.c_str());
  }
  virtual const int getInt() const { return value; }
  virtual void setValue(const std::string valueString) {
    value=atoi(valueString.c_str());
  }
  virtual void broadcast(Dune::CollectiveCommunication<MPI_Comm>& comm) {
    comm.broadcast(&value,1,master_rank);
  }
 private:
  int value;
};

/* ************************************************************** *
 * Class representing a string parameter
 * ************************************************************** */
class stringParameter : public Parameter {
 public:
  stringParameter(const std::string valueString) :
    Parameter(valueString) {
      value = valueString.substr(1, valueString.size()-2);
  }
  virtual const std::string getString() const { return value; }
  virtual void setValue(const std::string valueString) {
    value = valueString.substr(1, valueString.size()-2);
  }
  virtual void broadcast(Dune::CollectiveCommunication<MPI_Comm>& comm) {
    int len = value.size();
    comm.broadcast(&len,1,master_rank);
    char *buffer=new char[len+1];
    buffer[len]=0;
    memcpy(buffer,value.c_str(),len);
    comm.broadcast(buffer,len+1,master_rank);
    value.assign(buffer);
    delete[] buffer;
  }
 private:
  std::string value;
};

/* ************************************************************** *
 * Class representing a boolean parameter
 * ************************************************************** */
class boolParameter : public Parameter {
 public:
  boolParameter(const std::string valueString) :
    Parameter(valueString) {
      value = ( ( valueString == "true" ) ||
                ( valueString == "True" ) ||
                ( valueString == "TRUE" ) ) ;

  }
  virtual const bool getBool() const { return value; }
  virtual void setValue(const std::string valueString) {
    value = ( ( valueString == "true" ) ||
              ( valueString == "True" ) ||
              ( valueString == "TRUE" ) ) ;
  }
  virtual void broadcast(Dune::CollectiveCommunication<MPI_Comm>& comm) {
    comm.broadcast(&value,1,master_rank);
  }
 private:
  bool value;
};

/* ************************************************************** *
 *
 * Base class for parameters which can be read from file.
 *
 * The file has to have the following structure:
 *
 * sectionname:
 *   key = value
 *   key = value
 *   ...
 * sectionname:
 *   key = value
 *   key = value
 *   ...
 *
 * Blank lines and lines starting with '#' are ignored.
 *
 * ************************************************************** */
class Parameters {
  public:
   enum Datatype {Integer, Double, String, Bool};
   friend std::ostream& operator<<(std::ostream& output, const Parameters& p);

/* ************************************************************** *
 * Constructor.
 * ************************************************************** */
    Parameters(const std::string section) :
      comm_(MPI_COMM_WORLD), section_(section) {
      regcomp(&regexKeyword_,
              "^ *([a-zA-Z]+): *(#.*)?$",REG_EXTENDED);
      regcomp(&regexComment_,
              "^ *(#.*)?$",REG_EXTENDED);
      regcomp(&regexKeyValueAny_,
              "^ *([a-zA-Z0-9]+) *= *([0-9]+|([+-]?[0-9]*\\.?[0-9]*([Ee][+-]?[0-9]+)?|(true|True|TRUE|false|False|FALSE)|[\'\"].*[\'\"])) *(#.*)?$", REG_EXTENDED);
      regcomp(&regexKeyValueInt_,
              "^ *([a-zA-Z0-9]+) *= *([0-9]+) *(#.*)?$", REG_EXTENDED);
      regcomp(&regexKeyValueDouble_,"^ *([a-zA-Z0-9]+) *= *([+-]?[0-9]*\\.?[0-9]*([Ee][+-]?[0-9]+)?) *(#.*)?$", REG_EXTENDED);
      regcomp(&regexKeyValueBool_,
              "^ *([a-zA-Z0-9]+) *= *(true|True|TRUE|false|False|FALSE) *(#.*)?$", REG_EXTENDED);
      regcomp(&regexKeyValueString_,
              "^ *([a-zA-Z0-9]+) *= *([\'\"].*[\'\"]) *(#.*)?$", REG_EXTENDED);
    }

/* ************************************************************** *
 * Read from stream (go back to beginning of stream) and return
 * 0 if everything is ok, 1 otherwise
 * ************************************************************** */
    int readFile(const std::string filename) {
      // reset stream
      if (comm_.rank() == master_rank) {
        std::ifstream is;
        is.open(filename.c_str());
        // read from stream until keyword is found
        bool lineValid;
        char line[256];
        bool parseSection = false;
        bool foundSection = false;
        // initialise found list to zero
        std::map<std::string,bool> found;
        typedef std::map<std::string,Datatype>::iterator Iterator;
        for (Iterator it = keywords.begin(); it!= keywords.end();++it) {
          found.insert(std::pair<std::string,bool>((*it).first,false));
        }
        while (is) {
          lineValid=false;
          is.getline(line,256);
          std::string sline(line);
          // Comment
          if (!regexec(&regexComment_,line,0,0,0)) {
            if (verbose > 0)
              std::cout << " >>> COMMENT: " << line << std::endl;
            lineValid = true;
          }
          // Section Keyword
          regmatch_t matchKeyword[3];
          if (!regexec(&regexKeyword_,line,3,matchKeyword,0)) {
            std::string section = sline.substr(matchKeyword[1].rm_so,
                                         matchKeyword[1].rm_eo);
            if (verbose > 0)
              std::cout << " >>> SECTION: \'" << section << "\'"<< std::endl;
            if (section == section_) {
              // Start parsing section
              parseSection = true;
              foundSection = true;
            } else {
              // Have reached a different section, stop parsing
              parseSection = false;
            }
            lineValid = true;
          }
          // Found a key-value pair
          regmatch_t matchKeyValueAny[4];
          if (!regexec(&regexKeyValueAny_,line,4,matchKeyValueAny,0) ) {
            // Check if this parameter is in the list of keywords
            std::string key = sline.substr(matchKeyValueAny[1].rm_so,
                                           matchKeyValueAny[1].rm_eo-
                                         matchKeyValueAny[1].rm_so);
            std::string value = sline.substr(matchKeyValueAny[2].rm_so,
                                             matchKeyValueAny[2].rm_eo-
                                             matchKeyValueAny[2].rm_so);
            if (verbose > 0)
              std::cout << " >>> KEY: \'" << key << "\' VALUE: \'"
                        << value << "\'" << std::endl;
            if (parseSection) {
              if (keywords.count(key) > 0) {
                if (!found[key]) {
                 // Integer
                  if ( (!regexec(&regexKeyValueInt_,line,0,0,0)) &&
                       (keywords[key] == Integer) ) {
                  contents[key]->setValue(value);
                // Double
                } else if ( (!regexec(&regexKeyValueDouble_,line,0,0,0)) &&
                   (keywords[key] == Double) ) {
                  contents[key]->setValue(value);
                // Bool
                } else if ( (!regexec(&regexKeyValueBool_,line,0,0,0)) &&
                   (keywords[key] == Bool) ) {
                  contents[key]->setValue(value);
                // String
                } else if ( (!regexec(&regexKeyValueString_,line,0,0,0)) &&
                   (keywords[key] == String) ) {
                  contents[key]->setValue(value);
                } else {
                  // Parameter has wrong type
                  std::cerr << " ERROR reading parameters: Parameter: \'"
                            << key << " = " << value
                            << "\' in section \'" << section_ << "\' "
                            << " has wrong type." << std::endl;
                  return 1;
                }
                found[key] = true;
              } else {
                std::cerr << " ERROR reading parameters: Duplicate parameter: \'"
                          << key << " = " << value
                          << "\' in section \'" << section_ << "\' " << std::endl;
                return 1;
              }
              // Unexpected key-value pair in section
            } else {
              std::cerr << " ERROR reading parameters: Invalid parameter: \'"
                        << key << " = " << value
                        << "\' in section \'" << section_ << "\'" << std::endl;
              return 1;
            }
          }
          lineValid = true;
        }
        if (!lineValid) {
          std::cerr << " ERROR reading parameters: Can not parse expression: \'"
                      << line
                      << "\' in section \'" << section_ << "\'" << std::endl;
          return 1;
        }
      }
        // Check whether we found the section
      if (!foundSection) {
          std::cerr << " ERROR reading parameters: Can not find section: \'"
                    << section_ << "\'" << std::endl;
          return 1;
        }
      // Check whether we have found all key-value pair of this section
      typedef std::map<std::string,bool>::iterator FIterator;
      for (FIterator it = found.begin(); it!= found.end();++it) {
        if (!(*it).second) {
          std::cerr << " ERROR reading parameters: parameter \'"
                    << (*it).first << "\' in section \'" << section_
                    << "\' has not been specified." << std::endl;
          return 1;
        }
      }
      is.close();
      }
      typedef std::map<std::string,Parameter*>::iterator Iterator;
      for (Iterator it = contents.begin(); it!= contents.end();++it) {
        it->second->broadcast(comm_);
      }
      return 0;
    }

  protected:
   void addKey(const std::string key, const Datatype datatype) {
     keywords.insert(std::pair<std::string,Datatype>(key,datatype));
     if (datatype == Integer) {
        contents[key] = new intParameter("0");
      } else if (datatype == Double) {
        contents[key] = new doubleParameter("0.0");
      } else if (datatype == String) {
        contents[key] = new stringParameter("blank");
      } else if (datatype == Bool) {
        contents[key] = new boolParameter("false");
      }
   }
   std::map<std::string,Parameter*> contents;
  private:
   Dune::CollectiveCommunication<MPI_Comm> comm_;
   std::map<std::string,Datatype> keywords;
   std::string section_;
   regex_t regexComment_;
   regex_t regexKeyword_;
   regex_t regexKeyValueAny_;
   regex_t regexKeyValueInt_;
   regex_t regexKeyValueDouble_;
   regex_t regexKeyValueBool_;
   regex_t regexKeyValueString_;
   static const bool verbose=0;
   static const int master_rank=0;
};

/* ************************************************************** *
 * Write to stream
 * ************************************************************** */

std::ostream& operator<<(std::ostream& output, const Parameters& p) {
  output << p.section_ << ":" << std::endl;
  std::map<std::string,Parameter*>::const_iterator it;
  std::map<std::string,Parameters::Datatype>::const_iterator pit;
  for (it=p.contents.begin(), pit=p.keywords.begin();
    it != p.contents.end(); ++it,++pit) {
    output << "  " << it->first << " = ";
    if (pit->second == Parameters::Integer) {
      output << (it->second)->getInt();
    } else if (pit->second == Parameters::Double) {
      output << std::scientific << (it->second)->getDouble();
    } else if (pit->second == Parameters::Bool) {
      if ((it->second)->getBool()) {
        output << "true";
      } else {
        output << "false";
      }
    } else if (pit->second == Parameters::String) {
      output << (it->second)->getString();
    }
    output << std::endl;
  }
  return output;
}

#endif // PARAMETERS_HH

