/*** COPYRIGHT STATEMENT ************************************ 
 *
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2013]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  This code has been developed by
 *  - Andreas Dedner (University of Warwick)
 *  - Eike Mueller (University of Bath)
 *
 *** COPYRIGHT STATEMENT ************************************/

#ifndef ISTLOPERATORCGJACSOLVER_HH
#define ISTLOPERATORCGJACSOLVER_HH

#include <dune/istl/bvector.hh>
#include <dune/istl/solvers.hh>
#include <dune/istl/preconditioners.hh>
#include "gridfunction.hh"
#include "istloperator.hh"

/* ********************************************************** *
 * ********************************************************** *
 *
 * ISTL operator for CG solver preconditioned with Jacobi
 *
 * ********************************************************** *
 * ********************************************************** */
template <class Op>
class IstlOperatorCGJacSolver : public IstlOperator<Op>
{
  typedef IstlOperator<Op> BaseType;
  typedef typename BaseType::DF DF;
  static const int NZ = DF::nz;
  typedef typename BaseType::Matrix Matrix;
  typedef typename Dune::BlockVector <typename DF::LocalVector> X;
public:
/* ********************************************************** *
 * Constructor
 * ********************************************************** */
  IstlOperatorCGJacSolver(const GridParameters<NZ> &grid_param,
                          const SmootherParameters &smoother_param,
                          double resreduction=1.0e-5,
                          int verbose=0,
                          int nprec=1,
                          double rho=1.0
                          )
  : BaseType(grid_param,smoother_param),
    resreduction_(resreduction),
    verbose_(verbose),
    nprec_(nprec),
    rho_(rho) {}
/* ********************************************************** *
 * Destructor
 * ********************************************************** */
  ~IstlOperatorCGJacSolver() {
    for(size_t i=0;i<invsolver_.size();++i) {
      if (invsolver_[i]) delete(invsolver_[i]);
    }
    for(size_t i=0;i<precond_.size();++i) {
      if (precond_[i]) delete(precond_[i]);
    }
    for(size_t i=0;i<linop_.size();++i) {
      if (linop_[i]) delete(linop_[i]);
    }
  }

/* ********************************************************** *
 * Solve A.u = b
 * ********************************************************** */
  virtual void solve(const DF &b, DF &u) const
  {
    int level = b.level();
    if ( !assembleMatrix(u) ) // has not yet been assembled
    {
      if (linop_.size()<level+1)
      {
        linop_.resize( level+1, 0);
        precond_.resize( level+1, 0);
        invsolver_.resize( level+1, 0);
      }
      // Create linear operator from matrix adapter
      linop_[level] = new LinOp(*matrix_[level]);
      // Create preconditioner on a given level (can use switch to change between different preconditioners...
      SeqJac *prec = new SeqJac(*matrix_[level],nprec_,rho_);
      // Create solver on a given level
      invsolver_[level] = new CGSolver(*linop_[level],
                                      *prec,
                                      resreduction_,
                                      solveSteps_,
                                      verbose_);
      precond_[level] = prec;
    }
    Dune::InverseOperatorResult res;
    DF b_tmp(b.gridView(),b.level());
    b_tmp.dofs() = b.dofs();
    invsolver_[level]->apply(u.dofs(),b_tmp.dofs(),res);
  }

  private:

  using BaseType::matrix_;
  using BaseType::solveSteps_;

  typedef typename Dune::SeqJac<Matrix,X,X> SeqJac;
  typedef typename Dune::CGSolver<X> CGSolver;

  typedef typename Dune::InverseOperator<X,X> InvSolver;
  mutable std::vector< InvSolver* > invsolver_;
  typedef typename Dune::Preconditioner<X,X> Preconditioner;
  mutable std::vector<Preconditioner* > precond_;
  typedef typename Dune::MatrixAdapter<Matrix,X,X> LinOp;
  mutable std::vector<LinOp* > linop_;
  // Target residual reduction
  double resreduction_;
  // Verbosity of CG solver
  int verbose_;
  // Number of preconditioner iterations
  int nprec_;
  // Overrelaxation parameter
  double rho_;
};
#endif
