/*** COPYRIGHT STATEMENT ************************************ 
 *
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2013]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  This code has been developed by
 *  - Andreas Dedner (University of Warwick)
 *  - Eike Mueller (University of Bath)
 *
 *** COPYRIGHT STATEMENT ************************************/
/* *************************************************************** *
 *
 * Main program for testing profiles read from netcdf file
 *
 * *************************************************************** */

#ifdef HAVE_CONFIG_H
# include "config.h"     
#endif
#include<iostream>
#include<fstream>
#include<vector>
#include<dune/common/mpihelper.hh> // An initializer of MPI
#include<dune/common/exceptions.hh> // We use exceptions
#include<dune/common/fvector.hh>
#include<dune/grid/alugrid.hh>
#include"gridfunction.hh"     
#include"generalparameters.hh"
#include"gridparameters.hh"
#include"profileparameters.hh"
#if defined UGGRID
  #include"sphericalgridgenerator.hh"
#endif // UGGRID
#include"profiles.hh"
#include"netcdffunctions.hh"
#include"modeloperator.hh"
const int nz=128;

#if defined UGGRID 
const int dim = 2;
const int dimworld = 3;
#else // defined UGGRID
const int dim = 2;
const int dimworld = 3;
#endif // defined UGGRID


/* *************************************************************** *
 * Function class for extracting a particular field
 * *************************************************************** */
enum ProfileSelector {Prof_a_hh, Prof_a_rr, Prof_a_r, Prof_b};

template <class Profile>
class Function {
  public:
    Function(const Profile& profile,
             const ProfileSelector profselect) :
      profile_(profile),
      profselect_(profselect) {};
    template <class FieldVector> 
    double operator()( const FieldVector& x) const {
      double r = sqrt(x[0]*x[0]+x[1]*x[1]+x[2]*x[2]);
      FieldVector x0;
      for (size_t i=0;i<3;++i) x0[i] = x[i]/r;
      switch (profselect_) {
        case(Prof_a_hh) : return profile_.a_hh(x,r); break;
        case(Prof_a_rr) : return profile_.a_rr(x,r); break;
        case(Prof_a_r)  : return profile_.a_r(x,r);  break;
        case(Prof_b)    : return profile_.b(x,r);    break;
      }
    }
  private:
    const Profile& profile_;
    const ProfileSelector profselect_;
};

/* *************************************************************** *
 * Function class for extracting a particular vertical field
 * *************************************************************** */
template <class Profile>
class VertFunction {
  public:
    enum Value {Minimum, Maximum, Average};
    VertFunction(const Profile& profile,
                 const ProfileSelector profselect,
                 const Value value=Average) :
      profile_(profile),
      profselect_(profselect) {
        if (value == Average) index_ = 0;
        if (value == Minimum) index_ = 1;
        if (value == Maximum) index_ = 2;
      };
    double operator()(const double r) const {
      switch (profselect_) {
        case(Prof_a_hh) : return profile_.a_hh_r(r,index_); break;
        case(Prof_a_rr) : return profile_.a_rr_r(r,index_); break;
        case(Prof_a_r)  : return profile_.a_r_r(r,index_);  break;
        case(Prof_b)    : return profile_.b_r(r,index_);    break;
      }
    }
  private:
    const Profile& profile_;
    const ProfileSelector profselect_;
    int index_;
};

/* *************************************************************** *
 * Subroutine for saving a particular 3d field and its
 * horizontal average to disk.
 * *************************************************************** */
template <class Operator>
void save_profile(Operator& op,
                  const ProfileSelector profselect,
                  const GridParameters<nz> &grid_param,
                  const std::string filename) {
  typedef typename Operator::Grid Grid;
  typedef typename Operator::ProfileType Profile;
  typedef typename Grid::LevelGridView GV;
  const Grid &grid = op.grid();
  const GV& gv = grid.levelView(grid.maxLevel());
  typename Operator::DF u(gv,grid.maxLevel());
  const Profile& profile = op.profile();
  const Function<Profile> function(profile,profselect);
  op.project(function,u);
  u.savetovtk(filename);

  // Save vertical functions
  typedef VertFunction<Profile> VF;
  const VF vertfunctionMin(profile,profselect,VF::Minimum);
  const VF vertfunctionAvg(profile,profselect,VF::Average);
  const VF vertfunctionMax(profile,profselect,VF::Maximum);
  std::ofstream file;
  file.open(filename+"_vert.dat");
  file << "Field: " << filename << std::endl;
  for (size_t k=0;k<grid_param.r().size();++k) {
    double r_vert = grid_param.r()[k];
    file << std::setprecision(12) << r_vert << " ";
    file << std::setprecision(12) << vertfunctionMin(r_vert) << " ";
    file << std::setprecision(12) << vertfunctionAvg(r_vert) << " ";
    file << std::setprecision(12) << vertfunctionMax(r_vert) << " ";
    file << std::endl;
  }
  file.close();
}

/* *************************************************************** *
 * Main program
 * *************************************************************** */
int main(int argc, char** argv)
{
  try{
    //Maybe initialize Mpi
    Dune::MPIHelper& helper = Dune::MPIHelper::instance(argc, argv);
    if(Dune::MPIHelper::isFake)
      std::cout<< "Test netCDF profile [sequential version]" << std::endl;
    else
      if (helper.rank() == 0) {
        std::cout<< "test netCDF profile running on "<< helper.size()<<" processes."<<std::endl;
      }

    if (argc != 2) {
      std::cout << "Usage: " << argv[0] << " <parameterfile>" << std::endl;
      return 0;
    }
    Dune::CollectiveCommunication<MPI_Comm> comm(MPI_COMM_WORLD);
    // Read command line parameters
    std::string parameterFilename = argv[1];

    // Read parameters from file
    GeneralParameters general_param;
    GridParameters<nz> grid_param;
    ProfileParameters profile_param;
    SmootherParameters smoother_param;

    if (general_param.readFile(parameterFilename)) return 1;
    if (grid_param.readFile(parameterFilename)) return 1;
    if (profile_param.readFile(parameterFilename)) return 1;
    if (smoother_param.readFile(parameterFilename)) return 1;

    if (helper.rank() == 0) {
      std::cout << std::endl;
      std::cout << std::endl;
      std::cout << "------------------ " << std::endl;
      std::cout << "--- Parameters --- " << std::endl;
      std::cout << "------------------ " << std::endl;
      std::cout << general_param << std::endl;
      std::cout << grid_param << std::endl;
      std::cout << profile_param << std::endl;
      std::cout << smoother_param << std::endl;
    }

    // Create grid

    typedef Dune::GridSelector::GridType Grid;
    Dune::GridPtr<Grid> gridPtr(grid_param.gridFilename());
    Grid& grid = *gridPtr;
    
    grid.loadBalance();             
    grid.globalRefine(grid_param.refcount());

    if (helper.rank() == 0) {
      std::cout << " Number of grid cells on horizontal host grid = " 
                << grid.levelView(grid.maxLevel()).size(0) << std::endl;
      std::cout << " Number of vertical grid cells = " << nz << std::endl;
      std::cout << " Total number of grid cells = "
                << (nz*grid.levelView(grid.maxLevel()).size(0)) << std::endl;
    }

    // Create profile
    typedef AtmosphericProfile Profile;
    Profile profile(profile_param);

    typedef HierarchicDF<nz,Grid> HDF;
    typedef HDF::LevelDF DF;

    // Construct model operator
    typedef UnfactorizedModelOperator<DF,SphericalGeometryDescr<nz>,Profile>
      ModelOp;
    ModelOp opsmooth(grid,grid_param,smoother_param,profile);

    typedef Grid::LevelGridView GV;
    const GV& gv = grid.levelView(grid.maxLevel());

    // Extract functions and save to disk
    save_profile(opsmooth,Prof_a_hh,grid_param,"a_hh");
    save_profile(opsmooth,Prof_a_rr,grid_param,"a_rr");
    save_profile(opsmooth,Prof_a_r,grid_param,"a_r");
    save_profile(opsmooth,Prof_b,grid_param,"b");
  }
  catch (Dune::Exception &e){
    std::cerr << "Dune reported error: " << e << std::endl;
  }
  catch (...){
    std::cerr << "Unknown exception thrown!" << std::endl;
  }
}
