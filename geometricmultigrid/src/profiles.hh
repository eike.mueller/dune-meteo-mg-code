/*** COPYRIGHT STATEMENT ************************************ 
 *
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2013]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  This code has been developed by
 *  - Andreas Dedner (University of Warwick)
 *  - Eike Mueller (University of Bath)
 *
 *** COPYRIGHT STATEMENT ************************************/

#ifndef PROFILES_HH
#define PROFILES_HH
#include<math.h>
#include<dune/grid/alugrid.hh>
#include "profileparameters.hh"
#include "netcdffunctions.hh"

/* ********************************************************** *
 * Factorized profile with constant entries
 * ********************************************************** */
class ConstantProfile {

  public:
  // Constructor
  ConstantProfile(const ProfileParameters& profile_param) :
    profile_param_(profile_param) {
      omega2_ = profile_param.omega2();
      lambda2_ = profile_param.lambda2();
    }

  // 2x2 matrix for horizontal couplings in 2nd order term
  template <class Entity>
  double a_hh(const Entity &en,
              const typename Entity::Geometry::GlobalCoordinate &x,
              const double r) const
  {
    return a_hh_h(en,x) * a_hh_r(r);
  }

  template <class Entity>
  double a_hh_h(const Entity &en,
                const typename Entity::Geometry::GlobalCoordinate &x) const
  {
    return omega2_;
  };

  double a_hh_r(const double r) const
  {
    return 1.0;
  }

  // scalar for vertical couplings in 2nd order term
  template <class Entity>
  double a_rr(const Entity &en,
              const typename Entity::Geometry::GlobalCoordinate &x,
              const double r) const
  {
    return a_rr_h(en,x) * a_rr_r(r);
  }

  template <class Entity>
  double a_rr_h(const Entity &en,
                const typename Entity::Geometry::GlobalCoordinate &x)
    const
  {
    return omega2_;
  }

  double a_rr_r(const double r) const
  {
    return lambda2_;
  }

  // scalar for vertical couplings in 1st order term
  template <class Entity>
  double a_r(const Entity &en,
              const typename Entity::Geometry::GlobalCoordinate &x,
              const double r) const
  {
    return a_r_h(en,x) * a_r_r(r);
  }

  template <class Entity>
  double a_r_h(const Entity &en,
               const typename Entity::Geometry::GlobalCoordinate &x) const
  {
    return 0.0;
  }

  double a_r_r(const double r) const
  {
    return 0.0;
  }

  // scalar for zero order term
  template <class Entity>
  double b(const Entity &en,
           const typename Entity::Geometry::GlobalCoordinate &x,
           const double r) const
  {
    return b_h(en,x) * b_r(r);
  }

  template <class Entity>
  double b_h(const Entity &en,
             const typename Entity::Geometry::GlobalCoordinate &x)
    const
  {
    return 1.0;
  }

  double b_r(const double r) const
  {
    return 1.0;
  }

  ProfileParameters profile_param() const { return profile_param_; }

  private:
  double omega2_;
  double lambda2_;
  const ProfileParameters profile_param_;
};

/* ********************************************************** *
 * Factorized profile with exponentially decaying entries
 * ********************************************************** */
class ExponentialProfile {

  public:
  // Constructor
  ExponentialProfile(const ProfileParameters& profile_param) :
    profile_param_(profile_param) {
      omega2_ = profile_param.omega2();
      lambda2_ = profile_param.lambda2();
    }

  // 2x2 matrix for horizontal couplings in 2nd order term
  template <class Entity>
  double a_hh_h(const Entity &en,
                const typename Entity::Geometry::GlobalCoordinate &x) const
  {
    return omega2_*(1.0+x[0]*x[0]);
  };

  double a_hh_r(const double r) const
  {
    return exp(-5.0*(r-1.)/0.01);
  }

  // scalar for vertical couplings in 2nd order term
  template <class Entity>
  double a_rr_h(const Entity &en,
                const typename Entity::Geometry::GlobalCoordinate &x) const
  {
    return omega2_*(1.0+x[1]*x[1]);
  }

  double a_rr_r(const double r) const
  {
    return lambda2_*exp(-10.*(r-1.)/0.01);
  }

  // scalar for vertical couplings in 1st order term
  template <class Entity>
  double a_r_h(const Entity &en,
               const typename Entity::Geometry::GlobalCoordinate &x) const
  {
    return omega2_*(1.0+x[1]*x[1]);
  }

  double a_r_r(const double r) const
  {
    return lambda2_*exp(-10.*(r-1.)/0.01);
  }

  // scalar for zero order term
  template <class Entity>
  double b_h(const Entity &en,
             const typename Entity::Geometry::GlobalCoordinate &x)
    const
  {
    return 1.0+x[2]*x[2];
  }

  double b_r(const double r) const
  {
    return exp(-20.*(r-1.)/0.01);
  }

  ProfileParameters profile_param() const { return profile_param_; }

  private:
  double omega2_;
  double lambda2_;
  const ProfileParameters profile_param_;
};

/* ********************************************************** *
 * Factorised profile for zonal flow
 * ********************************************************** */

class FactorisingZonalFlowProfile {
  public:
  // Constructor
  FactorisingZonalFlowProfile(const ProfileParameters& profile_param) :
    g_grav_(9.81), // gravitational acceleration [m s^{-2}]
    Omega_(2.*3.14159/(24.*3600.)), // Angular frequency of the earth [s^{-1}]
    Rearth_(6371229.0), // Earth radius [m]
    Rd_(287.05),        // specific heat capacity of dry air [J kg^{-1} K^{-1}]
    cp_(1005.0),        // specific gas constant of dry air [J kg^{-1} K^{-1}]
    T0_(273.0),         // reference temperature [K]
    p0_(100000.0),      // Reference pressure [Pa]
    u0_(100.),          // Jet velocity [m s^{-1}]
    profile_param_(profile_param),
    pi_(3.14159265358979323846),
    phi_m_(0.25*pi_),   // Mean jet position
    sigma_(0.1),        // width of jet
    nudt_(profile_param.nudt()) {
    N_ = profile_param.Nbuoyancy();
    omega2_ = nudt_*nudt_*cp_*T0_/(Rearth_*Rearth_);
    kappa_ = Rd_/cp_;
    gamma_ = (1.-kappa_)/kappa_;
    Lambda_ = 1./(1.+N_*N_*nudt_*nudt_);
    G_ = g_grav_*g_grav_/(N_*N_*cp_); // Reference temperature [K]
    Gamma_ = p0_/Rd_;
    c_phi_m_ = cos(phi_m_);
    // Calculate jet function at North pole
    F_NP_ = 0.0;
    F_NP_ = F(0.5*pi_);
  }

  // return epsilon
  double epsilon() { return (1.-G_/T0_)/(G_/T0_); }

  // 2x2 matrix for horizontal couplings in 2nd order term
  template <class Entity>
  double a_hh(const Entity &en,
              const typename Entity::Geometry::GlobalCoordinate &x,
              const double r) const
  {
    return a_hh_h(en,x)*a_hh_r(r);
  };

  template <class Entity>
  double a_hh_h(const Entity &en,
                const typename Entity::Geometry::GlobalCoordinate &x) const
  {
    return omega2_*Gamma_/T0_*pow(Pi_h(en,x),gamma_);
  };

  double a_hh_r(const double r) const
  {
    return pow(Pi_r(r),gamma_);
  }

  // scalar for vertical couplings in 2nd order term
  template <class Entity>
  double a_rr(const Entity &en,
              const typename Entity::Geometry::GlobalCoordinate &x,
              const double r) const
  {
    return a_rr_h(en,x)*a_rr_r(r);
  }

  template <class Entity>
  double a_rr_h(const Entity &en,
                const typename Entity::Geometry::GlobalCoordinate &x) const
  {
    return omega2_*Lambda_*Gamma_/T0_*pow(Pi_h(en,x),gamma_);
  }

  double a_rr_r(const double r) const
  {
    return r*r*pow(Pi_r(r),gamma_);
  }

  // scalar for vertical couplings in 1st order term
  template <class Entity>
  double a_r(const Entity &en,
             const typename Entity::Geometry::GlobalCoordinate &x,
             const double r) const
  {
    return a_r_h(en,x)*a_r_r(r);
  }

  template <class Entity>
  double a_r_h(const Entity &en,
               const typename Entity::Geometry::GlobalCoordinate &x) const
  {
    return omega2_*Rearth_*Lambda_*N_*N_*Gamma_/(g_grav_*T0_)*pow(Pi_h(en,x),gamma_);
  }

  double a_r_r(const double r) const
  {
    return pow(Pi_r(r),gamma_);
  }

  // scalar for zero order term
  template <class Entity>
  double b(const Entity &en,
           const typename Entity::Geometry::GlobalCoordinate &x,
           const double r)
    const
  {
    return b_h(en,x)*b_r(r);
  }

  template <class Entity>
  double b_h(const Entity &en,
             const typename Entity::Geometry::GlobalCoordinate &x)
    const
  {
    return gamma_*Gamma_/T0_*E_h(en,x)*pow(Pi_h(en,x),gamma_-1.);
  }

  double b_r(const double r) const
  {
    return E_r(r)*pow(Pi_r(r),gamma_-1.);
  }

  ProfileParameters profile_param() const { return profile_param_; }

  protected:

  // Exner pressure pi
  template <class Entity>
  double Pi_h(const Entity &en,
            const typename Entity::Geometry::GlobalCoordinate &x)
    const {
    return E_h(en,x);
  }

  double Pi_r(const double r)
    const {
    return (1.-G_/T0_) + G_/T0_*E_r(r);
  }

  // Horizontal and vertical exponential functions
  template <class Entity>
  double E_h(const Entity &en,
            const typename Entity::Geometry::GlobalCoordinate &x)
    const {
    double phi = atan2(x[2],sqrt(x[0]*x[0]+x[1]*x[1]));
    return exp(-N_*N_/(g_grav_*g_grav_)*F(phi));
  }

  double E_r(const double r)
    const {
    return exp(-N_*N_/g_grav_*((r-1.)*Rearth_));
  }

  // Jet function F(phi)
  double F(const double phi) const {
    double c_phi = cos(phi);
    /*
    return -u0_*(Rearth_*Omega_+0.5*u0_)*c_phi*c_phi;
    */
    double r = -2.*Rearth_*Omega_*u0_/c_phi_m_ * (sqrt(0.5*pi_)*c_phi_m_*sigma_*erf((c_phi-c_phi_m_)/(sqrt(2.)*sigma_)) - sigma_*sigma_*exp(-0.5*(c_phi-c_phi_m_)*(c_phi-c_phi_m_)/(sigma_*sigma_)));
    r += -u0_*u0_/(c_phi_m_*c_phi_m_)* (0.5*sqrt(pi_)*c_phi_m_*sigma_*erf((c_phi-c_phi_m_)/sigma_) - 0.5*sigma_*sigma_*exp(-(c_phi-c_phi_m_)*(c_phi-c_phi_m_)/(sigma_*sigma_)));
    return r - F_NP_;
  }

  const double Rearth_;
  double Rd_;
  double cp_;
  double nudt_;
  const double g_grav_;
  double omega2_;
  double kappa_;
  double gamma_;
  double Gamma_;
  double Lambda_;
  double N_;
  double T0_;
  double u0_;
  double p0_;
  double G_;
  const double Omega_;
  const ProfileParameters profile_param_;
  const double pi_;
  const double phi_m_;
  const double sigma_;
  double c_phi_m_;
  double F_NP_;
};

/* ********************************************************** *
 * Unfactorised profile for zonal flow
 * ********************************************************** */

class ZonalFlowProfile : public FactorisingZonalFlowProfile {
  public:
  // Constructor
  ZonalFlowProfile(const ProfileParameters& profile_param) :
    FactorisingZonalFlowProfile(profile_param) {
  };


  // 2x2 matrix for horizontal couplings in 2nd order term
  template <class Entity>
  double a_hh(const Entity &en,
              const typename Entity::Geometry::GlobalCoordinate &x,
              const double r) const
  {
    return omega2_*Gamma_/T0_*pow(Pi(en,x,r),gamma_);
  };

  // scalar for vertical couplings in 2nd order term
  template <class Entity>
  double a_rr(const Entity &en,
              const typename Entity::Geometry::GlobalCoordinate &x,
              const double r) const
  {
    return omega2_*Lambda_*Gamma_*(r*r)/T0_*pow(Pi(en,x,r),gamma_);
  }

  // scalar for vertical couplings in 1st order term
  template <class Entity>
  double a_r(const Entity &en,
             const typename Entity::Geometry::GlobalCoordinate &x,
             const double r) const
  {
    return omega2_*Rearth_*Lambda_*N_*N_*Gamma_/(g_grav_*T0_)*pow(Pi(en,x,r),gamma_);
  }

  // scalar for zero order term
  template <class Entity>
  double b(const Entity &en,
           const typename Entity::Geometry::GlobalCoordinate &x,
           const double r)
    const
  {
    return gamma_*Gamma_/T0_*E_h(en,x)*E_r(r)*pow(Pi(en,x,r),gamma_-1.);
  }
  protected:

  // Exner pressure pi
  template <class Entity>
  double Pi(const Entity &en,
            const typename Entity::Geometry::GlobalCoordinate &x,
            const double r)
    const {
    return (1.-G_/T0_) + G_/T0_*E_h(en,x)*E_r(r);
  }
};

/* ********************************************************** *
 * Unfactorized profile using \theta, \pi and \rho read
 * from a netCDF file
 * ********************************************************** */
class AtmosphericProfile {

  public:
  // Constructor
  AtmosphericProfile(const ProfileParameters& profile_param) :
    profile_param_(profile_param),
    nudt_(profile_param.nudt()),
    Rd_(287.05),        // specific heat capacity of dry air [J kg^{-1} K^{-1}]
    cp_(1005.0),        // specific gas constant of dry air [J kg^{-1} K^{-1}]
    T0_(300.0),         // reference temperature [K]
    rho0_(1.0),         // reference density [kg / m^3]
    g_grav_(9.81) {     // gravitational acceleration [m s^{-2}]
    functions_ = new NetCDFfunctions(profile_param.filename(),nudt_,0);
    const double Rearth = functions_->getRearth();
    omega2_ = nudt_*nudt_*cp_*T0_/(Rearth*Rearth);
    double kappa = Rd_/cp_;
    gamma_ = (1.-kappa)/kappa;
  }

  // 2x2 matrix for horizontal couplings in 2nd order term
  template <class FieldVector>
  double a_hh(const FieldVector &x,
              const double r) const
  {
    return omega2_*functions_->geta_hh(x,r)/(rho0_*T0_);
  };

  template <class Entity>
  double a_hh(const Entity &en,
              const typename Entity::Geometry::GlobalCoordinate &x,
              const double r) const
  {
    return a_hh(x,r);
  };

  template <class Entity>
  double a_hh_h(const Entity &en,
                const typename Entity::Geometry::GlobalCoordinate &x) const
  {
    return 1.0;
  };

  double a_hh_r(const double r,const int index=0) const
  {
    return omega2_*functions_->geta_hh_r(r,index)/(rho0_*T0_);
  };

  // scalar for vertical couplings in 2nd order term
  template <class FieldVector>
  double a_rr(const FieldVector &x,
              const double r) const
  {
    return omega2_*functions_->geta_rr(x,r)/(rho0_*T0_);
  }

  template <class Entity>
  double a_rr(const Entity &en,
              const typename Entity::Geometry::GlobalCoordinate &x,
              const double r) const
  {
    return a_rr(x,r);
  }

  template <class Entity>
  double a_rr_h(const Entity &en,
                const typename Entity::Geometry::GlobalCoordinate &x) const
  {
    return 1.0;
  }

  double a_rr_r(const double r,const int index=0) const
  {
    return omega2_*functions_->geta_rr_r(r,index)/(rho0_*T0_);
  }

  // scalar for vertical couplings in 1st order term
  template <class FieldVector>
  double a_r(const FieldVector &x,
             const double r) const
  {
    return omega2_*functions_->geta_r(x,r)/(rho0_*T0_);
  }

  template <class Entity>
  double a_r(const Entity &en,
             const typename Entity::Geometry::GlobalCoordinate &x,
             const double r) const
  {
    return a_r(x,r);
  }

  template <class Entity>
  double a_r_h(const Entity &en,
               const typename Entity::Geometry::GlobalCoordinate &x) const
  {
    return 1.0;
  }

  double a_r_r(const double r,const int index=0) const
  {
    return omega2_*functions_->geta_r_r(r,index)/(rho0_*T0_);
  }

  // scalar for zero order term
  template <class FieldVector>
  double b(const FieldVector &x,
           const double r) const
  {
    return functions_->getb(x,r)/(rho0_);
  }
  template <class Entity>
  double b(const Entity &en,
           const typename Entity::Geometry::GlobalCoordinate &x,
           const double r) const
  {
    return b(x,r);
  }

  template <class Entity>
  double b_h(const Entity &en,
             const typename Entity::Geometry::GlobalCoordinate &x)
    const
  {
    return 1.0;
  }

  double b_r(const double r,const int index=0) const
  {
    return functions_->getb_r(r,index)/(rho0_);
  }

  ProfileParameters profile_param() const { return profile_param_; }

  private:

  // Adiabatic coefficient (for testing)
  double gamma() const {
    return gamma_;
  }

  const NetCDFfunctions *functions_;
  // specific heat capacity of dry air [J kg^{-1} K^{-1}]
  const double Rd_;
  // specific gas constant of dry air [J kg^{-1} K^{-1}]
  const double cp_;
  // reference temperature [K]
  const double T0_;
  // reference density [kg / m^3]
  const double rho0_;
  // Gravitational acceleration at the earth surface
  const double g_grav_;
  // Time step size (including off-centering parameter) in seconds,
  // needed in calculation of \Lambda
  const double nudt_;
  // omega2 = (\nu dt)^2 c_p T_0 / R_{earth}^2
  double omega2_;
  // Adiabatic coefficient (1-\kappa)/\kappa
  double gamma_;

  const ProfileParameters& profile_param_;
};
/* ********************************************************** *
 * Generate Unfactorised profile from a factorised profile
 * ********************************************************** */
template <class Profile>
class ToUnfacProfile {

  public:
  // Constructor
  ToUnfacProfile(const Profile& profile) : profile_(profile) {}

  // 2x2 matrix for horizontal couplings in 2nd order term
  template <class Entity>
  double a_hh_h(const Entity &en,
                const typename Entity::Geometry::GlobalCoordinate &x) const
  {
    return profile_.a_hh_h(en,x);
  };

  double a_hh_r(const double r) const
  {
    return profile_.a_hh_r(r);
  }

  template <class Entity>
  double a_hh(const Entity &en,
              const typename Entity::Geometry::GlobalCoordinate &x,
              const double r) const
  {
    return profile_.a_hh_h(en,x) * profile_.a_hh_r(r);
  }

  // scalar for vertical couplings in 2nd order term
  template <class Entity>
  double a_rr_h(const Entity &en,
                const typename Entity::Geometry::GlobalCoordinate &x)
    const
  {
    return profile_.a_rr_h(en,x);
  }

  double a_rr_r(const double r) const
  {
    return profile_.a_rr_r(r);
  }

  template <class Entity>
  double a_rr(const Entity &en,
              const typename Entity::Geometry::GlobalCoordinate &x,
              const double r) const
  {
    return profile_.a_rr_h(en,x) * profile_.a_rr_r(r);
  }

  template <class Entity>
  double a_r_h(const Entity &en,
               const typename Entity::Geometry::GlobalCoordinate &x)
    const
  {
    return profile_.a_r_h(en,x);
  }

  double a_r_r(const double r) const
  {
    return profile_.a_r_r(r);
  }

  template <class Entity>
  double a_r(const Entity &en,
             const typename Entity::Geometry::GlobalCoordinate &x,
             const double r) const
  {
    return profile_.a_r_h(en,x) * profile_.a_r_r(r);
  }

  // scalar for zero order term
  template <class Entity>
  double b_h(const Entity &en,
             const typename Entity::Geometry::GlobalCoordinate &x)
    const
  {
    return profile_.b_h(en,x);
  }

  double b_r(const double r) const
  {
    return profile_.b_r(r);
  }

  template <class Entity>
  double b(const Entity &en,
           const typename Entity::Geometry::GlobalCoordinate &x,
           const double r) const
  {
    return profile_.b_h(en,x) * profile_.b_r(r);
  }

  ProfileParameters profile_param() const { return profile_.profile_param(); }

  private:
  const Profile &profile_;
};

#endif // PROFILES_HH
