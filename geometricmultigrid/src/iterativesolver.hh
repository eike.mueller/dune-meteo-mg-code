/*** COPYRIGHT STATEMENT ************************************ 
 *
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2013]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  This code has been developed by
 *  - Andreas Dedner (University of Warwick)
 *  - Eike Mueller (University of Bath)
 *
 *** COPYRIGHT STATEMENT ************************************/

#ifndef ITERATIVESOLVER_HH
#define ITERATIVESOLVER_HH
#include<parameters.hh>
#include <dune/common/mpihelper.hh>

/* ********************************************************** *
 *  Parameters of solver
 * ********************************************************** */

class SolverParameters : public Parameters {
 public:
  // Constructor
  SolverParameters() :
                  Parameters("solver"),
                  maxiter_(100),
                  resreduction_(1.0e-5),
                  rhorelax_(1.0) {
      addKey("maxiter",Integer);
      addKey("resreduction",Double);
      addKey("rhorelax",Double);
    }

  //
  int readFile(const std::string filename) {
    int readSuccess = Parameters::readFile(filename);
    if (!readSuccess) {
      maxiter_ = contents["maxiter"]->getInt();
      resreduction_ = contents["resreduction"]->getDouble();
      rhorelax_ = contents["rhorelax"]->getDouble();
    }
    return readSuccess;
  }

  // Return data
  double resreduction() const { return resreduction_; }
  unsigned int maxiter() const { return maxiter_; }
  double rhorelax() const { return rhorelax_; }

 private:
  unsigned int maxiter_;
  double resreduction_;
  double rhorelax_;
};

/* ********************************************************** *
 * Information about convergence of iterative solver
 * ********************************************************** */
struct SolveInfo {
  SolveInfo(const bool converged,
            const unsigned int iterations,
            const double resreduction) :
              converged_(converged),
              iterations_(iterations),
              resreduction_(resreduction) {}

  double rho_avg() const { return pow(resreduction_,1./iterations_);}
  bool converged_;
  unsigned int iterations_;
  double resreduction_;
  double tTotal_;
  double tLoop_;
  double tIter_;
};

/* ********************************************************** *
 *  Preconditioned Richardson iteration
 * ********************************************************** */
template <class DF>
class LoopSolver : public Operator<DF> {
 public:
  // Constructor
  LoopSolver(Dune::MPIHelper& mpihelper,
             Operator<DF>& op,
             Operator<DF>& prec,
             const SolverParameters& solver_param) :
             mpihelper_(mpihelper),
             op_(op),
             prec_(prec),
             solver_param_(solver_param),
             solveInfo_(false,0,0.0) {}
  // Apply operator
  virtual void apply(const DF &u, DF &b) const {
    op_.apply(u,b);
  }

  // Solve approximately
  virtual void solveApprox(const DF &b, DF &u) const {
    op_.solveApprox(b,u);
  }

  // Solve 'exactly' (to given tolerance) using Richardson iteration
  virtual void solve(const DF &b, DF &u) const
  {
    Dune::Timer totalTimer(true);
    // Calculate initial residual
    DF res(b.gridView(),b.level()), e(b.gridView(),b.level());
    op_.residual(b,u,res);
    double resnorm0 = norm(res);
    double resnorm_old = resnorm0;
    if (mpihelper_.rank() == 0) {
      std::cout << "Initial residual = " << std::scientific << std::setprecision(5) << resnorm0 << std::endl;
    }
    solveInfo_.converged_ = false;
    solveInfo_.iterations_ = 0;
    solveInfo_.resreduction_ = 0.0;
    if (mpihelper_.rank() == 0) {
      std::cout << "iter  :   res/res0      rho" << std::endl;
    }
    Dune::Timer loopTimer(true);
    for (unsigned int iter=0;iter<solver_param_.maxiter();++iter) {
      e.dofs() = 0.0;
      prec_.solveApprox(res,e);
      e.dofs() *= solver_param_.rhorelax();
      u.dofs() += e.dofs();
      op_.residual(b,u,res);
      double resnorm = norm(res);
      if (mpihelper_.rank() == 0) {
        std::cout << std::setw(5) << (iter+1) << " :   ";
        std::cout << std::scientific << std::setprecision(5) << resnorm/resnorm0 << "   ";
        std::cout << std::fixed << std::setprecision(5) << resnorm/resnorm_old;
        std::cout << std::endl;
      }
      if (resnorm/resnorm0 < solver_param_.resreduction()) {
        solveInfo_.iterations_ = iter+1;
        solveInfo_.resreduction_ = resnorm/resnorm0;
        solveInfo_.converged_ = true;
        break;
      }
      resnorm_old = resnorm;
    }
    double tLoop = loopTimer.stop();
    double tTotal = totalTimer.stop();
    solveInfo_.tLoop_ = mpihelper_.getCollectiveCommunication().max(tLoop);
    solveInfo_.tTotal_ = mpihelper_.getCollectiveCommunication().max(tTotal);
    solveInfo_.tIter_ = tLoop/solveInfo_.iterations_;
  }

  virtual double norm( const DF &u) const
  {
    return u.dof_norm();
  }

  SolveInfo solveInfo() { return solveInfo_; }

 private:
  Dune::MPIHelper& mpihelper_;
  const Operator<DF> &op_;
  const Operator<DF> &prec_;
  SolverParameters solver_param_;
  double resreduction_;
  mutable SolveInfo solveInfo_;
};

/* ********************************************************** *
 *  Preconditioned Conjugate Gradient solver
 * ********************************************************** */
template <class DF>
class PCGSolver : public Operator<DF> {
 public:
  // Constructor
  PCGSolver(Dune::MPIHelper& mpihelper,
             Operator<DF>& op,
             Operator<DF>& prec,
             const SolverParameters& solver_param) :
             mpihelper_(mpihelper),
             op_(op),
             prec_(prec),
             solver_param_(solver_param),
             solveInfo_(false,0,0.0) {}

  // Apply operator
  virtual void apply(const DF &u, DF &b) const {
    op_.apply(u,b);
  }

  // Solve approximately
  virtual void solveApprox(const DF &b, DF &u) const {
    op_.solveApprox(b,u);
  }

  // Solve 'exactly' (to given tolerance) using CG iteration
  virtual void solve(const DF &b, DF &u) const
  {
    Dune::Timer totalTimer(true);
    // Temporary vectors
    DF res(b.gridView(),b.level()),
       p(b.gridView(),b.level()),
       z(b.gridView(),b.level());
    // Calculate initial residual and initialise
    op_.residual(b,u,res);
    z.dofs() = 0.0;
    prec_.solveApprox(res,z);
    p.dofs() = z.dofs();
    double rz_old = dotprod(res,z);
    double resnorm0 = norm(res);
    double resnorm_old = resnorm0;
    if (mpihelper_.rank() == 0) {
      std::cout << "Initial residual = " << std::scientific << std::setprecision(5) << resnorm0 << std::endl;
    }
    solveInfo_.converged_ = false;
    solveInfo_.iterations_ = 0;
    solveInfo_.resreduction_ = 0.0;
    if (mpihelper_.rank() == 0) {
      std::cout << "iter  :   res/res0      rho" << std::endl;
    }
    Dune::Timer loopTimer(true);
    for (unsigned int iter=0;iter<solver_param_.maxiter();++iter) {
      op_.apply(p,z);
      double alpha = rz_old / dotprod(p,z);
      /*
      p.dofs() *= alpha;
      u.dofs() += p.dofs();
      z.dofs() *= alpha;
      res.dofs() -= z.dofs();
      */
      u.dofs().axpy(alpha,p.dofs());
      res.dofs().axpy(-alpha,z.dofs());

      double resnorm = norm(res);
      if (mpihelper_.rank() == 0) {
        std::cout << std::setw(5) << (iter+1) << " :   ";
        std::cout << std::scientific << std::setprecision(5) << resnorm/resnorm0 << "   ";
        std::cout << std::fixed << std::setprecision(5) << resnorm/resnorm_old;
        std::cout << std::endl;
      }
      if (resnorm/resnorm0 < solver_param_.resreduction()) {
        solveInfo_.converged_ = true;
        solveInfo_.iterations_ = iter+1;
        solveInfo_.resreduction_ = resnorm/resnorm0;
        break;
      }
      z.dofs() = 0.0;
      prec_.solveApprox(res,z);
      double rz = dotprod(res,z);
      double beta = rz / rz_old;
      rz_old = rz;
      p.dofs() *= beta;
      p.dofs() += z.dofs();
      resnorm_old = resnorm;
    }
    double tLoop = loopTimer.stop();
    double tTotal = totalTimer.stop();
    solveInfo_.tLoop_ = mpihelper_.getCollectiveCommunication().max(tLoop);
    solveInfo_.tTotal_ = mpihelper_.getCollectiveCommunication().max(tTotal);
    solveInfo_.tIter_ = tLoop/solveInfo_.iterations_;
  }

  virtual double dotprod( const DF &u, const DF &v) const
  {
    return u.dof_dotprod(v);
  }

  virtual double norm( const DF &u) const
  {
    return u.dof_norm();
  }

  SolveInfo solveInfo() { return solveInfo_; }

 private:
  Dune::MPIHelper& mpihelper_;
  const Operator<DF> &op_;
  const Operator<DF> &prec_;
  SolverParameters solver_param_;
  double resreduction_;
  mutable SolveInfo solveInfo_;
};

/* ********************************************************** *
 *  Preconditioned BiCGStab solver
 * ********************************************************** */
template <class DF>
class PBiCGStabSolver : public Operator<DF> {
 public:
  // Constructor
  PBiCGStabSolver(Dune::MPIHelper& mpihelper,
                  Operator<DF>& op,
                  Operator<DF>& prec,
                  const SolverParameters& solver_param) :
                  mpihelper_(mpihelper),
                  op_(op),
                  prec_(prec),
                  solver_param_(solver_param),
                  solveInfo_(false,0,0.0) {}

  // Apply operator
  virtual void apply(const DF &u, DF &b) const {
    op_.apply(u,b);
  }

  // Solve approximately
  virtual void solveApprox(const DF &b, DF &u) const {
    op_.solveApprox(b,u);
  }

  // Solve 'exactly' (to given tolerance) using BiCGStab iteration
  virtual void solve(const DF &b, DF &u) const
  {
    Dune::Timer totalTimer(true);
    // Temporary vectors
    DF res(b.gridView(),b.level()),
       res_0(b.gridView(),b.level()),
       p(b.gridView(),b.level()),
       v(b.gridView(),b.level()),
       s(b.gridView(),b.level()),
       t(b.gridView(),b.level());
    p.dofs() = 0.0;
    v.dofs() = 0.0;
    double alpha = 1.0;
    double omega = 1.0;
    double rho_old = 1.0;
    // Calculate initial residual and initialise
    op_.residual(b,u,res_0);
    res.dofs() = res_0.dofs();
    double resnorm0 = norm(res_0);
    double resnorm_old = resnorm0;
    if (mpihelper_.rank() == 0) {
      std::cout << "Initial residual = " << std::scientific << std::setprecision(5) << resnorm0 << std::endl;
    }
    solveInfo_.converged_ = false;
    solveInfo_.iterations_ = 0;
    solveInfo_.resreduction_ = 0.0;
    if (mpihelper_.rank() == 0) {
      std::cout << "iter  :   res/res0      rho" << std::endl;
    }
    Dune::Timer loopTimer(true);
    for (unsigned int iter=0;iter<solver_param_.maxiter();++iter) {
      double rho = dotprod(res,res_0);
      double beta = alpha*rho/(omega*rho_old);
      t.dofs() = res.dofs();
      t.dofs().axpy(-beta*omega,v.dofs());
      s.dofs() = 0.0;
      prec_.solveApprox(t,s);
      p.dofs() *= beta;
      p.dofs().axpy(1.0,s.dofs());
      op_.apply(p,v);
      alpha = rho/dotprod(res_0,v);
      s.dofs() = res.dofs();
      s.dofs().axpy(-alpha,v.dofs());
      res.dofs() = 0.0;
      prec_.solveApprox(s,res);
      op_.apply(res,t);
      double st = dotprod(t,s);
      double ss_tmp = norm(s);
      double ss = ss_tmp*ss_tmp;
      double tt_tmp = norm(t);
      double tt = tt_tmp*tt_tmp;
      omega = st/tt;
      u.dofs().axpy(alpha,p.dofs());
      u.dofs().axpy(omega,res.dofs());
      res.dofs() = s.dofs();
      res.dofs().axpy(-omega,t.dofs());
      double resnorm = sqrt(ss-st*st/tt);
      if (mpihelper_.rank() == 0) {
        std::cout << std::setw(5) << (iter+1) << " :   ";
        std::cout << std::scientific << std::setprecision(5) << resnorm/resnorm0 << "   ";
        std::cout << std::fixed << std::setprecision(5) << resnorm/resnorm_old;
        std::cout << std::endl;
      }
      if (resnorm/resnorm0 < solver_param_.resreduction()) {
        solveInfo_.converged_ = true;
        solveInfo_.iterations_ = iter+1;
        solveInfo_.resreduction_ = resnorm/resnorm0;
        break;
      }
      resnorm_old = resnorm;
    }
    double tLoop = loopTimer.stop();
    double tTotal = totalTimer.stop();
    solveInfo_.tLoop_ = mpihelper_.getCollectiveCommunication().max(tLoop);
    solveInfo_.tTotal_ = mpihelper_.getCollectiveCommunication().max(tTotal);
    solveInfo_.tIter_ = tLoop/solveInfo_.iterations_;
  }

  virtual double dotprod( const DF &u, const DF &v) const
  {
    return u.dof_dotprod(v);
  }

  virtual double norm( const DF &u) const
  {
    return u.dof_norm();
  }

  SolveInfo solveInfo() { return solveInfo_; }

 private:
  Dune::MPIHelper& mpihelper_;
  const Operator<DF> &op_;
  const Operator<DF> &prec_;
  SolverParameters solver_param_;
  double resreduction_;
  mutable SolveInfo solveInfo_;
};

#endif // ITERATIVESOLVER_HH

