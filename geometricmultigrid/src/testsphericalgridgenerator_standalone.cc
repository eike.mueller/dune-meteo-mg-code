/*** COPYRIGHT STATEMENT ************************************ 
 *
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2013]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  This code has been developed by
 *  - Andreas Dedner (University of Warwick)
 *  - Eike Mueller (University of Bath)
 *
 *** COPYRIGHT STATEMENT ************************************/
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include<iostream>
#include<vector>
#include <string>
#include <sstream>
#include<dune/common/mpihelper.hh>
#include<dune/common/exceptions.hh>
#include<dune/common/fvector.hh>
#include<dune/grid/io/file/vtk/vtkwriter.hh>
#include<dune/grid/uggrid.hh>
#include<dune/grid/io/file/dgfparser/dgfug.hh>
#include <dune/istl/bvector.hh>

// Number of vertical levels
const int nz=128;

#define UGGRID UGGRID

/* *************************************************************** *
 * *************************************************************** *
 * Exceptions
 * *************************************************************** *
 * *************************************************************** */
class SphericalGridGeneratorException : public Dune::Exception {
};

/* *************************************************************** *
 * *************************************************************** *
 * Class for reading grid function .dgf file
 * *************************************************************** *
 * *************************************************************** */

class SphericalGridGenerator {
  public:

    static const int dim = 3;
    static const int dimworld = 3;
    typedef Dune::UGGrid < dim > Grid;

  /* *************************************************************** *
   * Constructor
   * *************************************************************** */
    SphericalGridGenerator(std::string filename, 
                           const int refcount=0) :
      filename_(filename),
      refcount_(refcount) {
      createGrid();
    }

  /* *************************************************************** *
   * Destructor
   * *************************************************************** */
    ~SphericalGridGenerator() { delete(gridPtr); }
    
  /* *************************************************************** *
   * return pointer to grid
   * *************************************************************** */
    Grid* getgridPtr() { return gridPtr; }

  protected:
  /* *************************************************************** *
   * Generate grid from file
   * *************************************************************** */
    void createGrid() {
      // Create grid 

      Dune::DGFGridFactory<Grid> factory_(filename_);
      gridPtr = factory_.grid();
      gridPtr->loadBalance();

      /* *** Refine grid *** */
      typedef Grid::LeafGridView GV ;
      GV gv = gridPtr->leafView();
      typedef GV::Codim<0>::Iterator ElementLeafIterator;
      typedef ElementLeafIterator::Entity Element;
      typedef Element::Geometry LeafGeometry;
      ElementLeafIterator ibegin = gv.begin<0>();
      ElementLeafIterator iend = gv.end<0>();
      for (int iref =0;iref<refcount_;++iref) {
        for (ElementLeafIterator it = ibegin;it!=iend;++it) {
          Dune::UG_NS<dim>::RefinementRule refinementrule;
          Element& e = *it;
          if (e.type().isCube()) {
            refinementrule = UG::D3::HEX_QUADSECT_0;
          }
          if (e.type().isPrism()) {
            refinementrule = UG::D3::PRISM_QUADSECT;
          } 
          gridPtr->mark(*it,refinementrule,0);
        }
        
        gridPtr->adapt();
        gridPtr->postAdapt();      
      }
    }

  protected:
    // Pointer to grid
    Grid* gridPtr;
    // Name of .dgf file
    std::string filename_;
    // Number of refinement steps
    int refcount_;
};

/* *************************************************************** *
 * *************************************************************** *
 *
 *   Grid function on a fixed level of a 2d grid 
 *   (defined by a (level-)gridview).
 *
 *   Data on each entity of the underlying 2dimensional grid 
 *   is stored in a field of type 
 *     LocalVector = Dune::FieldVector<double,NZ>
 *   where the template parameter NZ stores the number of vertical 
 *   grid cells.
 *   All data is contained in the field dofs_ which is of type 
 *     Dune::BlockVector<LocalVector>
 *   
 * *************************************************************** *
 * *************************************************************** */
template <int NZ, class GV,int codim>
struct DFBase
{
  static const int nz = NZ; // Number of vertical levels (cells)
#if defined UGGRID
  static const int dim = GV::dimension-1;
#else // defined UGGRID
  static const int dim = GV::dimension;
#endif // defined UGGRID
  typedef GV GridView;
  typedef typename GridView::Grid Grid;
  static const int dimworld = GV::dimensionworld;
  typedef typename GridView::template Codim<0>::
   	      template Partition<Dune::Interior_Partition>::Iterator Iterator; 
  typedef typename Iterator::Entity Entity;
  typedef typename Entity::EntityPointer EntityPointer;
  typedef typename Entity::Geometry Geometry;
  typedef Dune::FieldVector<double,NZ> LocalVector;
  typedef double field_type;

 /* *************************************************************** *
  * Constructors
  * *************************************************************** */
  DFBase(const GV &gv,    // Gridview
         int level
        ) : gv_(gv),
            level_(level),
            dofs_(gv_.size(codim),0)
  {}
  
 /* *************************************************************** *
  * Iterators for looping over the grid function
  * *************************************************************** */
  const Iterator end() const
  {
    return gv_.template end<0,Dune::Interior_Partition>();
  }
  Iterator begin() const
  {
    return gv_.template begin<0,Dune::Interior_Partition>();
  }

 /* *************************************************************** *
  * [] operator extracts data in vertical column on a specific
  * element of the 2-dimensional grid
  * *************************************************************** */
  const LocalVector &operator()(const Entity &en,int i) const
  {
    int idx = index(en,i);
    assert( idx < dofs_.size() );
    return dofs_[ idx ];
  }
  LocalVector &operator()(const Entity &en,int i)
  {
    int idx = index(en,i);
    assert( idx < dofs_.size() );
    return dofs_[ idx ];
  }
  size_t index(const Entity &en,int i) const
  {
    return gv_.indexSet().subIndex(en,i,codim);
  }
  size_t level() const
  {
    return level_;
  }


 /* *************************************************************** *
  * Extract all degrees of freedom (data)
  * *************************************************************** */
  const Dune::BlockVector<LocalVector>& dofs() const
  {
    return dofs_;
  }

  Dune::BlockVector<LocalVector>& dofs()
  {
    return dofs_;
  }

  /* *************************************************************** *
  * Extract gridview of 2dimensional grid
  * *************************************************************** */
  const GV &gridView() const
  {
    return gv_;
  }

  /* *************************************************************** *
   * private class data
   * *************************************************************** */
 protected:
  const GV gv_; // Underlying gridview
  int level_;
  typedef Dune::BlockVector<LocalVector> V;
  V dofs_; // field data
};

template <int NZ, class GV,int codim=0>
struct DF;

template <int NZ, class GV,int codim>
struct DF : public DFBase<NZ,GV,codim>
{
  typedef DFBase<NZ,GV,codim> Base;
  static const int dim=Base::dim; 
  DF(const GV &gv,    // Gridview
     int level
    ) : Base(gv,level) {}
};

/* *************************************************************** *
 * Specialisation for codim 0 entities
 * *************************************************************** */
template <int NZ, class GV>
struct DF<NZ,GV,0> : public DFBase<NZ,GV,0>
{
  typedef DFBase<NZ,GV,0> Base;
  static const int dim=Base::dim; 
  static const int nz =Base::nz;
  typedef typename Base::LocalVector LocalVector;
  typedef typename Base::Iterator Iterator;
  typedef typename Base::Entity Entity;
  typedef typename Base::V V;
  typedef typename GV::CollectiveCommunication CollectiveCommunication;
  DF(const GV &gv,    // Gridview
     int level
    ) : Base(gv,level),
        interior_dofs_mask_(gv_.size(0)), 
        haloexchanger_(gv_.indexSet(),dofs_),
        r_(NZ+1)
        {
    precompute_interior_dofs_mask();
    for (int i=0;i<=NZ;++i) r_[i] = 1.+double(i)/double(NZ);
  }
    
/* *************************************************************** *
 * Communication interface
 * *************************************************************** */
  template <class IndexSet, class V>
  class VectorExchange :
    public Dune::CommDataHandleIF<VectorExchange<IndexSet,V>, 
                                  typename V::value_type> {
   public:
    typedef typename V::value_type DataType; // Data type
    bool contains (int dim, int codim) const { return (codim == 0); }
    bool fixedsize (int dim, int codim) const { return true; }
    template <class EntityType>
    size_t size(EntityType& e) const { return 1; }
    // Pack user data into message buffer
    template <class MessageBuffer, class EntityType>
    void gather (MessageBuffer& buff, const EntityType& e) const {
      buff.write(c_[indexset_.index(e)]);
    }
    // Unpack user data from message buffer 
    template <class MessageBuffer, class EntityType>
    void scatter (MessageBuffer& buff, const EntityType& e, size_t n) {
      DataType x;
      buff.read(x);
      c_[indexset_.index(e)] = x;
    }
    // Constructor
    VectorExchange(const IndexSet& indexset, V& c) : 
      indexset_(indexset), c_(c) {}
   private:
    const IndexSet& indexset_; // map entity -> index set
    V& c_; // Data container
  };

/* ********************************************************** *
 * Precompute interior dofs mask
 * ********************************************************** */
  void precompute_interior_dofs_mask()
  {
    for (size_t i=0;i<dofs_.size();++i) 
    {
      interior_dofs_mask_[i] = 0;
    }
    Iterator ibegin = this->begin();
    Iterator iend = this->end();
    for(Iterator it=ibegin;it!=iend;++it) {
      const Entity &en = *it;
      interior_dofs_mask_[index(en)] = 1;
    }
  }

  /* *************************************************************** *
   * Index operators
   * *************************************************************** */
  const LocalVector &operator[](const Entity &en) const
  {
    assert( gv_.indexSet().index(en) < dofs_.size() );
    return dofs_[ gv_.indexSet().index(en) ];
  }

  LocalVector &operator[](const Entity &en)
  {
    assert( gv_.indexSet().index(en) < dofs_.size() );
    return dofs_[ gv_.indexSet().index(en) ];
  }
  size_t index(const Entity &en) const
  {
    return gv_.indexSet().index(en);
  }
  /* *************************************************************** *
  * Extract global communication object
  * *************************************************************** */
  const CollectiveCommunication& comm() const {
    return gv_.comm();
  }
 
 /* *************************************************************** *
  * Halo exchange
  * *************************************************************** */
  void haloexchange() {
    gv_.communicate(haloexchanger_,
                    Dune::InteriorBorder_All_Interface,
                    Dune::ForwardCommunication);
  }

  /* *************************************************************** *
   * Save data on one level to vtk file
   * *************************************************************** */
  void savetovtk(const std::string filename,
                 Dune::VTK::OutputType vtkoutputtype=Dune::VTK::ascii) {
    std::vector<double> c(dofs_.size()*nz);
    Iterator ibegin = this->begin();
    Iterator iend = this->end();
    for(Iterator it=ibegin;it!=iend;++it) {
      const Entity &en = *it;
      for (int k =0;k<nz;++k)
        c[gv_.indexSet().index(en)*nz+k] = dofs_[gv_.indexSet().index(en)][k];
    }
    typename Dune::VTKWriter<GV> vtkwriter(gv_,Dune::VTK::conforming);
    vtkwriter.addCellData(c,"level",nz);
    std::string path="";
    vtkwriter.pwrite(filename,path,path,vtkoutputtype);
  }


/* ********************************************************** *
 * Project continuous function onto grid function by 
 * evaluating the function at the centre of the cell.
 * ********************************************************** */
  template <class F>
  void project(const F &f)
  {
    Iterator it = this->begin();
    const Iterator endIt = this->end();
    for (;it!=endIt;++it)
    {
      const Entity &en = *it;
      typename Base::Geometry::GlobalCoordinate center = en.geometry().center();
      for (size_t i=0;i<NZ;++i)
      {
        typename Base::Geometry::GlobalCoordinate 
          scaled_center = center;
          scaled_center /= scaled_center.two_norm();
          scaled_center *= 0.5*(r_[i]+r_[i+1]);
        int idx = Base::index(en,0);
        dofs_[idx][i] = f(scaled_center);
      }
    }
    haloexchange();
  }

private:
  std::vector<double> r_;
  using Base::gv_;
  using Base::dofs_;
  std::vector<short int> interior_dofs_mask_;
  VectorExchange<typename GV::IndexSet,V> haloexchanger_;
};


/* *************************************************************** *
 * *************************************************************** *
 *
 * Hiearchical grid function
 *
 * *************************************************************** *
 * *************************************************************** */

template <int NZ, class G>
struct HierarchicDF
{
  typedef G Grid;
  typedef typename G::LevelGridView GridView;
  typedef DF<NZ,GridView> LevelDF;
  static const int dim = LevelDF::dim;
#if defined UGGRID
  typedef typename Dune::FieldVector<double,2> LocalCoordinate;
#else
  typedef typename LevelDF::Geometry::LocalCoordinate LocalCoordinate;
#endif // defined UGGRID
  /* *************************************************************** *
   * Constructors
   * *************************************************************** */
  HierarchicDF(const G &g,         // Underlying 2d grid
               int numberOfLevels  // Number of levels
              ) : 
    u_( numberOfLevels ),
    numberOfLevels_(numberOfLevels)
  {
    // Check that the grid has enough levels
    assert(g.maxLevel()+1-numberOfLevels>=0);
    // Construct a grid function on each level of the grid
    for (int l=0;l<numberOfLevels;++l)
    {
      u_[l] = new LevelDF(g.levelView(g.maxLevel()-l),l);
    }
  }

  /* *************************************************************** *
   * Return the number of levels
   * *************************************************************** */ 
  int numberOfLevels() const
  {
    return numberOfLevels_;
  }

  /* *************************************************************** *
   * [] operator returns grid function on specific level
   * *************************************************************** */
  const LevelDF &operator[](int l) const
  {
    return *(u_[l]);
  }
  LevelDF &operator[](int l)
  {
    return *(u_[l]);
  }

  /* *************************************************************** *
   * Private class data
   * *************************************************************** */
 private:
  std::vector< LevelDF* > u_;  // Vector of discrete grid functions,
                               // representing the field on different
                               // levels
  const int numberOfLevels_; // Number of levels
  // Temporary variables, used in interpolation
  std::vector<std::vector<double>> dx_;

  std::vector< LocalCoordinate > omega_;
  Dune::FieldVector< typename LevelDF::Geometry::GlobalCoordinate, NZ > gradient_;
};

/* *************************************************************** *
 * *************************************************************** *
 * Class for spherical harmonic function
 * *************************************************************** *
 * *************************************************************** */
class SphericalHarmonic {
 public:
  typedef Dune::FieldVector<double,3> FVector;
  SphericalHarmonic() : pi_(4.*atan2(1.,1.)) {}
  double operator()(const FVector& x) const {
    double rsq = x[0]*x[0]+x[1]*x[1]+x[2]*x[2]; 
    double phi = atan2(x[0],x[1]);
    double theta = atan2(sqrt(x[0]*x[0]+x[1]*x[1]),x[2]);
    double f = 0.5*sqrt(15./(2.*pi_))*sin(theta)*sin(theta)*cos(2.*phi) + 0.5*sqrt(3./pi_)*cos(theta);
    return f*rsq;
 } 
 private:
  double pi_;
};

// Print out extra information?
bool verbose = 1;

/* *************************************************************** *
 * Main program
 * *************************************************************** */
int main(int argc, char** argv)
{
  
  const int dim = 3;

  try{
    //Maybe initialize Mpi
    Dune::MPIHelper& helper = Dune::MPIHelper::instance(argc, argv);
    if (helper.rank() == 0) {
      std::cout<< "testsphericalgridgenerator" << std::endl;
      std::cout<< "Running on " << helper.size() << " processors." << std::endl;
    }
    if (argc != 3) {
      if (helper.rank() == 0) {
        std::cout << "Usage: " << argv[0] << "<filename> <refcount>" <<std::endl;
        std::cout << "   generate grid from file <filename> and refine <refcount> times." 
                  << std::endl;
      }
      exit(0);
    }

    // Read command line parameters
    std::string filename = argv[1];
    int refcount = atoi(argv[2]);
    if (helper.rank() == 0) {
      std::cout << "Reading grid from file " << filename << std::endl; 
    }
    typedef Dune::UGGrid<dim> Grid;
    Grid::setDefaultHeapSize(8000);
    SphericalGridGenerator gridgenerator(filename,refcount);
    Grid& grid = *(gridgenerator.getgridPtr());
    helper.getCollectiveCommunication().barrier();
    /* The following loadBalance causes the code to crash with 
       a segmentation fault.
       Note that in our case it is not necessary, as the load balancing
       is already ideal because each processor has exactly one 
       element of the macrogrid.
      */
    // grid.loadBalance(); 
    if (helper.rank() == 0) {
      std::cout << "Finished grid construction and load balancing" << std::endl;
    }
    typedef Grid::LeafGridView GV;
    // Create hierarchical grid function
    typedef HierarchicDF<nz,Grid> GridFunction;    
    GridFunction u(grid,refcount+1);
    // Project spherical harmonic onto each level
    // and save as vtk file
    SphericalHarmonic sphericalharmonic;
    for (int level=0;level<=refcount;++level) {
      u[level].project(sphericalharmonic);
      std::stringstream filename;
      filename << "elementdata_level_" << level;
      u[level].savetovtk(filename.str());
    }
    // Carry out nHalo halo exchanges on each level
    int nHalo=10;
    for (int level=0;level<=refcount;++level) {
      for (int i=0;i<nHalo;++i) {
        u[level].haloexchange();
      }
    }    
  }
  catch (Dune::Exception &e){
    std::cerr << "Dune reported error: " << e << std::endl;
  }
  catch (...){
    std::cerr << "Unknown exception thrown!" << std::endl;
  }
}

