/*** COPYRIGHT STATEMENT ************************************ 
 *
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2013]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  This code has been developed by
 *  - Andreas Dedner (University of Warwick)
 *  - Eike Mueller (University of Bath)
 *
 *** COPYRIGHT STATEMENT ************************************/
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include<iostream>
#include<vector>
#include<dune/common/mpihelper.hh>
#include<dune/common/exceptions.hh>
#include<dune/common/fvector.hh>
#include<dune/grid/common/scsgmapper.hh>
#include<dune/grid/io/file/vtk/vtkwriter.hh>
#include<dune/grid/uggrid.hh>
#include"sphericalgridgenerator.hh"

// Print out extra information?
bool verbose = 1;

/* *************************************************************** *
 * P0 Layout class is needed for attaching data to grid with
 * mapper.
 * *************************************************************** */
template < int dim >
struct P0Layout
{
  bool contains ( Dune::GeometryType gt )
  {
   if ( gt.dim ()== dim ) return true ;
     return false ;
  }
};


/* *************************************************************** *
 * Main program
 * *************************************************************** */
int main(int argc, char** argv)
{
  
  const int dim = 3;

  try{
    //Maybe initialize Mpi
    Dune::MPIHelper& helper = Dune::MPIHelper::instance(argc, argv);
    if (helper.rank() == 0) {
      std::cout<< "testsphericalgridgenerator" << std::endl;
      std::cout<< "Running on " << helper.size() << " processors." << std::endl;
    }
    if (argc != 3) {
      if (helper.rank() == 0) {
        std::cout << "Usage: " << argv[0] << "<filename> <refcount>" <<std::endl;
        std::cout << "   generate grid from file <filename> and refine <refcount> times." 
                  << std::endl;
      }
      exit(0);
    }

    // Read command line parameters
    std::string filename = argv[1];
    int refcount = atoi(argv[2]);
    if (helper.rank() == 0) {
      std::cout << "Reading grid from file " << filename << std::endl; 
    }
    typedef Dune::UGGrid<dim> Grid;
    Grid::setDefaultHeapSize(8000);
    SphericalGridGenerator gridgenerator(filename,refcount);
    Grid& grid = *(gridgenerator.getgridPtr());
    helper.getCollectiveCommunication().barrier();
    if (helper.rank() == 0) {
      std::cout << "Finished grid construction and load balancing" << std::endl;
    }
    // Loop over grid and attach user data
    typedef Grid::LeafGridView GV ;
    typedef Grid::ctype Coord;
    typedef GV::Codim<0>::Partition<Dune::Interior_Partition>::Iterator 
      ElementLeafIterator; 
    typedef ElementLeafIterator::Entity::Geometry LeafGeometry;
    GV gv = grid.leafView();
    Dune::LeafMultipleCodimMultipleGeomTypeMapper <Grid, P0Layout> mapper(grid);
    std::vector<double> c(mapper.size());
    ElementLeafIterator ibegin = gv.begin<0,Dune::Interior_Partition>();
    ElementLeafIterator iend = gv.end<0,Dune::Interior_Partition>();
    srand(1);
    int nInterior=0;
    for (ElementLeafIterator it = ibegin;it!=iend;++it,++nInterior) {
      const LeafGeometry geo = it->geometry();
      Dune::FieldVector<Coord,dim> global = geo.center();
      double x = global[0];
      double y = global[1];
      double z = global[2];
      double phi = atan2(x,y);
      double theta = atan2(sqrt(x*x+y*y),z);
      double pi = 4.*atan2(1.,1.);      
//      c[mapper.map(*it)] = (rand() % mapper.size());
      c[mapper.map(*it)] = 0.5*sqrt(15./(2.*pi))*sin(theta)*sin(theta)*cos(2.*phi) + 0.5*sqrt(3./pi)*cos(theta);
    }
    if (verbose) {
      std::cout << "Number of elements on process " << helper.rank() << " : " << nInterior << std::endl;
    }
    
    // Write to vtu file elementdata.vtu for visualisation
    /*
    Dune::VTKWriter<GV> vtkwriter(gv);
    vtkwriter.addCellData(c,"data");
    vtkwriter.write("elementdata",Dune::VTK::ascii);
    */
  }
  catch (Dune::Exception &e){
    std::cerr << "Dune reported error: " << e << std::endl;
  }
  catch (...){
    std::cerr << "Unknown exception thrown!" << std::endl;
  }
}

