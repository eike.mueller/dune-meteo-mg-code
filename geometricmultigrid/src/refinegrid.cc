/*** COPYRIGHT STATEMENT ************************************ 
 *
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2013]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  This code has been developed by
 *  - Andreas Dedner (University of Warwick)
 *  - Eike Mueller (University of Bath)
 *
 *** COPYRIGHT STATEMENT ************************************/
/* *************************************************************** *
 *
 *  Read icosahedral grid from dgf file
 *
 * *************************************************************** */

#ifdef HAVE_CONFIG_H
# include "config.h"     
#endif
#include<iostream>
#include<vector>
#include<dune/common/mpihelper.hh> // An initializer of MPI
#include<dune/common/exceptions.hh> // We use exceptions
#include<dune/common/fvector.hh>

#include <dune/grid/alugrid.hh>
#include <dune/grid/io/file/dgfparser/dgfalu.hh>
#include <dune/grid/io/file/dgfparser/dgfwriter.hh>
#include <dune/grid/common/scsgmapper.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>

const int dim = 2;
const int dimworld = 3;

/* *************************************************************** *
 * Main program
 * *************************************************************** */
int main(int argc, char** argv)
{
  try{
    //Maybe initialize Mpi
    Dune::MPIHelper& helper = Dune::MPIHelper::instance(argc, argv);
    if(Dune::MPIHelper::isFake)
      std::cout<< "Refinegridgrid [sequential version]" << std::endl;
    else
      if (helper.rank() == 0) {
        std::cout<< "Refinegrid running on "<< helper.size()<<" processes."<<std::endl;
      }

    if ( (argc <4) || (argc > 5) ){
      std::cout << "Usage: " << argv[0] << " <infilename> <outfilename> <refinementlevels> [<sphericalprojection>]" << std::endl;
      return 0;
    }
    // Read command line parameters
    std::string inFilename = argv[1];
    std::string outFilename = argv[2];    
    int reflevels = atoi(argv[3]);
    std::string projectStr = argv[4];
    bool sphericalProjection = false;
    if (argc > 4) sphericalProjection = !(projectStr.compare("true"));

    // Create grid
    typedef Dune::GridSelector::GridType Grid;
    Dune::GridPtr<Grid> gridPtr(inFilename);
    Grid& grid = *gridPtr;
    typedef Grid::LeafGridView GV ;
    grid.globalRefine(reflevels);

    // Loop over grid and attach user data
    typedef Grid::ctype Coord;
    Dune::LeafSingleCodimSingleGeomTypeMapper<Grid,0> mapper(grid);
    std::vector<double> c(mapper.size());
    typedef GV::Codim<0>::Iterator ElementLeafIterator;
    typedef ElementLeafIterator::Entity::Geometry LeafGeometry;
    GV gv = grid.leafView();
    ElementLeafIterator ibegin = gv.begin<0>();
    ElementLeafIterator iend = gv.end<0>();
    for (ElementLeafIterator it = ibegin;it!=iend;++it) {
      const LeafGeometry geo = it->geometry();
      Dune::FieldVector<Coord,dimworld> global = geo.center();
      c[mapper.map(*it)] = 1.0;
    }

    // Write to vtu file for visualisation
    Dune::VTKWriter<GV> vtkwriter(gv);
    vtkwriter.addCellData(c,"data");
    vtkwriter.write("elementdata",Dune::VTK::appendedbase64);

    // Save to new dgf file
    Dune::DGFWriter<GV> dgfwriter(gv);
    std::ofstream gridout(outFilename.c_str());
    dgfwriter.write(gridout);
    if (sphericalProjection) {
      gridout << "PROJECTION" << std::endl;
      gridout << "function p(x) = x / |x|" << std::endl;
      gridout << "default p" << std::endl;
      gridout << "#" << std::endl;
    }
    gridout.close();
  }
  catch (Dune::Exception &e){
    std::cerr << "Dune reported error: " << e << std::endl;
  }
  catch (...){
    std::cerr << "Unknown exception thrown!" << std::endl;
  }
}
