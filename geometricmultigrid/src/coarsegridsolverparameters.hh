/*** COPYRIGHT STATEMENT ************************************ 
 *
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2013]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  This code has been developed by
 *  - Andreas Dedner (University of Warwick)
 *  - Eike Mueller (University of Bath)
 *
 *** COPYRIGHT STATEMENT ************************************/

#ifndef COARSEGRIDSOLVERPARAMETERS_HH
#define COARSEGRIDSOLVERPARAMETERS_HH
/* ********************************************************** *
 *
 *  Parameters of coarse grid solver
 *
 * ********************************************************** */
#include<parameters.hh>

class CoarsegridSolverParameters : public Parameters {
 public:
  // Constructor
  CoarsegridSolverParameters() :
                  Parameters("coarsegridsolver"),
                  niter_(1) {
      addKey("niter",Integer);
    }

  //
  int readFile(const std::string filename) {
    int readSuccess = Parameters::readFile(filename);
    if (!readSuccess) {
      niter_ = contents["niter"]->getInt();
    }
    return readSuccess;
  }

  // Return data
  int niter() const { return niter_; }

 private:
  int niter_;
};

#endif // COARSEGRIDSOLVERPARAMETERS_HH
