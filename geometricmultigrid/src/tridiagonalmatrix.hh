/*** COPYRIGHT STATEMENT ************************************ 
 *
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2013]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  This code has been developed by
 *  - Andreas Dedner (University of Warwick)
 *  - Eike Mueller (University of Bath)
 *
 *** COPYRIGHT STATEMENT ************************************/

// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
#ifndef DUNE_TRIDIAGONAL_MATRIX_HH
#define DUNE_TRIDIAGONAL_MATRIX_HH

/*! \file
\brief  This file implements a quadratic diagonal matrix of fixed size.
*/

#include<cmath>
#include<cstddef>
#include<complex>
#include<iostream>
#include<memory>
#include <dune/common/exceptions.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>
#include <dune/common/typetraits.hh>
#include <dune/common/genericiterator.hh>

#include <dune/istl/diagonalmatrix.hh>


namespace Dune {

template< class K, int n > class TriDiagonalRowVectorConst;
template< class K, int n > class TriDiagonalRowVector;
template< class TriDiagonalMatrixType > class TriDiagonalMatrixWrapper;
template< class C, class T, class R> class ContainerWrapperIterator;

/**
    @addtogroup DenseMatVec
    @{
*/

/**
 *@brief A tridiagonal matrix of static size.
 *
 * This is meant to be a replacement of FieldMatrix for the case that
 * it is a tridiagonal matrix.
 *
 * \tparam K Type used for scalars
 * \tparam n Matrix size
 */
template<class K, int n>
class TriDiagonalMatrix
{
    typedef TriDiagonalMatrixWrapper< TriDiagonalMatrix<K,n> > WrapperType;

    public:
    //===== type definitions and constants

    //! export the type representing the field
    typedef K field_type;

    //! export the type representing the components
    typedef K block_type;

    //! The type used for the index access and size operations.
    typedef std::size_t size_type;

    //! We are at the leaf of the block recursion
    enum {
        //! The number of block levels we contain. This is 1.
        blocklevel = 1
    };

    //! Each row is implemented by a field vector
    typedef TriDiagonalRowVector<K,n> row_type;
    typedef row_type reference;
    typedef TriDiagonalRowVectorConst<K,n> const_row_type;
    typedef const_row_type const_reference;

    //! export size
    enum {
        //! The number of rows
        rows = n,
        //! The number of columns
        cols = n
    };



    //===== constructors

    //! Default constructor
    TriDiagonalMatrix () : isDiag_(false) {}

    //! Constructor initializing the whole matrix with a scalar
    TriDiagonalMatrix (const K& k)
        : diag_(k), lower_(k), upper_(k), isDiag_(true)
    {}

    //! Constructor initializing the diagonal with a vector
    TriDiagonalMatrix (const FieldVector<K,n>& diag)
        : diag_(diag) , lower_(0), upper_(0), isDiag_(true)
    {}

    //! Constructor initializing the diagonal, lower and upper offdiagonalwith three vectors
    TriDiagonalMatrix (const FieldVector<K,n>& diag,
                       const FieldVector<K,n-1>& upper, const FieldVector<K,n-1>& lower)
        : diag_(diag) , lower_(lower), upper_(upper), isDiag_(false)
    {}

    /** \brief Assignment from a scalar */
    TriDiagonalMatrix& operator= (const K& k)
    {
        diag_ = k;
        upper_ = k;
        lower_ = k;
        isDiag_ = true;
        return *this;
    }

    /** \brief Check if matrix is the same object as the other matrix */
    bool identical(const TriDiagonalMatrix<K,n>& other) const
    {
        return (this==&other);
    }

    //===== iterator interface to rows of the matrix
    //! Iterator class for sequential access
    typedef ContainerWrapperIterator<const WrapperType, reference, reference> Iterator;
    //! typedef for stl compliant access
    typedef Iterator iterator;
    //! rename the iterators for easier access
    typedef Iterator RowIterator;
    //! rename the iterators for easier access
    typedef typename row_type::Iterator ColIterator;

    //! begin iterator
    Iterator begin ()
    {
        return Iterator(WrapperType(this),0);
    }

    //! end iterator
    Iterator end ()
    {
        return Iterator(WrapperType(this),n);
    }

    //! @returns an iterator that is positioned before
    //! the end iterator of the rows, i.e. at the last row.
	Iterator beforeEnd ()
    {
        return Iterator(WrapperType(this),n-1);
    }

    //! @returns an iterator that is positioned before
    //! the first row of the matrix.
	Iterator beforeBegin ()
    {
        return Iterator(WrapperType(this),-1);
    }


    //! Iterator class for sequential access
    typedef ContainerWrapperIterator<const WrapperType, const_reference, const_reference> ConstIterator;
    //! typedef for stl compliant access
    typedef ConstIterator const_iterator;
    //! rename the iterators for easier access
    typedef ConstIterator ConstRowIterator;
    //! rename the iterators for easier access
    typedef typename const_row_type::ConstIterator ConstColIterator;

    //! begin iterator
    ConstIterator begin () const
    {
        return ConstIterator(WrapperType(this),0);
    }

    //! end iterator
    ConstIterator end () const
    {
        return ConstIterator(WrapperType(this),n);
    }

    //! @returns an iterator that is positioned before
    //! the end iterator of the rows. i.e. at the last row.
	ConstIterator beforeEnd() const
    {
        return ConstIterator(WrapperType(this),n-1);
    }

    //! @returns an iterator that is positioned before
    //! the first row of the matrix.
	ConstIterator beforeBegin () const
    {
        return ConstIterator(WrapperType(this),-1);
    }



    //===== vector space arithmetic

    //! vector space addition
    TriDiagonalMatrix& operator+= (const TriDiagonalMatrix& y)
    {
        diag_  += y.diag_;
        upper_ += y.upper_;
        lower_ += y.lower_;
        isDiag_ = isDiag_ && y.isDiag_;
        return *this;
    }

    //! vector space subtraction
    TriDiagonalMatrix& operator-= (const TriDiagonalMatrix& y)
    {
        diag_  -= y.diag_;
        upper_ -= y.upper_;
        lower_ -= y.lower_;
        isDiag_ = isDiag_ && y.isDiag_;
        return *this;
    }

    //! vector space addition of scalar
    TriDiagonalMatrix& operator+= (const K& k)
    {
        diag_ += k;
        return *this;
    }

    //! vector space subtraction of scalar
    TriDiagonalMatrix& operator-= (const K& k)
    {
        diag_ -= k;
        return *this;
    }

    //! vector space multiplication with scalar
    TriDiagonalMatrix& operator*= (const K& k)
    {
        diag_  *= k;
        if (isDiag_)
        {
          upper_ *= k;
          lower_ *= k;
        }
        return *this;
    }

    //! vector space division by scalar
    TriDiagonalMatrix& operator/= (const K& k)
    {
        diag_  /= k;
        if (isDiag_)
        {
          upper_ /= k;
          lower_ /= k;
        }
        return *this;
    }

    //===== comparison ops

    //! comparison operator
    bool operator==(const TriDiagonalMatrix& other) const
    {
        return ( diag_==other.diagonal() && upper_==other.upper() && lower_==other.lower() );
    }

    //! incomparison operator
    bool operator!=(const TriDiagonalMatrix& other) const
    {
        return ( diag_!=other.diagonal() || upper_!=other.upper() || lower_!=other.lower() );
    }


    //===== linear maps

    //! y = A x
    template<class X, class Y>
    void mv (const X& x, Y& y) const
    {
#ifdef DUNE_FMatrix_WITH_CHECKING
        if (x.N()!=M()) DUNE_THROW(FMatrixError,"index out of range");
        if (y.N()!=N()) DUNE_THROW(FMatrixError,"index out of range");
#endif
        if (isDiag_)
        {
          for (size_type i=0; i<n; ++i)
              y[i] = diag_[i] * x[i];
        }
        else
        {
          y[0] = 0;
          for (size_type i=0; i<n-1; ++i)
          {
            y[i]  += diag_[i] * x[i] + upper_[i] * x[i+1];
            y[i+1] = lower_[i] * x[i];
          }
          y[n-1]  += diag_[n-1] * x[n-1];
        }
    }

    //! y = A^T x
    template<class X, class Y>
    void mtv (const X& x, Y& y) const
    {
#ifdef DUNE_FMatrix_WITH_CHECKING
        if (x.N()!=M()) DUNE_THROW(FMatrixError,"index out of range");
        if (y.N()!=N()) DUNE_THROW(FMatrixError,"index out of range");
#endif
        if (isDiag_)
        {
          for (size_type i=0; i<n; ++i)
              y[i] = diag_[i] * x[i];
        }
        else
        {
          y[0] = 0;
          for (size_type i=0; i<n-1; ++i)
          {
            y[i]  += diag_[i] * x[i] + lower_[i] * x[i+1];
            y[i+1] = upper_[i] * x[i];
          }
          y[n-1]  += diag_[n-1] * x[n-1];
        }
    }

    //! y += A x
    template<class X, class Y>
    void umv (const X& x, Y& y) const
    {
#ifdef DUNE_FMatrix_WITH_CHECKING
        if (x.N()!=M()) DUNE_THROW(FMatrixError,"index out of range");
        if (y.N()!=N()) DUNE_THROW(FMatrixError,"index out of range");
#endif
        if (isDiag_)
        {
          for (size_type i=0; i<n; ++i)
              y[i] += diag_[i] * x[i];
        }
        else
        {
          for (size_type i=0; i<n-1; ++i)
          {
            y[i]   += diag_[i] * x[i] + upper_[i] * x[i+1];
            y[i+1] += lower_[i] * x[i];
          }
          y[n-1]   += diag_[n-1] * x[n-1];
        }
    }

    //! y += A^T x
    template<class X, class Y>
    void umtv (const X& x, Y& y) const
    {
#ifdef DUNE_FMatrix_WITH_CHECKING
        if (x.N()!=N()) DUNE_THROW(FMatrixError,"index out of range");
        if (y.N()!=M()) DUNE_THROW(FMatrixError,"index out of range");
#endif
        if (isDiag_)
        {
          for (size_type i=0; i<n; ++i)
              y[i] += diag_[i] * x[i];
        }
        else
        {
          for (size_type i=0; i<n-1; ++i)
          {
            y[i]   += diag_[i] * x[i] + lower_[i] * x[i+1];
            y[i+1] += upper_[i] * x[i];
          }
          y[n-1]   += diag_[n-1] * x[n-1];
        }
    }

    //! y += A^H x
    template<class X, class Y>
    void umhv (const X& x, Y& y) const
    {
#ifdef DUNE_FMatrix_WITH_CHECKING
        if (x.N()!=N()) DUNE_THROW(FMatrixError,"index out of range");
        if (y.N()!=M()) DUNE_THROW(FMatrixError,"index out of range");
#endif
        if (isDiag_)
        {
          for (size_type i=0; i<n; ++i)
              y[i] += conjugateComplex(diag_[i]) * x[i];
        }
        else
        {
          for (size_type i=0; i<n-1; ++i)
          {
            y[i]   += diag_[i] * x[i] + conjugateComplex(lower_[i]) * x[i+1];
            y[i+1] += conjugateComplex(upper_[i]) * x[i];
          }
          y[n-1]   += conjugateComplex(diag_[n-1]) * x[n-1];
        }
    }

    //! y -= A x
    template<class X, class Y>
    void mmv (const X& x, Y& y) const
    {
#ifdef DUNE_FMatrix_WITH_CHECKING
        if (x.N()!=M()) DUNE_THROW(FMatrixError,"index out of range");
        if (y.N()!=N()) DUNE_THROW(FMatrixError,"index out of range");
#endif
        if (isDiag_)
        {
          for (size_type i=0; i<n; ++i)
              y[i] -= diag_[i] * x[i];
        }
        else
        {
          for (size_type i=0; i<n-1; ++i)
          {
            y[i]   -= diag_[i] * x[i] + upper_[i] * x[i+1];
            y[i+1] -= lower_[i] * x[i];
          }
          y[n-1]   -= diag_[n-1] * x[n-1];
        }
    }

    //! y -= A^T x
    template<class X, class Y>
    void mmtv (const X& x, Y& y) const
    {
#ifdef DUNE_FMatrix_WITH_CHECKING
        if (x.N()!=N()) DUNE_THROW(FMatrixError,"index out of range");
        if (y.N()!=M()) DUNE_THROW(FMatrixError,"index out of range");
#endif
        if (isDiag_)
        {
          for (size_type i=0; i<n; ++i)
              y[i] -= diag_[i] * x[i];
        }
        else
        {
          for (size_type i=0; i<n-1; ++i)
          {
            y[i]   -= diag_[i] * x[i] + lower_[i] * x[i+1];
            y[i+1] -= upper_[i] * x[i];
          }
          y[n-1]   -= diag_[n-1] * x[n-1];
        }
    }

    //! y -= A^H x
    template<class X, class Y>
    void mmhv (const X& x, Y& y) const
    {
#ifdef DUNE_FMatrix_WITH_CHECKING
        if (x.N()!=N()) DUNE_THROW(FMatrixError,"index out of range");
        if (y.N()!=M()) DUNE_THROW(FMatrixError,"index out of range");
#endif
        if (isDiag_)
        {
          for (size_type i=0; i<n; ++i)
              y[i] -= conjugateComplex(diag_[i]) * x[i];
        }
        else
        {
          for (size_type i=0; i<n-1; ++i)
          {
            y[i]   -= diag_[i] * x[i] + conjugateComplex(lower_[i]) * x[i+1];
            y[i+1] -= conjugateComplex(upper_[i]) * x[i];
          }
          y[n-1]   -= conjugateComplex(diag_[n-1]) * x[n-1];
        }
    }

    //! y += alpha A x
    template<class X, class Y>
    void usmv (const K& alpha, const X& x, Y& y) const
    {
#ifdef DUNE_FMatrix_WITH_CHECKING
        if (x.N()!=M()) DUNE_THROW(FMatrixError,"index out of range");
        if (y.N()!=N()) DUNE_THROW(FMatrixError,"index out of range");
#endif
        if (isDiag_)
        {
          for (size_type i=0; i<n; ++i)
              y[i] += alpha * diag_[i] * x[i];
        }
        else
        {
          for (size_type i=0; i<n-1; ++i)
          {
            y[i]   += alpha * (diag_[i] * x[i] + upper_[i] * x[i+1]);
            y[i+1] += alpha * (lower_[i] * x[i]);
          }
          y[n-1]   += alpha * diag_[n-1] * x[n-1];
        }
    }

    //! y += alpha A^T x
    template<class X, class Y>
    void usmtv (const K& alpha, const X& x, Y& y) const
    {
#ifdef DUNE_FMatrix_WITH_CHECKING
        if (x.N()!=N()) DUNE_THROW(FMatrixError,"index out of range");
        if (y.N()!=M()) DUNE_THROW(FMatrixError,"index out of range");
#endif
        if (isDiag_)
        {
          for (size_type i=0; i<n; ++i)
              y[i] += alpha * diag_[i] * x[i];
        }
        else
        {
          for (size_type i=0; i<n-1; ++i)
          {
            y[i]   += alpha * (diag_[i] * x[i] + lower_[i] * x[i+1]);
            y[i+1] += alpha * (upper_[i] * x[i]);
          }
          y[n-1]   += alpha * diag_[n-1] * x[n-1];
        }
    }

    //! y += alpha A^H x
    template<class X, class Y>
    void usmhv (const K& alpha, const X& x, Y& y) const
    {
#ifdef DUNE_FMatrix_WITH_CHECKING
        if (x.N()!=N()) DUNE_THROW(FMatrixError,"index out of range");
        if (y.N()!=M()) DUNE_THROW(FMatrixError,"index out of range");
#endif
        if (isDiag_)
        {
          for (size_type i=0; i<n; ++i)
              y[i] += alpha * conjugateComplex(diag_[i]) * x[i];
        }
        else
        {
          for (size_type i=0; i<n-1; ++i)
          {
            y[i]   += alpha * (conjugateComplex(diag_[i]) * x[i] + conjugateComplex(lower_[i]) * x[i+1]);
            y[i+1] += alpha * conjugateComplex(upper_[i]) * x[i];
          }
          y[n-1]   += alpha * conjugateComplex(diag_[n-1]) * x[n-1];
        }
    }

    //===== norms

    //! frobenius norm: sqrt(sum over squared values of entries)
    double frobenius_norm () const
    {
        return sqrt(diag_.two_norm2()+upper_.two_norm2()+lower_.two_norm2());
    }

    //! square of frobenius norm, need for block recursion
    double frobenius_norm2 () const
    {
        return diag_.two_norm2()+upper_.two_norm2()+lower_.two_norm2();
    }

    //! infinity norm (row sum norm, how to generalize for blocks?)
    double infinity_norm () const
    {
        return std::max( std::max( diag_.infinity_norm(), upper_.infinity_norm()),lower_.infinity_norm());
    }

    //! simplified infinity norm (uses Manhattan norm for complex values)
    double infinity_norm_real () const
    {
        return std::max( std::max( diag_.infinity_norm_real(), upper_.infinity_norm_real()),lower_.infinity_norm_real());
    }



    //===== solve

    //! Solve system A x = b
    template<class V>
    void solve (V& x, const V& b) const
    {
      if (isDiag_ || n==1)
      {
        for (int i=0;i<n;++i)
          x[i] = b[i] / diag_[i];
        return;
      }
      V d,rhs;
      d[0]    = diag_[0];
      rhs[0]  = b[0];
      for (int i = 1; i < n; ++i)
      {
        double m = lower_[i-1]/d[i-1];
        d[i]   = diag_[i] - m * upper_[i-1];
        rhs[i] = b[i]     - m * rhs[i-1];
      }
      x[n-1] = rhs[n-1]/d[n-1];
      for (int i = n - 2; i >= 0; --i)
        x[i] = (rhs[i] - upper_[i] * x[i+1]) / d[i];
    }

    //===== sizes

    //! number of blocks in row direction
    size_type N () const
    {
        return n;
    }

    //! number of blocks in column direction
    size_type M () const
    {
        return n;
    }

    //===== query

    //! return true when (i,j) is in pattern
    bool exists (size_type i, size_type j) const
    {
#ifdef DUNE_FMatrix_WITH_CHECKING
        if (i<0 || i>=n) DUNE_THROW(FMatrixError,"row index out of range");
        if (j<0 || j>=n) DUNE_THROW(FMatrixError,"column index out of range");
#endif
        return (i==j) || (i==j-1) || (i==j+1);
    }



    //! Sends the matrix to an output stream
    friend std::ostream& operator<< (std::ostream& s, const TriDiagonalMatrix<K,n>& a)
    {
        abort();
        for (size_type i=0; i<n; i++) {
            for (size_type j=0; j<n; j++)
                s << ((i==j) ? a.diag_[i] : 0) << " ";
            s << std::endl;
        }
        return s;
    }

    //! Return reference object as row replacement
    reference operator[](size_type i)
    {
      if (i==0)
        return reference(0,const_cast<K*>(&diag_[i]),const_cast<K*>(&upper_[i]), i);
      else if (i==n-1)
        return reference(const_cast<K*>(&lower_[i]),const_cast<K*>(&diag_[i]),0, i);
      else
        return reference(const_cast<K*>(&lower_[i]),const_cast<K*>(&diag_[i]),const_cast<K*>(&upper_[i]), i);
    }

    //! Return const_reference object as row replacement
    const_reference operator[](size_type i) const
    {
      if (i==0)
        return const_reference(0,const_cast<K*>(&diag_[i]),const_cast<K*>(&upper_[i]), i);
      else if (i==n-1)
        return const_reference(const_cast<K*>(&lower_[i]),const_cast<K*>(&diag_[i]),0, i);
      else
        return const_reference(const_cast<K*>(&lower_[i]),const_cast<K*>(&diag_[i]),const_cast<K*>(&upper_[i]), i);
    }

    //! Get const reference to diagonal vector
    const FieldVector<K,n>& diagonal() const
    {
        return diag_;
    }

    //! Get reference to diagonal vector
    FieldVector<K,n>& diagonal()
    {
        return diag_;
    }

    //! Get const reference to upper offdiagonal vector
    const FieldVector<K,n-1>& upper() const
    {
        return upper_;
    }

    //! Get reference to upper offdiagonal vector
    FieldVector<K,n-1>& upper()
    {
        return upper_;
    }

    //! Get const reference to lower offdiagonal vector
    const FieldVector<K,n-1>& lower() const
    {
        return lower_;
    }

    //! Get reference to lower offdiagonal vector
    FieldVector<K,n-1>& lower()
    {
        return lower_;
    }

    private:

    // the data, a FieldVector storing the diagonal
    FieldVector<K,n> diag_;
    FieldVector<K,n-1> lower_, upper_;
    bool isDiag_;
};

template<class TriDiagonalMatrixType>
class TriDiagonalMatrixWrapper
{
        typedef typename TriDiagonalMatrixType::reference reference;
        typedef typename TriDiagonalMatrixType::const_reference const_reference;
        typedef typename TriDiagonalMatrixType::field_type K;
        typedef TriDiagonalRowVector<K, TriDiagonalMatrixType::rows> row_type;
        typedef std::size_t size_type;
        typedef TriDiagonalMatrixWrapper< TriDiagonalMatrixType> MyType;

        friend class ContainerWrapperIterator<const MyType, reference, reference>;
        friend class ContainerWrapperIterator<const MyType, const_reference, const_reference>;

    public:

        TriDiagonalMatrixWrapper() :
            mat_(0)
        {}

        TriDiagonalMatrixWrapper(const TriDiagonalMatrixType* mat) :
            mat_(const_cast<TriDiagonalMatrixType*>(mat))
        {}

        size_type realIndex(int i) const
        {
            return i;
        }

        row_type* pointer(int i) const
        {
            row_ = row_type(&(mat_->diagonal()[i]),
                            &(mat_->upper()[i]),
                            &(mat_->lower()[i]),
                            i);
            return &row_;
        }

        bool identical(const TriDiagonalMatrixWrapper& other) const
        {
            return mat_==other.mat_;
        }

    private:

        mutable TriDiagonalMatrixType* mat_;
        mutable row_type row_;
};

/** \brief
 *
 */
template< class K, int n >
class TriDiagonalRowVectorConst
{
    template<class TriDiagonalMatrixType>
    friend class TriDiagonalMatrixWrapper;
    friend class ContainerWrapperIterator<TriDiagonalRowVectorConst<K,n>, const K, const K&>;

public:
    // remember size of vector
    enum { dimension = n };

    // standard constructor and everything is sufficient ...

    //===== type definitions and constants

    //! export the type representing the field
    typedef K field_type;

    //! export the type representing the components
    typedef K block_type;

    //! The type used for the index access and size operation
    typedef std::size_t size_type;

    //! We are at the leaf of the block recursion
    enum {
        //! The number of block levels we contain
        blocklevel = 1
    };

    //! export size
    enum {
        //! The size of this vector.
        size = n
    };

    //! Constructor making uninitialized vector
    TriDiagonalRowVectorConst() :
        d_(0), u_(0), l_(0),
        row_(0)
    {}

    //! Constructor making vector with identical coordinates
    explicit TriDiagonalRowVectorConst (K* l, K* d, K* u, int col) :
        d_(d), u_(u), l_(l),
        row_(col)
    {}

    //===== access to components

    //! same for read only access
    const K& operator[] (size_type i) const
    {
#ifdef DUNE_FMatrix_WITH_CHECKING
        if (i!=row_i && i!=row_-1 && i!=row_+1)
            DUNE_THROW(FMatrixError,"index is contained in pattern");
#endif
        return (i==row_+1) ? *u_ : (i==row_-1) ? *l_ : *d_ ;
    }

    // check if row is identical to other row (not only identical values)
    // since this is a proxy class we need to check equality of the stored pointer
    bool identical(const TriDiagonalRowVectorConst<K,n>& other) const
    {
        return ((d_ == other.d_) and (u_ == other.u_) and (l_ == other.l_) and (row_ == other.row_));
    }

    //! ConstIterator class for sequential access
    typedef ContainerWrapperIterator<TriDiagonalRowVectorConst<K,n>, const K, const K&> ConstIterator;
    //! typedef for stl compliant access
    typedef ConstIterator const_iterator;

    //! begin ConstIterator
    ConstIterator begin () const
    {
      if (row_ == 0)
        return ConstIterator(*this,1);
      else
        return ConstIterator(*this,0);
    }

    //! end ConstIterator
    ConstIterator end () const
    {
      if (row_ == n)
        return ConstIterator(*this,2);
      else
        return ConstIterator(*this,3);
    }

    //! @returns an iterator that is positioned before
    //! the end iterator of the rows. i.e. at the row.
	ConstIterator beforeEnd() const
    {
      if (row_ == n)
        return ConstIterator(*this,1);
      else
        return ConstIterator(*this,2);
    }

    //! @returns an iterator that is positioned before
    //! the first row of the matrix.
	ConstIterator beforeBegin () const
    {
      if (row_ == 0)
        return ConstIterator(*this,0);
      else
        return ConstIterator(*this,-1);
    }

    //! Binary vector comparison
    bool operator== (const TriDiagonalRowVectorConst& y) const
    {
        return ((d_==y.d_) and (u_==y.u_) and (l_==y.l_) and (row_==y.row_));
    }

    //===== sizes

    //! number of blocks in the vector (are of size 1 here)
    size_type N () const
    {
        return n;
    }

    //! dimension of the vector space
    size_type dim () const
    {
        return n;
    }

    //! index of this row in surrounding matrix
    size_type rowIndex() const
    {
        return row_;
    }

protected:

    size_type realIndex(int i) const
    {
        return rowIndex();
    }

    K* pointer(size_type i) const
    {
      if (i==0)
        return const_cast<K*>(l_);
      else if (i==1)
        return const_cast<K*>(d_);
      else if (i==2)
        return const_cast<K*>(u_);
    }

    TriDiagonalRowVectorConst* operator&()
    {
        return this;
    }

    // the data, very simply a pointer to the diagonal value and the row number
    K* d_;
    K* u_;
    K* l_;
    size_type row_;
};

template< class K, int n >
class TriDiagonalRowVector : public TriDiagonalRowVectorConst<K,n>
{
    template<class TriDiagonalMatrixType>
    friend class TriDiagonalMatrixWrapper;
    friend class ContainerWrapperIterator<TriDiagonalRowVector<K,n>, K, K&>;

public:
    // standard constructor and everything is sufficient ...

    //===== type definitions and constants

    //! export the type representing the field
    typedef K field_type;

    //! export the type representing the components
    typedef K block_type;

    //! The type used for the index access and size operation
    typedef std::size_t size_type;

    //! Constructor making uninitialized vector
    TriDiagonalRowVector() : TriDiagonalRowVectorConst<K,n>()
    {}

    //! Constructor making vector with identical coordinates
    explicit TriDiagonalRowVector (K* l, K* d, K* u, int col) : TriDiagonalRowVectorConst<K,n>(l,d,u, col)
    {}

    //===== assignment from scalar
    //! Assignment operator for scalar
    TriDiagonalRowVector& operator= (const K& k)
    {
        *d_ = k;
        *l_ = k;
        *u_ = k;
        return *this;
    }

    //===== access to components

    //! random access
    K& operator[] (size_type i)
    {
#ifdef DUNE_FMatrix_WITH_CHECKING
        if (i!=row_i && i!=row_-1 && i!=row_+1)
            DUNE_THROW(FMatrixError,"index is contained in pattern");
#endif
        // if (i==row_+1) return *u_;
        // else if (i==row_-1) return *l_;
        // else return *d_;
        return (i==row_+1) ? *u_ : ( (i==row_-1) ? *l_ : *d_ ) ;
    }

    //! Iterator class for sequential access
    typedef ContainerWrapperIterator<TriDiagonalRowVector<K,n>, K, K&> Iterator;
    //! typedef for stl compliant access
    typedef Iterator iterator;

    //! begin iterator
    Iterator begin ()
    {
      if (row_ == 0)
        return Iterator(*this,1);
      else
        return Iterator(*this,0);
    }

    //! end iterator
    Iterator end ()
    {
      if (row_ == n)
        return Iterator(*this,2);
      else
        return Iterator(*this,3);
    }

    //! @returns an iterator that is positioned before
    //! the end iterator of the rows, i.e. at the last row.
	Iterator beforeEnd ()
    {
      if (row_ == n)
        return Iterator(*this,1);
      else
        return Iterator(*this,2);
    }

    //! @returns an iterator that is positioned before
    //! the first row of the matrix.
	Iterator beforeBegin ()
    {
      if (row_ == 0)
        return Iterator(*this,0);
      else
        return Iterator(*this,-1);
    }

    //! ConstIterator class for sequential access
    typedef ContainerWrapperIterator<TriDiagonalRowVectorConst<K,n>, const K, const K&> ConstIterator;
    //! typedef for stl compliant access
    typedef ConstIterator const_iterator;

    using TriDiagonalRowVectorConst<K,n>::identical;
    using TriDiagonalRowVectorConst<K,n>::operator[];
    using TriDiagonalRowVectorConst<K,n>::operator==;
    using TriDiagonalRowVectorConst<K,n>::begin;
    using TriDiagonalRowVectorConst<K,n>::end;
    using TriDiagonalRowVectorConst<K,n>::beforeEnd;
    using TriDiagonalRowVectorConst<K,n>::beforeBegin;
    using TriDiagonalRowVectorConst<K,n>::N;
    using TriDiagonalRowVectorConst<K,n>::dim;
    using TriDiagonalRowVectorConst<K,n>::rowIndex;

protected:

    TriDiagonalRowVector* operator&()
    {
        return this;
    }

private:

    using TriDiagonalRowVectorConst<K,n>::d_;
    using TriDiagonalRowVectorConst<K,n>::u_;
    using TriDiagonalRowVectorConst<K,n>::l_;
    using TriDiagonalRowVectorConst<K,n>::row_;
};


// implement type traits
template<class K, int n>
struct const_reference< TriDiagonalRowVector<K,n> >
{
    typedef TriDiagonalRowVectorConst<K,n> type;
};

template<class K, int n>
struct const_reference< TriDiagonalRowVectorConst<K,n> >
{
    typedef TriDiagonalRowVectorConst<K,n> type;
};

template<class K, int n>
struct mutable_reference< TriDiagonalRowVector<K,n> >
{
    typedef TriDiagonalRowVector<K,n> type;
};

template<class K, int n>
struct mutable_reference< TriDiagonalRowVectorConst<K,n> >
{
    typedef TriDiagonalRowVector<K,n> type;
};


#if 0

/** \brief Iterator class for sparse vector-like containers
 *
 * This class provides an iterator for sparse vector like containers.
 * It contains a ContainerWrapper that must provide the translation
 * from the position in the underlying container to the index
 * in the sparse container.
 *
 * The ContainerWrapper must be default and copy-constructable.
 * Furthermore it must provide the methods:
 *
 * bool identical(other)      - check if this is identical to other (same container, not only equal)
 * T* pointer(position)       - get pointer to data at position in underlying container
 * size_t realIndex(position) - get index in sparse container for position in underlying container
 *
 * Notice that the iterator stores a ContainerWrapper.
 * This allows to use proxy classes as underlying container
 * and as returned reference type.
 *
 * \tparam CW The container wrapper class
 * \tparam T The contained type
 * \tparam R The reference type returned by dereference
 */
template<class CW, class T, class R>
class ContainerWrapperIterator : public BidirectionalIteratorFacade<ContainerWrapperIterator<CW,T,R>,T, R, int>
{
        typedef typename remove_const<CW>::type NonConstCW;

        friend class ContainerWrapperIterator<CW, typename mutable_reference<T>::type, typename mutable_reference<R>::type>;
        friend class ContainerWrapperIterator<CW, typename const_reference<T>::type, typename const_reference<R>::type>;

        typedef ContainerWrapperIterator<CW, typename mutable_reference<T>::type, typename mutable_reference<R>::type> MyType;
        typedef ContainerWrapperIterator<CW, typename const_reference<T>::type, typename const_reference<R>::type> MyConstType;

    public:

        // Constructors needed by the facade iterators.
        ContainerWrapperIterator():
            containerWrapper_(),
            position_(0)
        {}

        ContainerWrapperIterator(CW containerWrapper, int position) :
            containerWrapper_(containerWrapper),
            position_(position)
        {}

        template<class OtherContainerWrapperIteratorType>
        ContainerWrapperIterator(OtherContainerWrapperIteratorType& other):
            containerWrapper_(other.containerWrapper_),
            position_(other.position_)
        {}

        ContainerWrapperIterator(const MyType& other):
            containerWrapper_(other.containerWrapper_),
            position_(other.position_)
        {}

        ContainerWrapperIterator(const MyConstType& other):
            containerWrapper_(other.containerWrapper_),
            position_(other.position_)
        {}

        template<class OtherContainerWrapperIteratorType>
        ContainerWrapperIterator& operator=(OtherContainerWrapperIteratorType& other)
        {
            containerWrapper_ = other.containerWrapper_;
            position_ = other.position_;
        }

        // This operator is needed since we can not get the address of the
        // temporary object returned by dereference
        T* operator->() const
        {
            return containerWrapper_.pointer(position_);
        }

        // Methods needed by the forward iterator
        bool equals(const MyType& other) const
        {
            return position_ == other.position_ && containerWrapper_.identical(other.containerWrapper_);
        }

        bool equals(const MyConstType& other) const
        {
            return position_ == other.position_ && containerWrapper_.identical(other.containerWrapper_);
        }

        R dereference() const
        {
            return *containerWrapper_.pointer(position_);
        }

        void increment()
        {
            ++position_;
        }

        // Additional function needed by BidirectionalIterator
        void decrement()
        {
            --position_;
        }

        // Additional function needed by RandomAccessIterator
        R elementAt(int i) const
        {
            return *containerWrapper_.pointer(position_+i);
        }

        void advance(int n)
        {
            position_=position_+n;
        }

        template<class OtherContainerWrapperIteratorType>
        std::ptrdiff_t distanceTo(OtherContainerWrapperIteratorType& other) const
        {
            assert(containerWrapper_.identical(other));
            return other.position_ - position_;
        }

        std::ptrdiff_t index() const
        {
            return containerWrapper_.realIndex(position_);
        }

    private:
        NonConstCW containerWrapper_;
        size_t position_;
};
#endif


template<class K, int n>
void istl_assign_to_fmatrix(FieldMatrix<K,n,n>& fm, const TriDiagonalMatrix<K,n>& s)
{
    fm = K();
    for(int i=0; i<n; ++i)
        fm[i][i] = s.diagonal()[i];
    for(int i=0; i<n-1; ++i)
    {
        fm[i][i+1] = s.upper()[i];
        fm[i+1][i] = s.lower()[i];
    }
}
/* @} */
} // end namespace
#endif
