/*** COPYRIGHT STATEMENT ************************************ 
 *
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2013]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  This code has been developed by
 *  - Andreas Dedner (University of Warwick)
 *  - Eike Mueller (University of Bath)
 *
 *** COPYRIGHT STATEMENT ************************************/

#ifndef ISTLOPERATOR_HH
#define ISTLOPERATOR_HH

#include <dune/istl/bcrsmatrix.hh>

#include "tridiagonalmatrix.hh"
#include "modeloperator.hh"

/* ********************************************************** *
 * ********************************************************** *
 *
 * ISTL operator
 *
 * ********************************************************** *
 * ********************************************************** */
template <class Op>
class IstlOperator : public Op
{
public:
  typedef typename Op::DF DF;
  typedef typename DF::GridView GV;
  static const int dim=DF::dim;
  static const int NZ = DF::nz;
  typedef typename Dune::BlockVector<typename DF::LocalVector> X; // Vector data type
protected:
  typedef Dune::TriDiagonalMatrix<double,NZ> BlockMatrix;
  typedef Dune::BCRSMatrix<BlockMatrix> Matrix;
  typedef Op BaseType;
  typedef typename DF::field_type field_type;
public:
/* ********************************************************** *
 * Constructor
 * ********************************************************** */
  IstlOperator(const GridParameters<NZ> &grid_param,
               const SmootherParameters &smoother_param)
  : BaseType(grid_param,smoother_param) {}
/* ********************************************************** *
 * Destructor
 * ********************************************************** */
  ~IstlOperator() {
    for(size_t i=0;i<matrix_.size();++i) {
      if (matrix_[i]) delete(matrix_[i]);
    }
  }

/* ********************************************************** *
 * Solve A.u = b
 * ********************************************************** */
  virtual void solve(const DF &b,DF &u) const = 0;

/* ********************************************************** *
 * Apply A.x = y
 * ********************************************************** */
  virtual void apply(const DF &x,DF &y) const {
    int level = x.level();
    // assemble
    assembleMatrix(x);
    matrix_[level]->mv(x.dofs(),y.dofs());
  }

  protected:

/* ********************************************************** *
 * Construct matrix for a given level
 * ********************************************************** */
  bool assembleMatrix(const DF &u) const {
    int level = u.level();
    if (matrix_.size()<level+1) matrix_.resize( level+1, 0);
    if ( !matrix_[level] )
    {
      // temporary arrays for construction of tridiagonal matrices
      Dune::FieldVector<field_type,NZ> diag;
      Dune::FieldVector<field_type,NZ-1> upper;
      Dune::FieldVector<field_type,NZ-1> lower;
      size_t nrow = u.dofs().size();
      // Create matrix
      assert(!matrix_[level]);
      matrix_[level] = new Matrix(nrow,nrow,Matrix::random);
      // Build sparsity pattern
      typedef typename DF::GridView::IndexSet::IndexType IndexType;
      std::vector<std::vector<IndexType> > Idxs(nrow);
      // Iterate over all grid cells
      const typename DF::Iterator ibegin = u.begin();
      const typename DF::Iterator iend = u.end();
      for (typename DF::Iterator it = ibegin;it!=iend;++it) {
        const typename DF::Entity &en = *it;
        IndexType rowIdx = u.index(en);
        // diagonal entry
        // Iterate over all intersections
        const typename DF::IntersectionIterator isend = u.gridView().iend(en);
        const typename DF::IntersectionIterator isbegin = u.gridView().ibegin(en);
        for (typename DF::IntersectionIterator is = isbegin;is!=isend;++is) {
          // Extract indices of neighbouring grid cells and
          // save to vector cols
          const typename DF::EntityPointer nbPtr = is->outside();
          const typename DF::Entity &nb = *nbPtr;
          Idxs[rowIdx].push_back(u.index(nb));
        }
        Idxs[rowIdx].push_back(rowIdx);
      }
      for (IndexType rowIdx=0; rowIdx<nrow;rowIdx++) {
        size_t ncol = Idxs[rowIdx].size();
        matrix_[level]->setrowsize(rowIdx,ncol);
      }
      matrix_[level]->endrowsizes();
      for (IndexType rowIdx=0; rowIdx<nrow;rowIdx++) {
        size_t ncol = Idxs[rowIdx].size();
        for(size_t i=0; i<ncol;++i) {
          IndexType colIdx = Idxs[rowIdx][i];
          matrix_[level]->addindex(rowIdx,colIdx);
        }
      }
      matrix_[level]->endindices();
      // Set matrix elements
      // Iterate over all grid cells
      for (typename DF::Iterator it = ibegin;it!=iend;++it) {
        const typename DF::Entity &en = *it;
        IndexType rowIdx = u.index(en);
        // Set up horizontal components of stencil
        setupalpha(u,en);
        // block-diagonal entries
        for (int k=0;k<NZ;k++) {
          mainCoeffs(k);
          diag[k] = a;
          if (k<NZ-1) upper[k] = b;
          if (k>0) lower[k-1] = c;
        }
        BlockMatrix mDiag(diag,upper,lower);
        (*matrix_[level])[rowIdx][rowIdx] = 0.0;
        (*matrix_[level])[rowIdx][rowIdx] += mDiag;
        // block-offdiagonal entries
        for (size_t i = 0;i<nofNb;++i) {
          IndexType colIdx = Idxs[rowIdx][i];
          for (int k=0;k<NZ;k++) {
            offDiagCoeff(k);
            diag[k] = alpha[i]*d;
          }
          BlockMatrix mOffDiag(diag);
          (*matrix_[level])[rowIdx][colIdx] = 0.0;
          (*matrix_[level])[rowIdx][colIdx] +=mOffDiag;
        }
      }
      return false;
    }
    return true;
  }
/* ********************************************************** *
 * Setup horizontal components of matrix (alpha) on a given
 * element
 * ********************************************************** */
  template <class Entity>
  void setupalpha(const DF &u, const Entity &en) const {
    const typename DF::Geometry::GlobalCoordinate centerEn = en.geometry().center();
    const typename DF::IntersectionIterator isend = u.gridView().iend(en);
    nofNb=0;
    salpha = 0.0;
    for (typename DF::IntersectionIterator is = u.gridView().ibegin(en);
         is!=isend; ++is, nofNb++) {
      if (is->neighbor()) {
        const typename DF::EntityPointer nbPtr = is->outside();
        const typename DF::Entity &nb = *nbPtr;
        typename DF::Geometry::GlobalCoordinate
          distance = nb.geometry().center();
        distance -= centerEn;
        if (nofNb < alpha.size()) {
          alpha[nofNb] = (is->geometry().volume()/u.EdgeScale())
                       / distance.two_norm();
        } else {
          alpha.push_back((is->geometry().volume()/u.EdgeScale())
                       / distance.two_norm());
        }
      } else {
#ifndef UGGRID
        typename DF::Geometry::GlobalCoordinate distance =
          is->geometry().center();
        distance -= centerEn;
        if (nofNb < alpha.size()) {
          alpha[nofNb] = (is->geometry().volume()/u.EdgeScale()) / distance.two_norm();
        } else {
          alpha.push_back((is->geometry().volume()/u.EdgeScale()) / distance.two_norm());
        }
#endif // UGGRID
      }
      salpha += alpha[nofNb];
    }
    volume_ = en.geometry().volume()/u.AreaScale();
  }

protected:
  mutable std::vector< Matrix* > matrix_;
  using BaseType::alpha;
  using BaseType::salpha;
  using BaseType::a;
  using BaseType::b;
  using BaseType::c;
  using BaseType::d;
  using BaseType::nofNb;
  using BaseType::volume_;
  using BaseType::offDiagCoeff;
  using BaseType::mainCoeffs;
};
#endif
