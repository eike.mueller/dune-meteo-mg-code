/*** COPYRIGHT STATEMENT ************************************ 
 *
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2013]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  This code has been developed by
 *  - Andreas Dedner (University of Warwick)
 *  - Eike Mueller (University of Bath)
 *
 *** COPYRIGHT STATEMENT ************************************/

#ifndef MULTIGRID_HH
#define MULTIGRID_HH

#include "operator.hh"
#include "gridfunction.hh"
#include "parameters.hh"

/* ********************************************************** *
 *  Parameters for multigrid
 * ********************************************************** */

class MultigridParameters : public Parameters {
 public:
  enum MGCycleType {VCycle, WCycle};
  // Constructor
  MultigridParameters() :
                  Parameters("multigrid"),
                  levels_(1),
                  npresmooth_(1),
                  npostsmooth_(1) {
      addKey("levels",Integer);
      addKey("npresmooth",Integer);
      addKey("npostsmooth",Integer);
      addKey("cycletype",String);
    }

  //
  int readFile(const std::string filename) {
    int readSuccess = Parameters::readFile(filename);
    if (!readSuccess) {
      levels_ = contents["levels"]->getInt();
      npresmooth_ = contents["npresmooth"]->getInt();
      npostsmooth_ = contents["npostsmooth"]->getInt();
      std::string cycleStr = contents["cycletype"]->getString();
      if (cycleStr == "VCycle") {
        cycletype_ = VCycle;
      } else if (cycleStr == "WCycle") {
        cycletype_ = WCycle;
      }
    }
    return readSuccess;
  }

  // Return data
  unsigned int levels() const { return levels_; }
  unsigned int npresmooth() const { return npresmooth_; }
  unsigned int npostsmooth() const { return npostsmooth_; }
  MGCycleType cycletype() const { return cycletype_; }

 private:
  unsigned int levels_;
  unsigned int npresmooth_;
  unsigned int npostsmooth_;
  MGCycleType cycletype_;
};

/* ********************************************************** *
 *
 *  Multigrid
 *
 * ********************************************************** */
template <int NZ, class G>
struct Multigrid : public Operator<typename HierarchicDF<NZ,G>::LevelDF>
{
  typedef HierarchicDF<NZ,G> HDF;
  typedef typename HDF::LevelDF DF;
  static const int dim=DF::dim;
  // Constructors
  Multigrid(G &grid,
            const MultigridParameters &multigrid_param,
            const Operator<DF> &oppresmooth,
            const Operator<DF> &oppostsmooth,
            const Operator<DF> &opcoarse) :
              grid_(grid),
              oppresmooth_(oppresmooth),
              oppostsmooth_(oppostsmooth),
              opcoarse_(opcoarse),
              multigrid_param_(multigrid_param),
              b_(grid_,multigrid_param.levels()),
              u_(grid_,multigrid_param.levels()),
              r_(grid_,multigrid_param.levels())
  {}

  // Solve approximately using one Vcycle
  virtual void solveApprox(const DF& b, DF& u) const {
    u_[0].dofs() = u.dofs();
    b_[0].dofs() = b.dofs();
    mgcycle(0);
    // Add correction from V-cycle
    u.dofs() = u_[0].dofs();
  }

  virtual void apply(const DF &b, DF &u) const {
    oppresmooth_.apply(b,u);
  }

  void initialize() const
  {
    oppostsmooth_.initialize();
    oppresmooth_.initialize();
    opcoarse_.initialize();
  }
  void finalize() const
  {
    oppresmooth_.finalize();
    oppostsmooth_.finalize();
    opcoarse_.finalize();
  }
  virtual double norm( const DF &u) const
  {
    return oppresmooth_.norm(u);
  }

  private:
    void mgcycle(const unsigned int level) const {
      int ncycle;
      if (multigrid_param_.cycletype() == MultigridParameters::VCycle)
        ncycle = 1;
      if (multigrid_param_.cycletype() == MultigridParameters::WCycle)
        ncycle = 2;
      if (level == 0) ncycle = 1;
      assert(level<multigrid_param_.levels());
      if (level == multigrid_param_.levels() - 1) {
        // Solve exactly
        u_[level].dofs() = 0.0;
        opcoarse_.solve(b_[level],u_[level]);
      } else {
        for (int i=0;i<ncycle;i++) {
          // Presmooth
          for (unsigned int ismooth=0;
               ismooth<multigrid_param_.npresmooth();
               ismooth++)
            oppresmooth_.solveApprox(b_[level],u_[level]);
          // Calculate residual
          oppresmooth_.residual(b_[level],u_[level],r_[level]);
          // Restrict residual to get RHS on coarser level
          r_.restriction(level+1);
          b_[level+1].dofs() = r_[level+1].dofs();
          // Initial solution on coarser level is zero
          u_[level+1].dofs() = 0.0;
          mgcycle(level+1);
          // Prolongate coarse grid correction and add to solution
          u_.prolongationAdd(level);
          u_[level].haloexchange();
          // Postsmooth
          for (unsigned int ismooth=0;
               ismooth<multigrid_param_.npostsmooth();
               ismooth++)
            oppostsmooth_.solveApprox(b_[level],u_[level]);
       }
     }
    }

    // Grid
    const G& grid_;
    // Operator
    const Operator<DF>& oppresmooth_;
    const Operator<DF>& oppostsmooth_;
    const Operator<DF>& opcoarse_;
    // Parameters
    MultigridParameters multigrid_param_;
    mutable HDF b_,u_,r_;
};
#endif
