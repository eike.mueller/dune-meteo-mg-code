### COPYRIGHT STATEMENT #######################################
#
#  (c) The copyright relating to this work is owned jointly 
#  by the Crown, Met Office and NERC [2013]. However, it
#  has been created with the help of the GungHo Consortium,
#  whose members are identified at
#  https://puma.nerc.ac.uk/trac/GungHo/wiki
#  
#  This code has been developed by
#  - Andreas Dedner (University of Warwick)
#  - Eike Mueller (University of Bath)
#
### COPYRIGHT STATEMENT #######################################

import sys
import numpy as np
import math

###################################################################
# Create grid
# See
# http://www.classes.cs.uchicago.edu/archive/2003/fall/23700/docs/handout-04.pdf
###################################################################
def createGrid(Filename):
  v = []
  s = []
  t = 0.5*(1.+math.sqrt(5.))
  scal = 1./math.sqrt(1.+t**2)

  # Vertices
  v.append(( t, 1, 0))
  v.append((-t, 1, 0))
  v.append(( t,-1, 0))
  v.append((-t,-1, 0))

  v.append(( 1, 0, t))
  v.append(( 1, 0,-t))
  v.append((-1, 0, t))
  v.append((-1, 0,-t))

  v.append(( 0, t, 1))
  v.append(( 0,-t, 1))
  v.append(( 0, t,-1))
  v.append(( 0,-t,-1))

  # Elements
  s.append(( 0, 8, 4))
  s.append(( 0, 5,10))
  s.append(( 2, 4, 9))
  s.append(( 2,11, 5))

  s.append(( 1, 6, 8))
  s.append(( 1,10, 7))
  s.append(( 3, 9, 6))
  s.append(( 3, 7,11))

  s.append(( 0,10, 8))
  s.append(( 1, 8,10))
  s.append(( 2, 9,11))
  s.append(( 3, 9,11))

  s.append(( 4, 2, 0))
  s.append(( 5, 0, 2))
  s.append(( 6, 1, 3))
  s.append(( 7, 3, 1))

  s.append(( 8, 6, 4))
  s.append(( 9, 4, 6))
  s.append((10, 5, 7))
  s.append((11, 7, 5))

  File = open(Filename,'w')
  print >> File, 'DGF'
  print >> File, ''
  print >> File, 'VERTEX'
  for vertex in v:
    print >> File, str(scal*vertex[0])+' '+str(scal*vertex[1])+' '+str(scal*vertex[2])
  print >> File, '#'
  print >> File, ''
  print >> File, 'SIMPLEX'
  for simplex in s:
    print >> File, str(simplex[0])+' '+str(simplex[1])+' '+str(simplex[2])
  print >> File, '#'
  print >> File, ''
  print >> File, 'PROJECTION'
  print >> File, 'function p(x) = x / |x|'
  print >> File, 'default p'
  print >> File, '#'
  print >> File, ''
  print >> File, 'GRIDPARAMETER'
  print >> File, 'NAME Icosahedron'
  # print >> File, 'dumpfilename icosahedron.dgf' better not because all processore dump...
  print >> File, 'REFINEMENTEDGE ARBIRTRARY'
  print >> File, '#'
  print >> File, ''
  print >> File, '#'

  File.close()

###################################################################
###################################################################
if (__name__ == '__main__'):
  if (len(sys.argv) != 2):
    print "Usage: python "+sys.argv[0]+" <filename>"
    sys.exit(0)
  Filename = sys.argv[1]
  createGrid(Filename)
