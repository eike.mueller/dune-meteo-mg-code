/*** COPYRIGHT STATEMENT ************************************ 
 *
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2013]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  This code has been developed by
 *  - Andreas Dedner (University of Warwick)
 *  - Eike Mueller (University of Bath)
 *
 *** COPYRIGHT STATEMENT ************************************/

#ifndef RHS_HH
#define RHS_HH
/* *************************************************************** *
 * Profiles
 * *************************************************************** */
#include"gridparameters.hh"

/* *** Spherical geometry *** */
struct FSpherical
{
  FSpherical(const double Hbottom,
             const double H) : Hbottom_(Hbottom), H_(H) {
    pi = 4.0*atan(1.0);
  }

  template <class Coord>
  double operator()(const Coord &x) const
  {
    double res = 0.0;
    double r = x.two_norm();
    double theta = acos(x[2]/r);
    double blow = 1.0 + Hbottom_ + 0.2*(H_-Hbottom_);
    double bup = 1.0 + Hbottom_ + 0.8*(H_-Hbottom_);
    if ( (r > blow) && (r < bup) &&
         ( ((theta > pi/10.) && (theta < pi/5.)) ||
           ((theta > 3.*pi/8.) && (theta < 5.*pi/8)) ||
           ((theta > 4.*pi/5.) && (theta < 9.*pi/10.)) ) )
        res = 1.0;
    return res;
  }
  double Hbottom_;
  double H_;
  double pi;
};

/* *** Constant RHS *** */
struct FConstant
{
  FConstant() {}

  template <class Coord>
  double operator()(const Coord &x) const
  {
    double res = 1.0;
    return res;
  }
};
#endif // RHS_HH

