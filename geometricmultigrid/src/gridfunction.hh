/*** COPYRIGHT STATEMENT ************************************ 
 *
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2013]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  This code has been developed by
 *  - Andreas Dedner (University of Warwick)
 *  - Eike Mueller (University of Bath)
 *
 *** COPYRIGHT STATEMENT ************************************/

#ifndef GRIDFUNCTION_HH
#define GRIDFUNCTION_HH

//#define RECONSTR_SINGLE 1 // use a single reconstruction on father element (remove this line for childwise reconstruction)
#define RECONSTR_CONS   1 // use a conservative version of the single reconstruction (remove line to not shift plane)

/* ***************************************************************
 *
 * Classes for representing fields (grid functions) on a
 * hierachical grid (2+1)dimensional grid. Each cell of the
 * underlying 2d grid stores an entire column of data, which is
 * stored contiguously in memory.
 *
 * *************************************************************** */

#include <string>
#include <dune/istl/bvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/grid/common/gridenums.hh>
#include <dune/grid/common/scsgmapper.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>
#include <dune/grid/common/mcmgmapper.hh>

/* *************************************************************** *
 * *************************************************************** *
 *
 *   Grid function on a fixed level of a 2d grid
 *   (defined by a (level-)gridview).
 *
 *   Data on each entity of the underlying 2dimensional grid
 *   is stored in a field of type
 *     LocalVector = Dune::FieldVector<double,NZ>
 *   where the template parameter NZ stores the number of vertical
 *   grid cells.
 *   All data is contained in the field dofs_ which is of type
 *     Dune::BlockVector<LocalVector>
 *
 * *************************************************************** *
 * *************************************************************** */
template <int NZ, class GV,int codim>
struct DFBase
{
  static const int nz = NZ; // Number of vertical levels (cells)
#if defined UGGRID
  static const int dim = GV::dimension-1;
#else // defined UGGRID
  static const int dim = GV::dimension;
#endif // defined UGGRID
  typedef GV GridView;
  typedef typename GridView::Grid Grid;
  static const int dimworld = GV::dimensionworld;
  typedef typename GridView::template Codim<0>::
   	      template Partition<Dune::Interior_Partition>::Iterator Iterator;
  typedef typename Iterator::Entity Entity;
  typedef typename Entity::EntityPointer EntityPointer;
  typedef typename Entity::Geometry Geometry;
  typedef typename GridView::IntersectionIterator IntersectionIterator;
  typedef typename IntersectionIterator::Intersection Intersection;
  typedef typename Intersection::Geometry IntersectionGeometry;
  typedef Dune::FieldVector<double,NZ> LocalVector;
  typedef double field_type;

 /* *************************************************************** *
  * Constructors
  * *************************************************************** */
  DFBase(const GV &gv,    // Gridview
         int level
        ) : gv_(gv),
            level_(level),
            dofs_(gv_.size(codim),0)
  {calculateScalingFactors(*begin(), Rmin_,Rmax_,AreaScale_,EdgeScale_); }

 /* *************************************************************** *
  * Iterators for looping over the grid function
  * *************************************************************** */
  const Iterator end() const
  {
    return gv_.template end<0,Dune::Interior_Partition>();
  }
  Iterator begin() const
  {
    return gv_.template begin<0,Dune::Interior_Partition>();
  }

 /* *************************************************************** *
  * [] operator extracts data in vertical column on a specific
  * element of the 2-dimensional grid
  * *************************************************************** */
  const LocalVector &operator()(const Entity &en,int i) const
  {
    int idx = index(en,i);
    assert( idx < dofs_.size() );
    return dofs_[ idx ];
  }
  LocalVector &operator()(const Entity &en,int i)
  {
    int idx = index(en,i);
    assert( idx < dofs_.size() );
    return dofs_[ idx ];
  }
  size_t index(const Entity &en,int i) const
  {
    return gv_.indexSet().subIndex(en,i,codim);
  }
  size_t level() const
  {
    return level_;
  }


 /* *************************************************************** *
  * Extract all degrees of freedom (data)
  * *************************************************************** */
  const Dune::BlockVector<LocalVector>& dofs() const
  {
    return dofs_;
  }

  Dune::BlockVector<LocalVector>& dofs()
  {
    return dofs_;
  }

  /* *************************************************************** *
  * Extract gridview of 2dimensional grid
  * *************************************************************** */
  const GV &gridView() const
  {
    return gv_;
  }

  /* *************************************************************** *
   * Return shell radius and geometrical scaling factors
   * for UG grid
   * *************************************************************** */
  double inline Rmin() const { return Rmin_; }
  double inline Rmax() const { return Rmax_; }

  double inline AreaScale() const { return AreaScale_; }
  double inline EdgeScale() const { return EdgeScale_; }

  /* *************************************************************** *
   * Calculate geometrical scaling factors
   * *************************************************************** */
  static void calculateScalingFactors(const Entity &en,
                                      double &Rmin_,double &Rmax_,
                                      double &AreaScale_,
                                      double &EdgeScale_
                                     ) {
#ifdef UGGRID
    const typename DFBase::Geometry geometry = en.geometry();
    double Rmin = 1.e9;
    double Rmax = -1.e-9;
    for (size_t i=0;i<geometry.corners();++i) {
      Dune::FieldVector<double,DFBase::dimworld> x = geometry.corner(i);
      if (x.two_norm() > Rmax) Rmax = x.two_norm();
      if (x.two_norm() < Rmin) Rmin = x.two_norm();
    }
    AreaScale_ = (Rmax*Rmax*Rmax-Rmin*Rmin*Rmin)/3.;
    EdgeScale_ = (Rmax*Rmax-Rmin*Rmin)/2.;
    Rmin_ = Rmin;
    Rmax_ = Rmax;
#else
    Rmin_ = 1.0;
    Rmax_ = 1.0;
    AreaScale_ = 1.0;
    EdgeScale_ = 1.0;
#endif // UGGRID
  }

  /* *************************************************************** *
   * private class data
   * *************************************************************** */
 protected:
  const GV gv_; // Underlying gridview
  int level_;
  typedef Dune::BlockVector<LocalVector> V;
  V dofs_; // field data
  // Inner and outer radius of shell for UGGrid, 1 otherwise
  double Rmin_;
  double Rmax_;
  // Geometric scaling factors.
  // * AreaScale_: Ratio of cell volume on 3d grid and 2d element on
  //               unit sphere grid AreaScale_ = (Rmax^3-Rmin^3)/3
  // * EdgeScale_: Ratio of face area on 3d grid to edge length on
  //               unit sphere grid EdgeScale_ = (Rmax^2-Rmin^2)/2
  double AreaScale_;
  double EdgeScale_;
};

template <int NZ, class GV,int codim=0>
struct DF;

template <int NZ, class GV,int codim>
struct DF : public DFBase<NZ,GV,codim>
{
  typedef DFBase<NZ,GV,codim> Base;
  static const int dim=Base::dim;
  DF(const GV &gv,    // Gridview
     int level
    ) : Base(gv,level) {}
};

/* *************************************************************** *
 * Specialisation for codim 0 entities
 * *************************************************************** */
template <int NZ, class GV>
struct DF<NZ,GV,0> : public DFBase<NZ,GV,0>
{
  typedef DFBase<NZ,GV,0> Base;
  static const int dim=Base::dim;
  static const int nz =Base::nz;
  typedef typename Base::LocalVector LocalVector;
  typedef typename Base::Iterator Iterator;
  typedef typename Base::Entity Entity;
  typedef typename Base::V V;
  typedef typename GV::CollectiveCommunication CollectiveCommunication;
  DF(const GV &gv,    // Gridview
     int level
    ) : Base(gv,level),
        interior_dofs_mask_(gv_.size(0)),
        haloexchanger_(gv_.indexSet(),dofs_)
        { precompute_interior_dofs_mask(); }

/* *************************************************************** *
 * Communication interface
 * *************************************************************** */
  template <class IndexSet, class V>
  class VectorExchange :
    public Dune::CommDataHandleIF<VectorExchange<IndexSet,V>,
                                  typename V::value_type> {
   public:
    typedef typename V::value_type DataType; // Data type
    bool contains (int dim, int codim) const { return (codim == 0); }
    bool fixedsize (int dim, int codim) const { return true; }
    template <class EntityType>
    size_t size(EntityType& e) const { return 1; }
    // Pack user data into message buffer
    template <class MessageBuffer, class EntityType>
    void gather (MessageBuffer& buff, const EntityType& e) const {
      buff.write(c_[indexset_.index(e)]);
    }
    // Unpack user data from message buffer
    template <class MessageBuffer, class EntityType>
    void scatter (MessageBuffer& buff, const EntityType& e, size_t n) {
      DataType x;
      buff.read(x);
      c_[indexset_.index(e)] = x;
    }
    // Constructor
    VectorExchange(const IndexSet& indexset, V& c) :
      indexset_(indexset), c_(c) {}
   private:
    const IndexSet& indexset_; // map entity -> index set
    V& c_; // Data container
  };

/* ********************************************************** *
 * Precompute interior dofs mask
 * ********************************************************** */
  void precompute_interior_dofs_mask()
  {
    for (size_t i=0;i<dofs_.size();++i)
    {
      interior_dofs_mask_[i] = 0;
    }
    Iterator ibegin = this->begin();
    Iterator iend = this->end();
    for(Iterator it=ibegin;it!=iend;++it) {
      const Entity &en = *it;
      interior_dofs_mask_[index(en)] = 1;
    }
  }

/* ********************************************************** *
 * Calculate Euclidean norm of grid function dofs
 * ********************************************************** */
  double dof_norm() const
  {
    double nrm2 = 0.0;
    for (size_t i=0;i<dofs_.size();++i)
    {
      for (size_t k=0;k<NZ;++k) {
        nrm2 += interior_dofs_mask_[i]*(dofs_[i][k])*(dofs_[i][k]);
      }
    }
    double global_nrm2 = this->comm().sum(nrm2);
    return sqrt(global_nrm2);
  }

/* ********************************************************** *
 * Calculate dot product with another grid function
 * ********************************************************** */
  double dof_dotprod(const DF& other) const
  {
    double dotprod = 0.0;
    for (size_t i=0;i<dofs_.size();++i)
    {
      for (size_t k=0;k<NZ;++k) {
        dotprod += interior_dofs_mask_[i]*(dofs_[i][k])*(other.dofs()[i][k]);
      }
    }
    double global_dotprod = this->comm().sum(dotprod);
    return global_dotprod;
  }

  /* *************************************************************** *
   * Index operators
   * *************************************************************** */
  const LocalVector &operator[](const Entity &en) const
  {
    assert( gv_.indexSet().index(en) < dofs_.size() );
    return dofs_[ gv_.indexSet().index(en) ];
  }

  LocalVector &operator[](const Entity &en)
  {
    assert( gv_.indexSet().index(en) < dofs_.size() );
    return dofs_[ gv_.indexSet().index(en) ];
  }
  size_t index(const Entity &en) const
  {
    return gv_.indexSet().index(en);
  }
  /* *************************************************************** *
  * Extract global communication object
  * *************************************************************** */
  const CollectiveCommunication& comm() const {
    return gv_.comm();
  }

 /* *************************************************************** *
  * Halo exchange
  * *************************************************************** */
  void haloexchange() {
    gv_.communicate(haloexchanger_,
                    Dune::InteriorBorder_All_Interface,
                    Dune::ForwardCommunication);
  }

  /* *************************************************************** *
   * Save data on one level to vtk file
   * *************************************************************** */
  void savetovtk(const std::string filename,
                 Dune::VTK::OutputType vtkoutputtype=Dune::VTK::ascii) {
    std::vector<double> c(dofs_.size()*nz);
    Iterator ibegin = this->begin();
    Iterator iend = this->end();
    for(Iterator it=ibegin;it!=iend;++it) {
      const Entity &en = *it;
      for (int k =0;k<nz;++k)
        c[gv_.indexSet().index(en)*nz+k] = dofs_[gv_.indexSet().index(en)][k];
    }
    typename Dune::VTKWriter<GV> vtkwriter(gv_,Dune::VTK::conforming);
    vtkwriter.addCellData(c,"level",nz);
    std::string path="";
    vtkwriter.pwrite(filename,path,path,vtkoutputtype);
  }
private:
  using Base::gv_;
  using Base::dofs_;
  std::vector<short int> interior_dofs_mask_;
  VectorExchange<typename GV::IndexSet,V> haloexchanger_;
};


/* *************************************************************** *
 * *************************************************************** *
 *
 * - Methods for restricting/prolongating data between the
 *   different levels
 * - Finest level = 0
 *
 * *************************************************************** *
 * *************************************************************** */

template <int NZ, class G>
struct HierarchicDF
{
  typedef G Grid;
  typedef typename G::LevelGridView GridView;
  typedef DF<NZ,GridView> LevelDF;
  static const int dim = LevelDF::dim;
#if defined UGGRID
  typedef typename Dune::FieldVector<double,2> LocalCoordinate;
#else
  typedef typename LevelDF::Geometry::LocalCoordinate LocalCoordinate;
#endif // defined UGGRID
  /* *************************************************************** *
   * Constructors
   * *************************************************************** */
  HierarchicDF(const G &g,         // Underlying 2d grid
               int numberOfLevels  // Number of levels
              ) :
    u_( numberOfLevels ),
    numberOfLevels_(numberOfLevels),
    uNb_(0)
  {
    // Check that the grid has enough levels
    assert(g.maxLevel()+1-numberOfLevels>=0);
    // Construct a grid function on each level of the grid
    for (int l=0;l<numberOfLevels;++l)
    {
      u_[l] = new LevelDF(g.levelView(g.maxLevel()-l),l);
    }
  }

  /* *************************************************************** *
   * Return the number of levels
   * *************************************************************** */
  int numberOfLevels() const
  {
    return numberOfLevels_;
  }

  /* *************************************************************** *
   * [] operator returns grid function on specific level
   * *************************************************************** */
  const LevelDF &operator[](int l) const
  {
    return *(u_[l]);
  }
  LevelDF &operator[](int l)
  {
    return *(u_[l]);
  }

  /* *************************************************************** *
   * Restrict data from level lnew-1 (fine) to lnew (coarse) using
   * the average of the data in all fine grid cells
   * corresponding to one coarse grid cell.
   * *************************************************************** */
  void restriction(int lnew)
  {
    // initialize u_[lnew]
    typename LevelDF::Iterator it = (*this)[lnew].begin();
    const typename LevelDF::Iterator endIt = (*this)[lnew].end();
    const int level=it->level();
    // Iterate over cells of coarser level
    for (;it!=endIt;++it)
    {
      const typename LevelDF::Entity &en = *it;
      typename LevelDF::LocalVector &localDofs = (*this)[lnew][en];
      // Initialise coarse grid field on element en to zero
      localDofs = typename LevelDF::LocalVector(0);
      // Loop over all fine grid cells that belong to the coarse cell en
      typename LevelDF::Entity::HierarchicIterator hit = en.hbegin( level+1 );
      const typename LevelDF::Entity::HierarchicIterator hend = en.hend( level+1 );
      double weight = 0.0;
      for (;hit!=hend;++hit)
      {
        const typename LevelDF::Entity &hen = *hit;
        typename LevelDF::LocalVector uEn = (*this)[lnew-1][hen];
        const double hweight = 1.;
        // const double hweight = hen.geometry().volume()/AreaScale();
        for (int k=0;k<NZ;++k)
          localDofs[k] += uEn[k] * hweight;
        weight += hweight;
      }
    }
  }

  /* *************************************************************** *
   * Prolongate data from level lnew+1 (coarse) to lnew (fine) and
   * add to already existing data on fine grid.
   * This method uses linear interpolation, i.e. it first puts a
   * plane though the nearest neighbours and then shifts this plane
   * using the data on the coarse grid element en.
   *
   * *************************************************************** */
  void prolongationAdd(int lnew)
  {
    // initialize u_[lnew]
    typename LevelDF::Iterator it = (*this)[lnew+1].begin();
    const typename LevelDF::Iterator endIt = (*this)[lnew+1].end();
    const int level=it->level();
    // Iterate over all cells of the coarse grid
    for (;it!=endIt;++it)
    {
      const typename LevelDF::Entity &en = *it;
      // Extract data and position of centre of grid cell
      const typename LevelDF::LocalVector &uEn = (*this)[lnew+1][en];
      const typename LevelDF::Geometry::GlobalCoordinate centerEn = en.geometry().center();
	    const typename LevelDF::IntersectionIterator isend = (*this)[lnew+1].gridView().iend(en);
      const unsigned int dimension = LevelDF::Geometry::GlobalCoordinate::dimension;
      unsigned int nofNb = 0;
      // Construct unit vectors n1, n2, which span an orthogonal system on
      // the coarse element
      std::vector<typename LevelDF::Geometry::GlobalCoordinate> n(2);
      std::vector<typename LevelDF::Geometry::GlobalCoordinate> centerNb(2);
      // Extract the centres of the neighbors
      int i=0;
	    for (typename LevelDF::IntersectionIterator
           is = (*this)[lnew+1].gridView().ibegin(en);i<2; ++is) {
        if (is->neighbor()) {
          centerNb[i] = is->geometry().center();
          i++;
        }
      }
      n[0] = centerEn;
      n[0] -= centerNb[0];
      n[1] = centerEn;
      n[1] -= centerNb[1];
      n[0] *= 1./n[0].two_norm();
      double n1n2 = n[0]*n[1];
      typename LevelDF::Geometry::GlobalCoordinate n1tmp = n[0];
      n1tmp *= n1n2;
      n[1] -= n1tmp;
      n[1] *= 1./n[1].two_norm();
#if RECONSTR_SINGLE // single reconstruction on father
      const unsigned int mat_dimension = 2;
      Dune::FieldMatrix<double,mat_dimension+1,mat_dimension+1> ATA(0.0);
	    for (typename LevelDF::IntersectionIterator is = (*this)[lnew+1].gridView().ibegin(en);
           is!=isend; ++is)
	  	{
        std::vector< double> row(mat_dimension+1);
        if (is->neighbor())
        {
          const typename LevelDF::EntityPointer nbPtr = is->outside();
          const typename LevelDF::Entity &nb = *nbPtr;
          if ( nofNb < uNb_.size() )
            uNb_[nofNb] = (*this)[lnew+1][nb];
          else
            uNb_.push_back( (*this)[lnew+1][nb] );
          const typename LevelDF::Geometry::GlobalCoordinate
            centerNb = nb.geometry().center();
          double len = 0;
          for (int k=0;k<mat_dimension;++k) {
            row[k] = (centerNb - centerEn)*n[k];
            len += row[k]*row[k];
          }
          len = (centerNb - centerEn).two_norm()/sqrt(len);
          for (int k=0;k<mat_dimension;++k)
             row[k] *= len;
          row[mat_dimension] = 1.;
          ++nofNb;
        }
        else // von Neumann BCs (not necessary on spherical grid!)
        {
        // For the UG Grid implementation, ignore all boundaries (ok on spherical grids)
#ifndef UGGRID
          if ( nofNb < uNb_.size() ) {
            uNb_[nofNb] = 0.0;
          } else {
            typename LevelDF::LocalVector uzero(0.0);
            uNb_.push_back(uzero);
          }
          typename LevelDF::Geometry::GlobalCoordinate distanceNb =
            is->geometry().center();
          distanceNb -= centerEn;
          for (int k=0;k<mat_dimension;++k)
            row[k] = (distanceNb)*n[k];
          row[mat_dimension] = 1.;
          if ( nofNb < dx_.size() )
            dx_[nofNb] = row;
          else
            dx_.push_back( row );
          for (unsigned int j=0;j<mat_dimension+1;++j) {
            for (unsigned int k=0;k<mat_dimension+1;++k) {
              ATA[j][k] += row[j]*row[k];
            }
          }
#endif // UGGRID
        }
        if ( nofNb < dx_.size() )
          dx_[nofNb] = row;
        else
          dx_.push_back( row );
        for (unsigned int j=0;j<mat_dimension+1;++j) {
          for (unsigned int k=0;k<mat_dimension+1;++k) {
            ATA[j][k] += row[j]*row[k];
          }
        }
      }
      ATA.invert();

      // For each vertical level, calculate the gradient vector
      // (a_1,...,a_d)
      typename LevelDF::LocalVector val(0);
      for (int k=0;k<NZ;++k)
      {
        Dune::FieldVector<double,mat_dimension+1> rhs(0.0);
        for (int i=0;i<nofNb;++i) {
          for (int j=0;j<mat_dimension+1;++j)
            rhs[j] += uNb_[i][k]*dx_[i][j];
        }
        for (int i=0;i<mat_dimension;++i) {
          gradient_[k][i] = 0.0;
          for (int j=0;j<mat_dimension+1;++j) {
            gradient_[k][i] += ATA[i][j]*rhs[j];
          }
        }
        for (int j=0;j<mat_dimension+1;++j)
          val[k] += ATA[mat_dimension][j]*rhs[j];
      }
#if RECONSTR_CONS
      val = uEn;  // remove for non-conservative version
#endif // RECONSTR_CONS

      typename LevelDF::Entity::HierarchicIterator hit = en.hbegin( level+1 );
      const typename LevelDF::Entity::HierarchicIterator hend = en.hend( level+1 );
      // Use the gradient and the value on the element en
      // (stored above in uEn) to prolongate from the coarse to the fine grid
      for (;hit!=hend;++hit)
      {
        const typename LevelDF::Entity &hen = *hit;
        typename LevelDF::Geometry::GlobalCoordinate
          x = hen.geometry().center();
        x -= centerEn;
        (*this)[lnew][hen] += val; // uEn;
        double len = x.two_norm();
        Dune::FieldVector<double,mat_dimension> xProjection;
        if (len > 1e-8)
        {
          for (int j=0;j<mat_dimension;++j) {
            xProjection[j] = x*n[j];
          }
          xProjection *= len/xProjection.two_norm();
        }
        else
          xProjection = 0;
        typename LevelDF::LocalVector tmp = (*this)[lnew][hen];
        for (int k=0;k<NZ;++k) {
          for (int j=0;j<mat_dimension;++j) {
            tmp[k] += gradient_[k][j]*xProjection[j];
          }
        }
        (*this)[lnew][hen] = tmp;
      }
#else // RECONSTR_SINGLE
	    for (typename LevelDF::IntersectionIterator is = (*this)[lnew+1].gridView().ibegin(en);
           is!=isend; ++is)
	  	{
        if (is->neighbor())
        {
          const typename LevelDF::EntityPointer nbPtr = is->outside();
          const typename LevelDF::Entity &nb = *nbPtr;
          const typename LevelDF::Geometry::GlobalCoordinate
            centerNb = nb.geometry().center();
          LocalCoordinate row;
          double len=0;
          for (int k=0;k<row.dimension;++k) {
            row[k] = (centerNb - centerEn)*n[k];
            len += row[k]*row[k];
          }
          len = (centerNb - centerEn).two_norm()/sqrt(len);
          for (int k=0;k<row.dimension;++k)
             row[k] *= len;
          if ( nofNb < uNb_.size() )
          {
            uNb_[nofNb] = (*this)[lnew+1][nb] - uEn;
            omega_[nofNb]  = row;
          }
          else
          {
            uNb_.push_back( (*this)[lnew+1][nb] - uEn );
            omega_.push_back( row );
          }
          nofNb++;
        }
        else
        {
#ifndef UGGRID
          assert(0);
#endif // UGGRID
        }
      }
      const unsigned int mat_dimension = 2;
      Dune::FieldMatrix<double,mat_dimension,mat_dimension> A(0.0);
      Dune::FieldVector<double,mat_dimension> a(0.0);
      typename LevelDF::Entity::HierarchicIterator hit = en.hbegin( level+1 );
      const typename LevelDF::Entity::HierarchicIterator hend = en.hend( level+1 );
      // Use the gradient and the value on the element en
      // (stored above in uEn) to prolongate from the coarse to the fine grid
      for (;hit!=hend;++hit)
      {
        const typename LevelDF::Entity &hen = *hit;
        typename LevelDF::Geometry::GlobalCoordinate
          x = hit->geometry().center();
        LocalCoordinate row;
        double len = 0;
        for (size_t k=0;k<mat_dimension;++k) {
          row[k] = (x - centerEn)*n[k];
          len += row[k]*row[k];
        }
        if (len>1e-8)
        {
          len = (x-centerEn).two_norm()/sqrt(len);
          for (size_t k=0;k<mat_dimension;++k)
            row[k] *= len;
        }
        std::vector< std::pair<double,int> > lenNb(nofNb);
        for (size_t i=0;i<nofNb;++i)
          lenNb[i] = std::make_pair((row-omega_[i]).two_norm2(),i);
        std::sort(lenNb.begin(),lenNb.end());
        A[0][0] = omega_[lenNb[0].second][0];
        A[0][1] = omega_[lenNb[1].second][0];
        A[1][0] = omega_[lenNb[0].second][1];
        A[1][1] = omega_[lenNb[1].second][1];
        A.solve(a,row);

        typename LevelDF::LocalVector tmp = (*this)[lnew][hen];
        for (int k=0;k<NZ;++k)
        {
          for (int i=0;i<2;++i)
            tmp[k] += a[i]*uNb_[lenNb[i].second][k];
          tmp[k] += uEn[k];
        }
        (*this)[lnew][hen] = tmp;
        ;
      }
#endif // RECONSTR_SINGLE
    }
  }

  /* *************************************************************** *
   * Private class data
   * *************************************************************** */
 private:
  std::vector< LevelDF* > u_;  // Vector of discrete grid functions,
                               // representing the field on different
                               // levels
  const int numberOfLevels_; // Number of levels
  // Temporary variables, used in interpolation
  std::vector< typename LevelDF::LocalVector > uNb_;
  std::vector<std::vector<double>> dx_;

  std::vector< LocalCoordinate > omega_;
  Dune::FieldVector< typename LevelDF::Geometry::GlobalCoordinate, NZ > gradient_;
};




#endif // GRIDFUNCTION_HH

