/*** COPYRIGHT STATEMENT ************************************ 
 *
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2013]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  This code has been developed by
 *  - Andreas Dedner (University of Warwick)
 *  - Eike Mueller (University of Bath)
 *
 *** COPYRIGHT STATEMENT ************************************/

#ifndef OPERATOR_HH
#define OPERATOR_HH

#include "gridfunction.hh"

/* ********************************************************** *
 *
 *  Operator base class for representing a linear operator.
 *  Provides virtual methods for
 *  - applying operator
 *  - inverting operator exactly
 *  - inverting operator approximately
 *
 * ********************************************************** */

template <class DF>
struct Operator
{
  static const int NZ = DF::nz;

  // Contructor
  Operator() : solveSteps_(10) {}

  // Initialise operator
  virtual void initialize() const {}

  // Finalize operator
  virtual void finalize() const {}

  // Invert operator approximately: u \approx op^{-1} b
  virtual void solveApprox(const DF &b,DF &u) const = 0;

  // Invert operator: u = op^{-1} b
  virtual void solve(const DF &b, DF &u) const {
    // Calculate initial residual
    DF res(b.gridView(),b.level()), e(b.gridView(),b.level());
    residual(b,u,res);
    for (unsigned int iter=0;iter<solveSteps_;++iter) {
      e.dofs() = 0.0;
      solveApprox(res,e);
      u.dofs() += e.dofs();
      residual(b,u,res);
    }
  }

  // Apply operator
  virtual void apply(const DF &u,DF &b) const = 0;

  // Helper method res = b - A.u
  virtual void residual(const DF& b, const DF& u, DF& res) const {
    apply(u,res);
    res.dofs() *= -1.;
    res.dofs() += b.dofs();
  }

  // Calculate norm of u
  virtual double norm( const DF &u) const = 0;

  /* ********************************************************** *
   * Return and set the number of solve steps which are applied
   * to invert the operator 'exactly' in the solve() method.
   * ********************************************************** */
  unsigned int solveSteps() const { return solveSteps_; }
  void setsolveSteps(const unsigned int solveSteps) {
    solveSteps_ = solveSteps;
  }

  protected:
    unsigned int solveSteps_; // Number of iterations of loop solver
                     // for 'exact' solve
};
#endif
