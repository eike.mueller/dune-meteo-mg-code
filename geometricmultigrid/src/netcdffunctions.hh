/*** COPYRIGHT STATEMENT ************************************ 
 *
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2013]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  This code has been developed by
 *  - Andreas Dedner (University of Warwick)
 *  - Eike Mueller (University of Bath)
 *
 *** COPYRIGHT STATEMENT ************************************/

#ifndef NETCDFFUNCTIONS_HH
#define NETCDFFUNCTIONS_HH NETCDFFUNCTIONS_HH
#include <math.h>
#include <algorithm>
#include <assert.h>
#include <cstdlib>
#include <iostream>
#include <typeinfo>
#include <vector>
#include <netcdf.h>
#include <netcdf.hh>
#include<dune/common/fvector.hh>

/* ************************************************** *
 * ************************************************** *
 * Class for reading the global functions for
 * rho (density), pi (Exner pressure) and
 * theta (potential temperature) from a netCDF file.
 * ************************************************** *
 * ************************************************** *
 *
 * All variables are stored in longitude-latitude
 * coordinates and in the data type float
 *
 * Using linear interpolation, the values can be
 * obtained on a arbitrary position in the atmosphere.
 * ************************************************** */

class NetCDFfunctions {
  typedef std::vector<float> CoordType;
  typedef std::vector<std::vector<std::vector<float> > >
    FieldType;
  typedef std::vector<float> VertFieldType;
 public:
  // Constructor
  NetCDFfunctions(std::string filename, double nudt, int verbose=0) :
    filename_(filename),
    nudt_(nudt),
    Rd_(287.05),        // specific heat capacity of dry air [J kg^{-1} K^{-1}]
    cp_(1005.0),        // specific gas constant of dry air [J kg^{-1} K^{-1}]
    g_grav_(9.81),
    verbose_(verbose),
    Rearth_(6371229.0), // earth radius [m]
    math_pi_(3.1415926535897932384626433),
    deg2rad_(3.1415926535897932384626433/180.0) {
    double kappa = Rd_/cp_;
    gamma_ = (1.-kappa)/kappa;
    readFile();
  }
  // Return fields at a particular position in the atmosphere
  // *** Theta ***
  template <class FieldVector>
  double getTheta(const FieldVector &x,
                  const double r) const {
    return interpolate(hybrid_ht_[0],theta_,x,r,true,false);
  }
  double getTheta_r(const double r, int index=0) const {
    return interpolate(hybrid_ht_[0],vert_theta_[index],r,true,false);
  }
  // *** dTheta/dr ***
  template <class FieldVector>
  double getdThetadr(const FieldVector &x,
                     const double r) const {
    return interpolate(hybrid_ht_[0],dtheta_dr_,x,r,false,true);
  }
  double getdThetadr_r(const double r,int index=0) const {
    return interpolate(hybrid_ht_[0],vert_dtheta_dr_[index],r,false,true);
  }
  // *** a_hh ***
  template <class FieldVector>
  double geta_hh(const FieldVector &x,
                      const double r) const {
    return interpolate(hybrid_ht_[0],a_hh_,x,r);
  }
  double geta_hh_r(const double r,int index=0) const {
    return interpolate(hybrid_ht_[0],vert_a_hh_[index],r);
  }
  // *** a_rr ***
  template <class FieldVector>
  double geta_rr(const FieldVector &x,
                      const double r) const {
    return interpolate(hybrid_ht_[0],a_rr_,x,r);
  }
  double geta_rr_r(const double r,int index=0) const {
    return interpolate(hybrid_ht_[0],vert_a_rr_[index],r);
  }
  // *** a_r ***
  template <class FieldVector>
  double geta_r(const FieldVector &x,
                      const double r) const {
    return interpolate(hybrid_ht_[0],a_r_,x,r);
  }
  double geta_r_r(const double r, int index=0) const {
    return interpolate(hybrid_ht_[0],vert_a_r_[index],r);
  }
  // *** b ***
  template <class FieldVector>
  double getb(const FieldVector &x,
              const double r) const {
    return interpolate(hybrid_ht_[0],b_,x,r);
  }
  double getb_r(const double r, int index=0) const {
    return interpolate(hybrid_ht_[0],vert_b_[index],r);
  }
  // *** Rho ***
  template <class FieldVector>
  double getRho(const FieldVector &x,
                const double r) const {
    return interpolate(hybrid_ht_[1],rho_,x,r)/(r*r*Rearth_*Rearth_);
  }
  double getRho_r(const double r, int index=0) const {
    return interpolate(hybrid_ht_[1],vert_rho_[index],r)/(r*r*Rearth_*Rearth_);
  }
  // *** Pi ***
  template <class FieldVector>
  double getPi(const FieldVector &x,
               const double r) const {
    return interpolate(hybrid_ht_[2],pi_,x,r);
  }
  double getPi_r(const double r, int index=0) const {
    return interpolate(hybrid_ht_[2],vert_pi_[index],r);
  }
  // Sort theta to ensure stability
  void sort_theta();
  // Calculate profiles a_hh, a_rr, a_h and b
  void calculate_profiles();
  // Return height of atmosphere (km)
  double getRlid() { return Rlid_;}
  // Return earth radius
  double getRearth() const { return Rearth_; }
  // Return height of atmosphere (in units of the earth radius)
  double getAspect() { return Rlid_/Rearth_; }
  // Return grid arrays
  CoordType getLatitude() { return latitude_; }
  CoordType getLongitude() { return longitude_; }
  std::vector<CoordType> getHybrid_ht() { return hybrid_ht_; }
 private:
  // read file from disk
  void readFile();
  // Average a field in the horizontal direction to get a
  // vertical-only profile
  void averageHorizontal(const FieldType& field,
                         std::vector<VertFieldType>& vertfield);
  void abort(const std::string message) {
    std::cout << "ERROR: "<< message << std::endl;
    exit(-1);
  }
  // read single variable from file
  // 1d field (coordinates)
  void readCoord(const std::string varName,
                 CoordType &var,
                 const bool convert2rad=false);
  // 3d field (rho, pi, theta)
  void readField(const std::string varName,
                 const size_t n_z,
                 const size_t n_y,
                 const size_t n_x,
                 FieldType &var);
  // Calculate and store vertical derivative of theta
  void calculate_vertical_derivative(const CoordType &r_vert,
                                     const FieldType& field,
                                     FieldType& dfield_dr);
  // get index of coordinate before x
  int coordIndex(const CoordType &coord,
                 const double x,
                 const bool isRegular=false) const;
  // Interpolate a given field with given vertical coordinates
  template <class FieldVector>
  double interpolate(const CoordType &coord_vert,
                     const FieldType& field,
                     const FieldVector &x,
                     const double r,
                     const bool is_theta=false,
                     const bool is_dthetadr=false) const;
  double interpolate(const CoordType &coord_vert,
                     const VertFieldType& field,
                     const double r,
                     const bool is_theta=false,
                     const bool is_dthetadr=false) const;
  // File handle
  NcFile* nc_file_;
  // name of netCDF file to read
  std::string filename_;
  // time step size
  const double nudt_;
  // Gravitational acceleration
  const double g_grav_;
  // verbosity level, controls output
  int verbose_;
  // latitude and longitude grid
  CoordType latitude_;
  CoordType longitude_;
  // Vertical grids (hybrid_ht, hybrid_ht_1, hybrid_ht_2)
  std::vector<CoordType> hybrid_ht_;
  // 3d fields
  FieldType rho_;
  FieldType theta_;
  FieldType dtheta_dr_;
  FieldType pi_;
  FieldType a_rr_;
  FieldType a_hh_;
  FieldType a_r_;
  FieldType b_;
  // 1d fields (vertical only)
  std::vector<VertFieldType> vert_rho_;
  std::vector<VertFieldType> vert_theta_;
  std::vector<VertFieldType> vert_dtheta_dr_;
  std::vector<VertFieldType> vert_pi_;
  std::vector<VertFieldType> vert_a_rr_;
  std::vector<VertFieldType> vert_a_r_;
  std::vector<VertFieldType> vert_a_hh_;
  std::vector<VertFieldType> vert_b_;
  const double Rd_;
  const double cp_;
  // Earth radius
  double Rearth_;
  // pi
  double math_pi_;
  // Conversion factor between degrees and radians (pi/180)
  double deg2rad_;
  // Atmospheric lid (in kilometers)
  double Rlid_;
  double gamma_;
};

/* ************************************************** *
 * Read 3d variable
 * ************************************************** */
void NetCDFfunctions::readField(const std::string varName,
                                const size_t n_z,
                                const size_t n_y,
                                const size_t n_x,
                                FieldType &var) {
  NcVar *var_tmp = nc_file_->get_var(varName.c_str());
  float* tmp = new float[n_x];
  var.resize(n_z);
  // Loop over vertical levels and latitude
  for (size_t i_z=0;i_z<n_z;++i_z) {
    var[i_z].resize(n_y);
    for (size_t i_y=0;i_y<n_y;++i_y) {
      // Read data along one latitude circle and store in var
      var[i_z][i_y].resize(n_x);
      var_tmp->set_cur(0,i_z,i_y,0);
      var_tmp->get(tmp,1,1,1,n_x);
      for (size_t i_x=0;i_x<n_x;++i_x) {
        var[i_z][i_y][i_x] =tmp[i_x];
      }
    }
  }
  delete [] tmp;
}

/* ************************************************** *
 * Average a field in the horizontal direction, i.e.
 * calculate a field which depends on the vertical
 * direction only.
 * ************************************************** */
void NetCDFfunctions::
  averageHorizontal(const FieldType& field,
                    std::vector<VertFieldType>& vertfield) {
  size_t n_x = longitude_.size();
  size_t n_y = latitude_.size();
  size_t n_z = field.size();
  // allocate memory
  vertfield.resize(3);
  for (int i=0;i<3;++i)
    vertfield[i].resize(n_z);
  for (size_t k=0;k<n_z;++k) {
    double total_area = 0.0;
    double sum = 0.0;
    double max_val = -1.e20;
    double min_val = 1.e20;
    for (size_t j=0;j<n_y;++j) {
      // Calculate area of horizontal cell
      double lat_m, lat_p;
      if (j>0) {
        lat_m = 0.5*(latitude_[j-1]+latitude_[j]);
      } else {
        lat_m = latitude_[j];
      }
      if (j<n_y-1) {
        lat_p = 0.5*(latitude_[j+1]+latitude_[j]);
      } else {
        lat_p = latitude_[j];
      }
      double local_area = (sin(lat_p)-sin(lat_m))*2.*math_pi_;
      total_area += local_area;
      double lon_avg = 0.0;
      // Average over longitude
      for (size_t i=0;i<n_x;++i) {
        double f_local = field[k][j][i];
        lon_avg += f_local;
        max_val = std::max(max_val,f_local);
        min_val = std::min(min_val,f_local);
      }
      lon_avg *= 1./n_x;
      sum += local_area*lon_avg;
    }
    vertfield[0][k] = sum / total_area;
    vertfield[1][k] = min_val;
    vertfield[2][k] = max_val;
  }
}

/* ************************************************** *
 * Calculate derivative of field at every
 * vertical grid-point
 * ************************************************** */
void NetCDFfunctions::
  calculate_vertical_derivative(const CoordType &r_vert,
                                const FieldType& field,
                                FieldType& dfield_dr) {
  size_t n_x = longitude_.size();
  size_t n_y = latitude_.size();
  size_t n_z = r_vert.size();
  // Allocate memory
  dfield_dr.resize(n_z);
  for (size_t k=0;k<n_z;++k) {
    dfield_dr[k].resize(n_y);
    for (size_t j=0;j<n_y;++j) {
      dfield_dr[k][j].resize(n_x);
    }
  }

  // Bottom and top boundary: Use one-sided finite differences
  for (size_t j=0;j<n_y;++j) {
    for (size_t i=0;i<n_x;++i) {
      dfield_dr[0][j][i] = (field[1][j][i] - field[0][j][i])
                          / (r_vert[1] - r_vert[0]) * Rearth_;
      dfield_dr[n_z-1][j][i] = (field[n_z-1][j][i] - field[n_z-2][j][i])
                              / (r_vert[n_z-1] - r_vert[n_z-2]) * Rearth_;
    }
  }
  // All interior points: Use two-sided finite differences by fitting
  // a quadratic function through the point itself and its neighbours
  for (size_t k=1;k<n_z-1;++k) {
    double x_m = r_vert[k-1] - r_vert[k];
    double x_p = r_vert[k+1] - r_vert[k];
    for (size_t j=0;j<n_y;++j) {
      for (size_t i=0;i<n_x;++i) {
        double f_0 = field[k  ][j][i];
        double f_p = field[k+1][j][i];
        double f_m = field[k-1][j][i];
        dfield_dr[k][j][i] = (x_m*x_m*(f_p-f_0) - x_p*x_p*(f_m-f_0))
                            / (x_p*x_m*(x_m-x_p)) * Rearth_;
      }
    }
  }
}

/* ************************************************** *
 * Read 1d variable
 * ************************************************** */
void NetCDFfunctions::readCoord(const std::string varName,
                                CoordType &var,
                                const bool convert2rad) {
  NcDim *dim = nc_file_->get_dim(varName.c_str());
  size_t n = dim->size();
  if (verbose_ > 0)
    std::cout << "Size of " << varName << " = \'" << n << "\'" << std::endl;
  NcVar *var_tmp = nc_file_->get_var(varName.c_str());
  float* tmp = new float[n];
  var_tmp->get(tmp,n);
  var.resize(n);
  for (size_t i=0;i<n;++i) {
    if (convert2rad) {
      var[i] =tmp[i]*deg2rad_;
    } else {
      var[i] =tmp[i];
    }
  }
  delete [] tmp;
}

/* ************************************************** *
 * Read data arrays from netCDF file
 * ************************************************** */
void NetCDFfunctions::readFile() {
  if (verbose_ > 0)
    std::cout << "Reading netCDF file \'" << filename_ << "\'" << std::endl;
  nc_file_ = new NcFile(filename_.c_str());

  // *** Read latitude array
   readCoord("latitude",latitude_,true);

  // *** Read longitude array
  readCoord("longitude",longitude_,true);

  // *** Read hybrid height arrays
  std::vector<std::string> hybrid_ht_label;
  hybrid_ht_label.push_back("hybrid_ht");
  hybrid_ht_label.push_back("hybrid_ht_1");
  hybrid_ht_label.push_back("hybrid_ht_2");
  CoordType hybrid_ht_tmp;
  Rlid_ = 0.0;
  for (size_t i=0;i<3;++i) {
    readCoord(hybrid_ht_label[i],hybrid_ht_tmp);
    hybrid_ht_.push_back(hybrid_ht_tmp);
    double maxR = hybrid_ht_tmp[hybrid_ht_tmp.size()-1];
    if (maxR > Rlid_) Rlid_ = maxR;
  }

  // *** Read theta array
  readField("theta",
            hybrid_ht_[0].size(),
            latitude_.size(),
            longitude_.size(),
            theta_);
  //sort_theta();
  // Calculate dtheta_dr
  calculate_vertical_derivative(hybrid_ht_[0],
                                theta_,
                                dtheta_dr_);
  // *** Read density array
  readField("unspecified",
            hybrid_ht_[1].size(),
            latitude_.size(),
            longitude_.size(),
            rho_);

  // *** Read Exner pressure array
  readField("field7",
            hybrid_ht_[2].size(),
            latitude_.size(),
            longitude_.size(),
            pi_);

  calculate_profiles();

  // Average fields in horizontal
  averageHorizontal(theta_,vert_theta_);
  averageHorizontal(dtheta_dr_,vert_dtheta_dr_);
  averageHorizontal(pi_,vert_pi_);
  averageHorizontal(rho_,vert_rho_);
  averageHorizontal(a_rr_,vert_a_rr_);
  averageHorizontal(a_hh_,vert_a_hh_);
  averageHorizontal(a_r_,vert_a_r_);
  averageHorizontal(b_,vert_b_);
}

/* ************************************************** *
 * Given a coordinate field vector, work out the
 * corresponding index in the field for a given
 * position.
 * ************************************************** */
int NetCDFfunctions::
  coordIndex(const CoordType &coord,
             const double x,
             const bool isRegular) const {
  size_t n_coord = coord.size();
  float x_min = coord[0];
  float x_max = coord[n_coord-1];
  int i_coord = -99;
  if (x < x_min) {
    i_coord = -1;
  } else if(x > x_max) {
    i_coord = n_coord-1;
  } else {
    if (isRegular) {
      // *** Case 1 *** Grid is regular
      i_coord = int((x-x_min)/(x_max-x_min)*(n_coord-1));
    } else {
      // *** Case 2 *** Grid is NOT regular
      for (size_t i = 0;i<n_coord-1;++i) {
        if ( x <= coord[i+1] ) {
          i_coord = i;
          break;
        }
      }
    }
  }
  return i_coord;
}

/* ************************************************** *
 * Interpolate 3d field to a given position
 *
 *  Assume that:
 *   - lon = 0...2*pi
 *   - lat = -pi...pi
 *   - r   = 1 ... 1 + Rlid_/Rearth_
 *
 * ************************************************** */
template <class FieldVector>
  double NetCDFfunctions::
    interpolate(const CoordType &coord_vert,
                const FieldType& field,
                const FieldVector &x,
                const double r,
                const bool is_theta,
                const bool is_dthetadr) const {
  double lon = atan2(x[1],x[0])+math_pi_;
  double lat = atan2(x[2],sqrt(x[0]*x[0]+x[1]*x[1]));
  // Calculate position in data arrays
  double res=0.0;
  if (lat >= 0.5*math_pi_) lat = 0.49999*math_pi_;
  double z = (r-1.)*Rearth_;
  assert(z>=0);
  assert(z<=Rlid_);
  int i_lon = coordIndex(longitude_,lon,true);
  assert(i_lon>=0);
  assert(i_lon<longitude_.size());
  int i_lon_p = i_lon+1;
  // Treat periodic coordinate in longitude direction:
  double rho_lon_shift = 0.;
  if (i_lon_p == longitude_.size()) {
    i_lon_p = 0;
    rho_lon_shift = 2.*math_pi_;
  }
  float rho_lon = (lon - longitude_[i_lon])
                / (longitude_[i_lon_p]+rho_lon_shift-longitude_[i_lon]);
  int i_lat = coordIndex(latitude_,lat,true);
  assert(i_lat>=0);
  assert(i_lat<latitude_.size());
  float rho_lat = (lat - latitude_[i_lat])
                / (latitude_[i_lat+1]-latitude_[i_lat]);
  int i_z = coordIndex(coord_vert,z,false);

  int z_level[2];
  if (i_z < 0) {
    z_level[0] = 0;
    z_level[1] = 1;
  } else if (i_z >= coord_vert.size()-1) {
    z_level[0] = coord_vert.size()-1;
    z_level[1] = coord_vert.size()-2;
  } else {
    z_level[0] = i_z;
    z_level[1] = i_z+1;
  }
  double rho_z = (z - coord_vert[z_level[0]])
               / (coord_vert[z_level[1]] - coord_vert[z_level[0]]);
  double res_tmp[2];
  for (size_t dz=0;dz<2;++dz) {
    res_tmp[dz]  = (1.-rho_lon) * (1.-rho_lat)
                 * field[z_level[dz]][i_lat  ][i_lon  ];
    res_tmp[dz] +=     rho_lon  * (1.-rho_lat)
                 * field[z_level[dz]][i_lat  ][i_lon_p];
    res_tmp[dz] += (1.-rho_lon) *     rho_lat
                 * field[z_level[dz]][i_lat+1][i_lon  ];
    res_tmp[dz] +=     rho_lon  *     rho_lat
                 * field[z_level[dz]][i_lat+1][i_lon_p];
  }
  if ( (i_z < 0) && (is_theta) ){
    return res_tmp[0];
  }
  if ( (i_z < 0) && (is_dthetadr) ){
    return 0.0;
  }
  return (1.-rho_z) * res_tmp[0] + rho_z * res_tmp[1];
}

/* ************************************************** *
 * Interpolate 1d (vertical) field to a given position
 *
 *  Assume that:
 *   - r   = 1 ... 1 + Rlid_/Rearth_
 *
 * ************************************************** */
double NetCDFfunctions::interpolate(const CoordType &coord_vert,
              const VertFieldType& vert_field,
              const double r,
              bool is_theta,
              bool is_dthetadr) const {
  // Calculate position in data arrays
  double res=0.0;
  double z = (r-1.)*Rearth_;
  assert(z>=0);
  assert(z<=Rlid_);
  int i_z = coordIndex(coord_vert,z,false);
  int z_level[2];
  if (i_z < 0) {
    z_level[0] = 0;
    z_level[1] = 1;
  } else if (i_z >= coord_vert.size()-1) {
    z_level[0] = coord_vert.size()-1;
    z_level[1] = coord_vert.size()-2;
  } else {
    z_level[0] = i_z;
    z_level[1] = i_z+1;
  }
  double rho_z = (z - coord_vert[z_level[0]])
               / (coord_vert[z_level[1]] - coord_vert[z_level[0]]);
  if ( (i_z < 0) && (is_theta) ) {
    return vert_field[z_level[0]];
  }
  if ( (i_z < 0) && (is_dthetadr) ) {
    return 0.0;
  }
  return (1.-rho_z) * vert_field[z_level[0]]
      +      rho_z * vert_field[z_level[1]];
}

/* ************************************************** *
 * Sort theta in ascending order in each column
 * ************************************************** */
void NetCDFfunctions::sort_theta() {
  size_t n_z = hybrid_ht_[0].size();
  size_t n_x = longitude_.size();
  size_t n_y = latitude_.size();
  std::vector<float> theta_col(n_z);
  for (size_t i=0;i<n_x;++i) {
    for (size_t j=0;j<n_y;++j) {
      for (size_t k=0;k<n_z;++k)
        theta_col[k] = theta_[k][j][i];
      std::sort(theta_col.begin(),theta_col.end());
      for (size_t k=0;k<n_z;++k)
        theta_[k][j][i] = theta_col[k];
    }
  }
}

/* ************************************************** *
 * Calculate a_rr, a_hh, a_r and b
 * ************************************************** */
void NetCDFfunctions::calculate_profiles() {
  size_t n_z = hybrid_ht_[0].size();
  size_t n_x = longitude_.size();
  size_t n_y = latitude_.size();
  a_rr_.resize(n_z);
  a_hh_.resize(n_z);
  a_r_.resize(n_z);
  b_.resize(n_z);

  for (size_t k=0;k<n_z;++k) {
    a_rr_[k].resize(n_y);
    a_hh_[k].resize(n_y);
    a_r_[k].resize(n_y);
    b_[k].resize(n_y);
    double z = hybrid_ht_[0][k];
    int i_z = coordIndex(hybrid_ht_[1],z,false);

    int z_level_rho[2];
    if (i_z < 0) {
      z_level_rho[0] = 0;
      z_level_rho[1] = 1;
    } else if (i_z >= hybrid_ht_[1].size()-1) {
      z_level_rho[0] = hybrid_ht_[1].size()-1;
      z_level_rho[1] = hybrid_ht_[1].size()-2;
    } else {
      z_level_rho[0] = i_z;
      z_level_rho[1] = i_z+1;
    }
    double rho_z_rho = (z - hybrid_ht_[1][z_level_rho[0]])
                 / (hybrid_ht_[1][z_level_rho[1]] - hybrid_ht_[1][z_level_rho[0]]);

    int z_level_pi[2];
    if (i_z < 0) {
      z_level_pi[0] = 0;
      z_level_pi[1] = 1;
    } else if (i_z >= hybrid_ht_[2].size()-1) {
      z_level_pi[0] = hybrid_ht_[2].size()-1;
      z_level_pi[1] = hybrid_ht_[2].size()-2;
    } else {
      z_level_pi[0] = i_z;
      z_level_pi[1] = i_z+1;
    }
    double rho_z_pi = (z - hybrid_ht_[2][z_level_pi[0]])
                  / (hybrid_ht_[2][z_level_pi[1]] - hybrid_ht_[2][z_level_pi[0]]);

    for (size_t j=0;j<n_y;++j) {
      a_rr_[k][j].resize(n_x);
      a_hh_[k][j].resize(n_x);
      a_r_[k][j].resize(n_x);
      b_[k][j].resize(n_x);
      for (size_t i=0;i<n_x;++i) {
        double theta = theta_[k][j][i];
        double rho = (1.-rho_z_rho) * rho_[z_level_rho[0]][j][i]
                   +      rho_z_rho * rho_[z_level_rho[1]][j][i];
        double pi = (1.-rho_z_pi) * pi_[z_level_pi[0]][j][i]
                  +      rho_z_pi * pi_[z_level_pi[1]][j][i];
        double r = 1.+hybrid_ht_[0][k]/Rearth_;
        double dtheta_dr = dtheta_dr_[k][j][i];
        double Lambda = 1./(1.+nudt_*nudt_*g_grav_/Rearth_*dtheta_dr/theta);
        a_rr_[k][j][i] = Lambda*rho*theta/(Rearth_*Rearth_);
        a_hh_[k][j][i] = rho*theta/(r*r*Rearth_*Rearth_);
        a_r_[k][j][i] = Lambda*rho*dtheta_dr/(r*r*Rearth_*Rearth_);
        b_[k][j][i] = gamma_*(rho/pi)/(r*r*Rearth_*Rearth_);
      }
    }
  }
}

#endif // NETCDFFUNCTIONS_HH
