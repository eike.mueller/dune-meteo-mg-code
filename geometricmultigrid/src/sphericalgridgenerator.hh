/*** COPYRIGHT STATEMENT ************************************ 
 *
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2013]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  This code has been developed by
 *  - Andreas Dedner (University of Warwick)
 *  - Eike Mueller (University of Bath)
 *
 *** COPYRIGHT STATEMENT ************************************/

#ifndef SPHERICALGRIDGENERATOR_HH
#define SPHERICALGRIDGENERATOR_HH SPHERICALGRIDGENERATOR_HH

#include<string>
#include<iostream>
#include<fstream>
#include<regex.h>
#include <dune/grid/uggrid.hh>
#include <dune/common/exceptions.hh>
#include "sphericalboundarysegment.hh"

/* *************************************************************** *
 * Exceptions
 * *************************************************************** */
class SphericalGridGeneratorException : public Dune::Exception {
};

/* *************************************************************** *
 * Read spherical grid from .dgf file and insert boundary segments
 * *************************************************************** */

class SphericalGridGenerator {
  public:

    static const int dim = 3;
    static const int dimworld = 3;
    typedef Dune::UGGrid < dim > Grid;

  /* *************************************************************** *
   * Constructor
   * *************************************************************** */
    SphericalGridGenerator(std::string filename,
                           const int refcount=0) :
      filename_(filename),
      refcount_(refcount),
      nElements(0),
      nVertices(0) {
      int rc;
      rc = regcomp(&regexHeader_,
                   "^ *DGF *$",
                   REG_EXTENDED);
      rc = regcomp(&regexElVert_,
                   "^\% *Elements *= *([0-9]+) *\\| *Vertices *= *([0-9]+) *$",
                   REG_EXTENDED);
      rc = regcomp(&regexVertexHeader_,
                   "^ *VERTEX *$",
                   REG_EXTENDED);
      rc = regcomp(&regexVertex_,
                   "^ *([+-]?[0-9]*\\.?[0-9]*[Ee][+-]?[0-9]+?) *([+-]?[0-9]*\\.?[0-9]*[Ee][+-]?[0-9]+) *([+-]?[0-9]*\\.?[0-9]*[Ee][+-]?[0-9]+) *$",
                   REG_EXTENDED);
      rc = regcomp(&regexCubeHeader_,
                   "^ *CUBE *$",
                   REG_EXTENDED);
      rc = regcomp(&regexCube_,
                   "^ *([0-9]+) *([0-9]+) *([0-9]+) *([0-9]+) *([0-9]+) *([0-9]+) *([0-9]+) *([0-9]+) *$",
                   REG_EXTENDED);
      rc = regcomp(&regexPrismHeader_,
                   "^ *PRISM *$",
                   REG_EXTENDED);
      rc = regcomp(&regexPrism_,
                   "^ *([0-9]+) *([0-9]+) *([0-9]+) *([0-9]+) *([0-9]+) *([0-9]+) *$",
                   REG_EXTENDED);
      rc = regcomp(&regexBoundarySegmentsHeader_,
                   "^ *BOUNDARYSEGMENTS *$",
                   REG_EXTENDED);
      rc = regcomp(&regexBoundarySegments_,
                   "^ *([0-9]+) *([0-9]+) *([0-9]+) *([0-9]+) *([0-9]*)? *$",
                   REG_EXTENDED);
      createGrid();
    }

  /* *************************************************************** *
   * Destructor
   * *************************************************************** */
    ~SphericalGridGenerator() { delete(gridPtr); }

  /* *************************************************************** *
   * return pointer to grid
   * *************************************************************** */
    Grid* getgridPtr() { return gridPtr; }

  protected:
  /* *************************************************************** *
   * Generate grid from file
   * *************************************************************** */
    void createGrid() {
      // Create grid
      char line[linewidth];
      // Open file for reading
      std::ifstream input(filename_.c_str(),std::ios::in);
      std::string header;
      // Check header (i.e. first line of file)
      input.getline(line,linewidth);
      regmatch_t matchHeader[1];
      if (regexec(&regexHeader_,line,1,matchHeader,0)) {
        DUNE_THROW(SphericalGridGeneratorException,"ERROR: File \'" << filename_ << "\' is not a .dgf file.");
      }
      while(input) {
        input.getline(line,linewidth);
        std::string sline(line);
        // Look for vertex/element information
        regmatch_t matchElVert[3];
        if (!regexec(&regexElVert_,line,3,matchElVert,0)) {
            std::string elements = sline.substr(matchElVert[1].rm_so,
                                                matchElVert[1].rm_eo);
            std::string vertices = sline.substr(matchElVert[2].rm_so,
                                                matchElVert[2].rm_eo);
            nElements = atoi(elements.c_str());
            nVertices = atoi(vertices.c_str());
            vertices_.resize(nVertices);
        }
        // Find vertex header
        regmatch_t matchVertexHeader[1];
        if (!regexec(&regexVertexHeader_,line,1,matchVertexHeader,0)) {
          readVertices(input);
        }
        // Find cube header
        regmatch_t matchCubeHeader[1];
        if (!regexec(&regexCubeHeader_,line,1,matchCubeHeader,0)) {
          readCubes(input);
        }
        // Find prism header
        regmatch_t matchPrismHeader[1];
        if (!regexec(&regexPrismHeader_,line,1,matchPrismHeader,0)) {
          readPrisms(input);
        }
        // Find boundary segments header
        regmatch_t matchBoundarySegmentsHeader[1];
        if (!regexec(&regexBoundarySegmentsHeader_,line,1,matchBoundarySegmentsHeader,0)) {
          readBoundarySegments(input);
        }

      }
      // Close file
      input.close();
      gridPtr = factory_.createGrid();
      gridPtr->loadBalance();

      /* *** Refine grid *** */
      typedef Grid::LeafGridView GV ;
      GV gv = gridPtr->leafView();
      typedef GV::Codim<0>::Iterator ElementLeafIterator;
      typedef ElementLeafIterator::Entity Element;
      typedef Element::Geometry LeafGeometry;
      ElementLeafIterator ibegin = gv.begin<0>();
      ElementLeafIterator iend = gv.end<0>();
      for (int iref =0;iref<refcount_;++iref) {
        for (ElementLeafIterator it = ibegin;it!=iend;++it) {
          Dune::UG_NS<dim>::RefinementRule refinementrule;
          Element& e = *it;
          if (e.type().isCube()) {
            refinementrule = UG::D3::HEX_QUADSECT_0;
          }
          if (e.type().isPrism()) {
            refinementrule = UG::D3::PRISM_QUADSECT;
          }
          gridPtr->mark(*it,refinementrule,0);
        }

        gridPtr->adapt();
        gridPtr->postAdapt();
      }
    }

  /* *************************************************************** *
   * Parse section with vertices
   * *************************************************************** */
    void readVertices(std::ifstream& input) {
      int iVertex=0;
      char line[linewidth];
      while(input) {
        input.getline(line,linewidth);
        std::string sline(line);
        regmatch_t matchVertex[4];
        if (!regexec(&regexVertex_,line,4,matchVertex,0)) {
          Dune::FieldVector<double,dim> vertex(0.0);
          for (int i=0;i<3;++i) {
            std::string tmp = sline.substr(matchVertex[i+1].rm_so,
                                           matchVertex[i+1].rm_eo);
            vertex[i] = atof(tmp.c_str());
          }
          vertices_[iVertex] = vertex;
          factory_.insertVertex(vertex);
          iVertex++;
        } else {
          break;
        }
      }
      if (nVertices != iVertex) {
        DUNE_THROW(SphericalGridGeneratorException,"Number of vertices differs from header information. Expected: " << nVertices << "Found: " << iVertex);
      }
    }

  /* *************************************************************** *
   * Parse section with prisms
   * *************************************************************** */
    void readPrisms(std::ifstream& input) {
      const Dune::GeometryType prism(Dune::GeometryType::prism,dim);
      std::vector<unsigned int> edges(6);
      char line[linewidth];
      while(input) {
        input.getline(line,linewidth);
        std::string sline(line);
        regmatch_t matchPrism[7];
        if (!regexec(&regexPrism_,line,7,matchPrism,0)) {
          for (int i=0;i<6;++i) {
            std::string tmp = sline.substr(matchPrism[i+1].rm_so,
                                           matchPrism[i+1].rm_eo);
            edges[i] = atoi(tmp.c_str());
          }
          factory_.insertElement(prism,edges);
        } else {
          break;
        }
      }
    }

  /* *************************************************************** *
   * Parse section with cubes
   * *************************************************************** */
    void readCubes(std::ifstream& input) {
      const Dune::GeometryType cube(Dune::GeometryType::cube,dim);
      std::vector<unsigned int> edges(8);
      char line[linewidth];
      while(input) {
        input.getline(line,linewidth);
        std::string sline(line);
        regmatch_t matchCube[9];
        if (!regexec(&regexCube_,line,9,matchCube,0)) {
          for (int i=0;i<8;++i) {
            std::string tmp = sline.substr(matchCube[i+1].rm_so,
                                           matchCube[i+1].rm_eo);
            edges[i] = atoi(tmp.c_str());
          }
          factory_.insertElement(cube,edges);
        } else {
          break;
        }
      }
    }

  /* *************************************************************** *
   * Parse section with boundary segments
   * *************************************************************** */
    void readBoundarySegments(std::ifstream& input) {
      int iSegment=0;
      char line[linewidth];
      while(input) {
        input.getline(line,linewidth);
        std::string sline(line);
        regmatch_t matchBoundarySegments[6];
        if (!regexec(&regexBoundarySegments_,line,6,matchBoundarySegments,0)) {
          std::vector<Dune::FieldVector<double,dim> > x;
          std::vector<unsigned int> element;
          for (int i=0;i<4;++i) {
            std::string tmp = sline.substr(matchBoundarySegments[i+2].rm_so,
                                           matchBoundarySegments[i+2].rm_eo-matchBoundarySegments[i+2].rm_so);
            if (tmp != "") {
              int iVertex = atoi(tmp.c_str());
              element.push_back(iVertex);
              x.push_back(vertices_[iVertex]);
            }
          }
          double Rsphere = x[0].two_norm();
          std::shared_ptr<Dune::BoundarySegment<dim,dimworld>> ptr(new SphericalBoundarySegment(x,Rsphere));
          factory_.insertBoundarySegment(element,ptr);
          iSegment++;
        } else {
          break;
        }
      }
    }

  protected:
    // Pointer to grid
    Grid* gridPtr;
    // Grid factory for generating grid
    Dune::GridFactory<Grid> factory_;
    // Name of .dgf file
    std::string filename_;
    // Number of refinement steps
    int refcount_;
    // Vector with vertex coordinates
    std::vector<Dune::FieldVector<double, dim> > vertices_;
    // Regular expressions
    regex_t regexHeader_;
    regex_t regexElVert_;
    regex_t regexVertexHeader_;
    regex_t regexVertex_;
    regex_t regexCubeHeader_;
    regex_t regexCube_;
    regex_t regexPrismHeader_;
    regex_t regexPrism_;
    regex_t regexBoundarySegmentsHeader_;
    regex_t regexBoundarySegments_;
    // Number of vertices and elements
    unsigned int nVertices;
    unsigned int nElements;
    // Maximal width of lines read from .dgf file
    static const int linewidth=256;
};

#endif // SPHERICALGRIDGENERATOR_HH

