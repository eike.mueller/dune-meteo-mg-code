/*** COPYRIGHT STATEMENT ************************************ 
 *
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2013]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  This code has been developed by
 *  - Andreas Dedner (University of Warwick)
 *  - Eike Mueller (University of Bath)
 *
 *** COPYRIGHT STATEMENT ************************************/

#ifndef GRIDPARAMETERS_HH
#define GRIDPARAMETERS_HH
/* ********************************************************** *
 *
 *  Parameters of grid
 *
 * ********************************************************** */
#include<parameters.hh>

template <int NZ>
class GridParameters : public Parameters {
 public:
  static const int nz = NZ; // Number of vertical levels (cells)
  // Constructor
  GridParameters() :
                  Parameters("grid"),
                  refcount_(0),
                  H_bottom_(0.0),
                  H_(1.0),
                  graded_(false),
                  ugheapsize_(8000) {
      addKey("refcount",Integer);
      addKey("gridfile",String);
      addKey("Hbottom",Double);
      addKey("H",Double);
      addKey("graded",Bool);
      addKey("ugheapsize",Integer);
      setVerticalLevels();
    }

  //
  int readFile(const std::string filename) {
    int readSuccess = Parameters::readFile(filename);
    if (!readSuccess) {
      refcount_ = contents["refcount"]->getInt();
      gridFilename_ = contents["gridfile"]->getString();
      H_ = contents["H"]->getDouble();
      H_bottom_ = contents["Hbottom"]->getDouble();
      graded_ = contents["graded"]->getBool();
      ugheapsize_ = contents["ugheapsize"]->getInt();
      setVerticalLevels();
    }
    return readSuccess;
  }

  // Return data
  unsigned int refcount() const { return refcount_; }
  double H() const { return H_; }
  double H_bottom() const { return H_bottom_; }
  double graded() const { return graded_; }
  std::string gridFilename() const { return gridFilename_; }
  Dune::FieldVector<double,NZ+1> r() const { return r_; }
  unsigned int ugheapsize() const { return ugheapsize_; }

 private:
  unsigned int refcount_;
  double H_;
  double H_bottom_;
  bool graded_;
  std::string gridFilename_;
  Dune::FieldVector<double,NZ+1> r_;
  unsigned int ugheapsize_;

  // set vertical levels
  void setVerticalLevels() {
    double z;
    for(int k=0;k<=nz;k++) {
      if (graded_)
        z = H_bottom_ + (H_-H_bottom_)*(1.*k/nz)*(1.*k/nz);
      else
        z = H_bottom_ + (H_-H_bottom_)*(1.*k/nz);
#ifdef CARTESIANGEOMETRY
      r_[k] = z;
#else
      r_[k] = 1.+z;
#endif
    }
  }
};

#endif // GRIDPARAMETERS_HH
