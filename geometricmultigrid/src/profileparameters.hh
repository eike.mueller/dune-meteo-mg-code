/*** COPYRIGHT STATEMENT ************************************ 
 *
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2013]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  This code has been developed by
 *  - Andreas Dedner (University of Warwick)
 *  - Eike Mueller (University of Bath)
 *
 *** COPYRIGHT STATEMENT ************************************/

#ifndef PROFILEPARAMETERS_HH
#define PROFILEPARAMETERS_HH
/* ********************************************************** *
 *
 *  Parameters of profile
 *
 * ********************************************************** */
#include "parameters.hh"

class ProfileParameters : public Parameters {
 public:
  // Constructor
  ProfileParameters() :
                    Parameters("profile"),
                    omega2_(1.0),
                    lambda2_(1.0),
                    nudt_(1.0),
                    Nbuoyancy_(0.01),
                    filename_("") {
      addKey("nudt",Double);
      addKey("omega2",Double);
      addKey("lambda2",Double);
      addKey("Nbuoyancy",Double);
      addKey("filename",String);
      addKey("factorizeVertical",Bool);
    }

  //
  int readFile(const std::string filename) {
    int readSuccess = Parameters::readFile(filename);
    if (!readSuccess) {
      omega2_ = contents["omega2"]->getDouble();
      lambda2_ = contents["lambda2"]->getDouble();
      nudt_ = contents["nudt"]->getDouble();
      Nbuoyancy_ = contents["Nbuoyancy"]->getDouble();
      filename_ = contents["filename"]->getString();
      factorizeVertical_ = contents["factorizeVertical"]->getBool();
    }
    return readSuccess;
  }

  // Return data
  double nudt() const { return nudt_; }
  double omega2() const { return omega2_; }
  double lambda2() const { return lambda2_; }
  double Nbuoyancy() const { return Nbuoyancy_; }
  std::string filename() const { return filename_; }
  bool factorizeVertical() const { return factorizeVertical_; }

 private:
  double nudt_;
  double omega2_;
  double lambda2_;
  double Nbuoyancy_;
  std::string filename_;
  bool factorizeVertical_;
};

#endif // PROFILEPARAMETERS_HH
