/*** COPYRIGHT STATEMENT ************************************ 
 *
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2013]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  This code has been developed by
 *  - Andreas Dedner (University of Warwick)
 *  - Eike Mueller (University of Bath)
 *
 *** COPYRIGHT STATEMENT ************************************/
/* *************************************************************** *
 *  Generate macro grid for spherical shell and save to
 *  .dgf file
 * *************************************************************** */

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include<iostream>
#include<vector>
#include<dune/common/mpihelper.hh>
#include<dune/common/exceptions.hh>
#include<dune/common/fvector.hh>

#include <dune/grid/uggrid.hh>
#include <dune/grid/common/scsgmapper.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>
#include "dgfwritercubeprism.hh"

#include "macrogridgenerator.hh"

const int dim = 3;
const int dimworld = 3;

/* *************************************************************** *
 * P0 Layout class is needed for attaching data to grid with
 * mapper.
 * *************************************************************** */
template < int dim >
struct P0Layout
{
  bool contains ( Dune::GeometryType gt )
  {
   if ( gt.dim ()== dim ) return true ;
     return false ;
  }
};

/* *************************************************************** *
 * Main program
 * *************************************************************** */
int main(int argc, char** argv)
{
  try{
    //Maybe initialize Mpi
    Dune::MPIHelper& helper = Dune::MPIHelper::instance(argc, argv);
    std::cout<< "macrogridgenerator" << std::endl;
    if (helper.size() > 1) {
      std::cout<< "ERROR: code needs to run sequentially." <<std::endl;
      exit(0);
    }
    
    if (argc != 4) {
      std::cout << "Usage: " << argv[0] << "<filename> <type> <refcount>" <<std::endl;
      std::cout << "   Produce macro grid with <refcount> refinements" 
                << std::endl 
                << "   and store .dgf file in <filename>." 
                << std::endl
                << "   <type> can be either \'cubedsphere\' or \'icosahedral\'."
                << std::endl;
      exit(0);
    }

    // Read command line parameters
    std::string filename = argv[1];
    std::string gridtype = argv[2];
    int refcount = atoi(argv[3]);
    if ( ! ( (gridtype == "cubedsphere") || 
             (gridtype == "icosahedral") ) ) {
      std::cout << "ERROR: Grid type has to be \'cubedsphere\' or \'icosahedral\'." << std::endl;
      exit(0);
    }
    std::cout << "Refining grid " << refcount << " times." << std::endl;
    std::cout << "Saving grid to " << filename << std::endl; 

    // Create grid
    double radius = 1.0;
    double thickness = 0.1;
    typedef MacrogridGenerator::Grid Grid;
    MacrogridGenerator* gridgenerator;
    if (gridtype == "cubedsphere") {
      gridgenerator= new CubedSphereMacrogridGenerator(radius,thickness,refcount);
    }
    if (gridtype == "icosahedral") {
      gridgenerator = new IcosahedralMacrogridGenerator(radius,thickness,refcount);
    }
    Grid& grid = *(gridgenerator->getgridPtr());
    typedef Grid::LeafGridView GV ;
    typedef Grid::ctype Coord;
    GV gv = grid.leafView();
    typedef GV::Codim<0>::Iterator ElementLeafIterator; 
    typedef ElementLeafIterator::Entity::Geometry LeafGeometry;

    // Loop over grid and attach user data
    Dune::LeafMultipleCodimMultipleGeomTypeMapper <Grid, P0Layout> mapper(grid);
    std::vector<double> c(mapper.size());
    gv = grid.leafView();
    ElementLeafIterator ibegin = gv.begin<0>();
    ElementLeafIterator iend = gv.end<0>();
    srand(1);
    for (ElementLeafIterator it = ibegin;it!=iend;++it) {
      const LeafGeometry geo = it->geometry();
      Dune::FieldVector<Coord,dimworld> global = geo.center();
      c[mapper.map(*it)] = (rand() % mapper.size());
    }

    std::cout << " Grid has " << gv.size(0) << " elements." << std::endl;

    // Write to vtu file elementdata.vtu for visualisation
    Dune::VTKWriter<GV> vtkwriter(gv);
    vtkwriter.addCellData(c,"data");
    vtkwriter.write("elementdata",Dune::VTK::ascii);

    // Write finest level to .dgf file
    Dune::DGFWriterCubePrism<GV> dgfwriter(gv);
    std::ofstream gridout(filename.c_str());
    dgfwriter.write(gridout);
    gridout.close();
  }
  catch (Dune::Exception &e){
    std::cerr << "Dune reported error: " << e << std::endl;
  }
  catch (...){
    std::cerr << "Unknown exception thrown!" << std::endl;
  }
}
