/*** COPYRIGHT STATEMENT ************************************ 
 *
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2013]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  This code has been developed by
 *  - Andreas Dedner (University of Warwick)
 *  - Eike Mueller (University of Bath)
 *
 *** COPYRIGHT STATEMENT ************************************/

#ifndef MODELOPERATOR_HH
#define MODELOPERATOR_HH

#include "dune/istl/operators.hh"
#include "gridparameters.hh"
#include "gridfunction.hh"
#include "operator.hh"
#include "profiles.hh"

/* ********************************************************** *
 * ********************************************************** *
 *
 *  Classes for describing geometry. These two classes
 *  define the geometry dependent coefficients v and S which
 *  are needed in the construction on the operator.
 *
 *  v(k) is the volume of the cell between the levels k+1
 *       and k for which the area of the horizontal surface
 *       is 1 when it is projected back to the reference
 *       surface.
 *
 *  S(k) is the factor by which the area of a unit surface
 *       element on the reference surface needs to be
 *       stretched to obtain the area of the corresponding
 *       surface element on level k.
 *
 * ********************************************************** *
 * ********************************************************** */
template <unsigned int NZ>
class GeometryDescr {
 public:
  // Constructor
  GeometryDescr(const Dune::FieldVector<double,NZ+1> &r) :
    r_(r) {}
 protected:
  Dune::FieldVector<double,NZ+1> r_;
};

/* *** Cartesian geometry *** */
template <unsigned int NZ>
class CartesianGeometryDescr : public GeometryDescr<NZ> {
  using GeometryDescr<NZ>::r_;
 public:
  // Constructor
  CartesianGeometryDescr(const Dune::FieldVector<double,NZ+1> &r) :
    GeometryDescr<NZ>(r) {}
  double v(int k) const {
    return r_[k+1]-r_[k];
  }
  // Surface stretch factor = 1
  double S(int k) const { return 1.0; }
};

/* *** Spherical geometry *** */
template <unsigned int NZ>
class SphericalGeometryDescr : public GeometryDescr<NZ> {
  using GeometryDescr<NZ>::r_;
 public:
  // Constructor
  SphericalGeometryDescr(const Dune::FieldVector<double,NZ+1> &r) :
    GeometryDescr<NZ>(r), ThreeInv(1./3.) {}
  double v(int k) const {
    return (r_[k+1]*r_[k+1]*r_[k+1]-r_[k]*r_[k]*r_[k])*ThreeInv;
  }
  // Surface stretch factor = r^2
  double S(int k) const { return r_[k]*r_[k]; }
 private:
  const double ThreeInv;
};

/* ********************************************************** *
 * ********************************************************** *
 *
 * Class describing the parameters of the smoother
 *
 * ********************************************************** *
 * ********************************************************** */
class SmootherParameters : public Parameters {
 public:
  enum Ordering {OrderingLex, OrderingRB}; // Ordering in smoother
  enum Direction {DirectionForward, DirectionBackward}; // Direction in smoother
  enum Type {TypeSOR, TypeSSOR, TypeJac}; // Smoother type
  // Constructor
  SmootherParameters() :
                  Parameters("smoother"),
                  ordering_(OrderingLex),
                  direction_(DirectionForward),
                  type_(TypeSOR)
    {
      addKey("type",String);
      addKey("direction",String);
      addKey("ordering",String);
    }

  //
  int readFile(const std::string filename) {
    int readSuccess = Parameters::readFile(filename);
    if (!readSuccess) {
      std::string directionStr = contents["direction"]->getString();
      std::string orderingStr = contents["ordering"]->getString();
      std::string typeStr = contents["type"]->getString();

      if (directionStr == "forward") {
        direction_ = DirectionForward;
      } else if (directionStr == "backward") {
        direction_ = DirectionBackward;
      } else {
        std::cerr << "Unknown smoother direction : \'" << directionStr << "\'" << std::endl;
      }

      if (orderingStr == "LEX") {
        ordering_ = OrderingLex;
      } else if (orderingStr == "RB") {
        ordering_ = OrderingRB;
      } else {
        std::cerr << "Unknown smoother ordering : \'" << orderingStr << "\'" << std::endl;
      }

      if (typeStr == "SOR") {
        type_ = TypeSOR;
      } else if (typeStr == "SSOR") {
        type_ = TypeSSOR;
      } else if (typeStr == "Jacobi") {
        type_ = TypeJac;
      } else {
        std::cerr << "Unknown smoother type : \'" << typeStr << "\'" << std::endl;
      }
      if ( type_ == TypeSSOR ) {
        std::cerr << "SSOR smoothing not allowed in spherical geometries." << std::endl;
        exit(1);
      }
      if ( ordering_ == OrderingRB ) {
        std::cerr << "RB smoothing not allowed in spherical geometries." << std::endl;
        exit(1);
      }
      if ( direction_ == DirectionBackward ) {
        std::cerr << "Backward smoothing not allowed in spherical geometries." << std::endl;
        exit(1);
      }
    }
    return readSuccess;

  }

  // Return data
  Direction direction() const { return direction_; }
  Ordering ordering() const { return ordering_; }
  Type type() const { return type_; }

 private:
  Ordering ordering_;
  Direction direction_;
  Type type_;
};

/* ********************************************************** *
 * ********************************************************** *
 *
 *  Model operator
 *
 * ********************************************************** *
 * ********************************************************** */
template <class df, class GD, class DerivedOperator, class Profile>
class ModelOperator : public Operator<df>,
                      public Dune::LinearOperator<df,df>
{
  public:
  typedef Profile ProfileType;
  const static bool COMPUTE = false;
  typedef df DF;
  typedef typename DF::Grid Grid;
  typedef typename DF::GridView GridView;
  typedef Operator<df> Base;
  typedef typename DF::field_type field_type;
  static const unsigned int dim=DF::dim;
  static const unsigned int NZ = DF::nz;  // Number of cells in the
                                 // vertical direction
  enum {category=Dune::SolverCategory::sequential};
/* ********************************************************** *
 * Create operator from a given problem description
 * ********************************************************** */
  ModelOperator(const Grid &grid,
                const GridParameters<NZ> &grid_param,
                const SmootherParameters &smoother_param,
                const DerivedOperator &derivedoperator,
                const Profile& profile) :
    grid_(grid),
    geoDescr_(grid_param.r()),
    r_(grid_param.r()),
    derivedoperator(derivedoperator),
    profile_(profile),
    uNb_(0),
    ordering_(smoother_param.ordering()),
    direction_(smoother_param.direction()),
    type_(smoother_param.type()),
    uzero(0.0)
  {
  }

  virtual ~ModelOperator() {}

/* ********************************************************** *
 * Return profile and grid
 * ********************************************************** */
  Profile profile() { return profile_; } const
  Grid& grid() { return grid_; } const

/* ********************************************************** *
 * Return and set the smoother direction and ordering
 * ********************************************************** */
  SmootherParameters::Ordering ordering() const { return ordering_; }
  void setordering(const SmootherParameters::Ordering ordering) {
    ordering_ = ordering;
  }
  SmootherParameters::Direction direction() const { return direction_; }
  void setdirection(const SmootherParameters::Direction direction) {
    direction_ = direction;
  }

/* ********************************************************** *
 * Solve 'exactly' by applying smoother several times
 * ********************************************************** */
  virtual void solve(const DF &b,DF &u) const
  {
    for (size_t i=0;i<Base::solveSteps_;++i)
      solveApprox(b,u);
  }

/* ********************************************************** *
 * Solve approximately by applying smoother once
 * ********************************************************** */
  virtual void solveApprox(const DF &b, DF &u) const
  {
    if (type_ == SmootherParameters::TypeSSOR) {
      if (direction_ == SmootherParameters::DirectionForward) {
        smooth(b,u,SmootherParameters::DirectionForward);
        smooth(b,u,SmootherParameters::DirectionBackward);
     } else {
        smooth(b,u,SmootherParameters::DirectionBackward);
        smooth(b,u,SmootherParameters::DirectionForward);
      }
    } else {
      smooth(b,u,direction_);
    }
  }

/* ********************************************************** *
 * Apply operator to a field x, i.e. calculate y = op(x)
 * ********************************************************** */
  virtual void apply(const DF &x,DF &y) const
  {
    typename DF::Iterator it = x.begin();
    const typename DF::Iterator endIt = x.end();
    for (;it!=endIt;++it)
    {
      const typename DF::Entity &en = *it;
      // Extract fields on nearest neighbours etc.
      setup(x,en);
      // apply off-diagonal part of operator
      y[en] = applyOffDiag();
      // apply diagonal part of operator
      y[en] += applyDiag(x[en]);
    }
    y.haloexchange();
  }

/* ********************************************************** *
 * Apply scaled add operator
 * ********************************************************** */
  virtual void applyscaleadd(field_type alpha, const DF &x,DF &y) const {
    DF y0(y.gridView(),y.level());
    typename DF::Iterator it = x.begin();
    const typename DF::Iterator endIt = x.end();
    for (;it!=endIt;++it)
    {
      const typename DF::Entity &en = *it;
      // Extract fields on nearest neighbours etc.
      setup(x,en);
      // apply off-diagonal part of operator
      y[en] = applyOffDiag();
      // apply diagonal part of operator
      y[en] += applyDiag(x[en]);
      y[en] *= alpha;
      y[en] += y0[en];
    }
    y.haloexchange();
  }

/* ********************************************************** *
 * Project continuous function onto grid function by
 * evaluating the function at the centre of the cell.
 * ********************************************************** */
  template <class F>
  void project(const F &f, DF &u) const
  {
    typename DF::Iterator it = u.begin();
    const typename DF::Iterator endIt = u.end();
    for (;it!=endIt;++it)
    {
      const typename DF::Entity &en = *it;
      typename DF::Geometry::GlobalCoordinate center = en.geometry().center();
      for (size_t i=0;i<NZ;++i)
      {
        typename DF::Geometry::GlobalCoordinate
          scaled_center = center;
          scaled_center /= scaled_center.two_norm();
          scaled_center *= 0.5*(r_[i]+r_[i+1]);
        u[en][i] = f(scaled_center);
      }
    }
    u.haloexchange();
  }

/* ********************************************************** *
 * Convert function stored as average value in a 3d cell to
 * function stored as integral over cell.
 * ********************************************************** */
  void convert_avg2int(DF &u) const
  {
    typename DF::Iterator it = u.begin();
    const typename DF::Iterator endIt = u.end();
    for (;it!=endIt;++it)
    {
      const typename DF::Entity &en = *it;
      for (size_t k=0;k<NZ;++k)
      {
        u[en][k] *= en.geometry().volume()/u.AreaScale()*geoDescr_.v(k);
      }
    }
    u.haloexchange();
  }

/* ********************************************************** *
 * Convert function stored as integral over a 3d cell to
 * function stored as the average in a cell.
 * ********************************************************** */
  void convert_int2avg(DF &u) const
  {
    typename DF::Iterator it = u.begin();
    const typename DF::Iterator endIt = u.end();
    for (;it!=endIt;++it)
    {
      const typename DF::Entity &en = *it;
      for (size_t k=0;k<NZ;++k)
      {
        u[en][k] /= (en.geometry().volume()/u.AreaScale()*geoDescr_.v(k));
      }
    }
    u.haloexchange();
  }

/* ********************************************************** *
 * Calculate L2 norm of grid function
 * ********************************************************** */
  double norm(const DF& u) const
  {
    double vol;
    double nrm = 0.0;
    typename DF::Iterator it = u.begin();
    const typename DF::Iterator endIt = u.end();
    for (;it!=endIt;++it)
    {
      const typename DF::Entity &en = *it;
      vol = (en.geometry().volume()/u.AreaScale());
      const typename DF::LocalVector tmp=u[en];
      for (size_t k=0;k<NZ;++k)
      {
        nrm += vol*geoDescr_.v(k) * (tmp[k]*tmp[k]);
      }
    }
    double globalnrm = u.comm().sum(nrm);
    return sqrt(globalnrm);
  }

  protected:
/* ********************************************************** *
 * Apply diagonal (i.e. vertical) couplings
 * ********************************************************** */
  Dune::FieldVector<double,NZ> applyDiag(const Dune::FieldVector<double,NZ> &u) const {
    Dune::FieldVector<double,NZ> res;
    for (size_t k=0;k<NZ;k++) {
      derivedoperator.mainCoeffs(k);
      res[k] = a_tilde*u[k];
      if (k>0) res[k] += c_tilde*u[k-1];
      if (k<NZ-1) res[k] += b_tilde*u[k+1];
    }
    return res;
  }

/* ********************************************************** *
 * Apply off-diagonal (i.e. horizontal) coupling
 * ********************************************************** */
  Dune::FieldVector<double,NZ> applyOffDiag() const
  {
    Dune::FieldVector<double,NZ> v(0.0);
    for (size_t i=0;i<nofNb;i++) {
      for (size_t k=0;k<NZ;k++) {
        derivedoperator.offDiagCoeff(k,i);
        v[k] += d_tilde*(*uNb_[i])[k];
      }
    }
    return v;
  }

/* ********************************************************** *
 * Invert tridiagonal matrix using the Thomas algorithm
 *
 *  The NZ x NZ matrix to be inverted has the following form:
 *
 *
 *  [ a b            ]
 *  [ c a b          ]
 *  [    c a b       ]
 *  [      c a b     ]
 *  [           ...  ]
 *
 *  where the coefficients a,b,c (i.e. the vertical couplings)
 *  are calculated on each vertical level (i.e. for each row
 *  of the tridiagonal matrix) with the mainCoeffs(k) method.
 *
 * ********************************************************** */
  void solveDiag(const Dune::FieldVector<double,NZ> &rhs,
                 Dune::FieldVector<double,NZ> &u) const {
    Dune::FieldVector<double,NZ> cc;
    // Forward iteration
    derivedoperator.mainCoeffs(0);
    double m = 1./a_tilde;
    cc[0] = b_tilde*m;
    u[0] = rhs[0]*m;
    for (size_t k=1; k<NZ;k++) {
      derivedoperator.mainCoeffs(k);
      m = 1./(a_tilde-cc[k-1]*c_tilde);
      cc[k] = b_tilde*m;
      u[k] = (rhs[k]-u[k-1]*c_tilde)*m;
    }
    // Backsubstitution
    for (int k=NZ-2;k>=0;k--)
      u[k] -= cc[k]*u[k+1];
  }

/* ********************************************************** *
 * Gauss-Seidel smoother once, for each element i on the
 * reference grid update the data in the vectical column
 * according to
 *
 * u_i <- T^{-1} (b_i - \sum_{j!=i} A_{ij}u_j)
 *
 * ********************************************************** */
  void smooth(const DF &b, DF &u,
              const SmootherParameters::Direction dir) const
  {
    const typename DF::Iterator endIt = u.end();
    const typename DF::Iterator begIt = u.begin();
    if (ordering_ == SmootherParameters::OrderingLex ) {
      if (dir == SmootherParameters::DirectionForward) {
        if (type_ == SmootherParameters::TypeJac) {
         // Forward lexicographic Jacobi
          DF u0(u.gridView(),u.level());
          u0.dofs() = u.dofs();
          for (typename DF::Iterator it = begIt;it!=endIt;++it)
          {
            const typename DF::Entity &en = *it;
            setup(u0,en);
            typename DF::LocalVector res = applyOffDiag();
            res = b[en] - res;
            solveDiag(res,u[en]);
          }
          double rho=2./3.;
          u.dofs() *= rho;
          u0.dofs() *= (1.0-rho);
          u.dofs() += u0.dofs();
        } else {
         // Forward lexicographic SOR
          for (typename DF::Iterator it = begIt;it!=endIt;++it)
          {
            const typename DF::Entity &en = *it;
            setup(u,en);
            typename DF::LocalVector res = applyOffDiag();
            res = b[en] - res;
            solveDiag(res,u[en]);
          }
        }
      } else {
        std::cerr << " Backward iteration only allowed for RB ordering." << std::endl;
        assert( 0 );
        exit(1);
      }
      u.haloexchange();
    } else {
    std::cerr << " RB ordering only allowed on cartesian grid." << std::endl;
    exit(1);
    }
  }

/* ********************************************************** *
 * Set up one element of the horizontal reference grid.
 *
 * - The fields on the neighbouring are stored in the vector
 *   uNb
 * - The vector alpha stores the part of the horizontal
 *   couplings which depends on the geometrical relationship
 *   between the element and its neighbours
 * - salpha is the sum over all alpha's
 * - volume_ is the volume (i.e. area) of the horizontal
 *   reference element.
 * ********************************************************** */
  template <class Entity>
  void setup(const DF &u, const Entity &en) const
  {
    derivedoperator.setup( u.AreaScale(), en );
    const typename DF::IntersectionIterator isend = u.gridView().iend(en);
    nofNb=0;
    for (typename DF::IntersectionIterator is = u.gridView().ibegin(en);
         is!=isend; ++is)
    {
      const typename DF::Intersection &inter = *is;
      if (inter.neighbor())
      {
        const typename DF::EntityPointer nbPtr = inter.outside();
        const typename DF::Entity &nb = *nbPtr;
        if (nofNb < uNb_.size()) {
          uNb_[nofNb] = &u[nb];
        } else {
          uNb_.push_back(&u[nb]);
        }

        derivedoperator.setup( u.EdgeScale(), en, nb, inter );
        nofNb++;
      }
      else
      {
#ifndef UGGRID
        abort();
#endif
      }
    }

  }

/* ********************************************************** *
 * Class data
 * ********************************************************** */
  const Grid &grid_;
  const GD geoDescr_; // geometry description
  const Dune::FieldVector<double,NZ+1> r_; // Vertical levels
  const DerivedOperator &derivedoperator;
  const Profile &profile_;

  // 2d Element specific data (temporary storage)
  // Vertical fields on neighbours of a specific horizontal element
  mutable std::vector<const typename DF::LocalVector*> uNb_;
  mutable size_t nofNb; // Number of neighbours
  mutable double a_tilde, b_tilde, c_tilde, d_tilde;
  mutable SmootherParameters::Ordering ordering_;    // Smoother ordering (Lex or RB)
  mutable SmootherParameters::Direction direction_;  // Smoother direction (Forward or Backward)
  mutable SmootherParameters::Type type_;            // Smoother type (SOR or SSOR)
  typename DF::LocalVector uzero;
};

/* ********************************************************** *
 * Unfactorised model operator
 * ********************************************************** */
template <class df, class GD, class Profile>
class UnfactorizedModelOperator : public ModelOperator< df,GD,UnfactorizedModelOperator<df,GD,Profile>,Profile>
{
  typedef ModelOperator< df,GD,UnfactorizedModelOperator<df,GD,Profile>,Profile> Base;
  public:
  static const bool COMPUTE = Base::COMPUTE;
  typedef df DFType;
  typedef typename DFType::Grid Grid;
  typedef typename DFType::GridView GridView;
  typedef typename DFType::field_type field_type;
  static const unsigned int dim=DFType::dim;
  static const unsigned int NZ = DFType::nz;  // Number of cells in the
                                 // vertical direction
  enum {category=Dune::SolverCategory::sequential};

  typedef DF<df::nz,GridView,1> EdgeFunction;
  typedef DF<3*(df::nz+1),GridView,0> AreaFunction;
  static std::vector< EdgeFunction* > edgeCoeffs_;
  static std::vector< AreaFunction* > areaCoeffs_;

/* ********************************************************** *
 * Create operator from a given problem description
 * ********************************************************** */
  UnfactorizedModelOperator(const Grid &grid,
                            const GridParameters<NZ> &grid_param,
                            const SmootherParameters &smoother_param,
                            const Profile &profile)
  : Base(grid,grid_param,smoother_param,*this,profile),
    alpha_hh_TT(0)
  {
    if (edgeCoeffs_.size() <= grid.maxLevel())
    {
      if (grid.comm().rank() == 0) {
        std::cout << "INIT EDGECOEFFS [Unfactorized] ..." << std::endl;
      }
      edgeCoeffs_.resize( grid.maxLevel()+1 );
      areaCoeffs_.resize( grid.maxLevel()+1 );
      for (int l=0;l<=grid.maxLevel();++l)
      {
        const GridView gv = grid.levelView(grid.maxLevel()-l);
        edgeCoeffs_[l] = new EdgeFunction(gv,l);
        areaCoeffs_[l] = new AreaFunction(gv,l);
        typename AreaFunction::Iterator it =
          gv.template begin<0,Dune::Interior_Partition>();
        const typename AreaFunction::Iterator endIt =
          gv.template end<0,Dune::Interior_Partition>();
        double Rmin,Rmax,AreaScale,EdgeScale;
        AreaFunction::calculateScalingFactors(*it,Rmin,Rmax,AreaScale,EdgeScale);
        // Initialise edge function to zero
        for (;it!=endIt;++it)
        {
          const typename AreaFunction::Entity &en = *it;
          const typename EdgeFunction::IntersectionIterator isend = gv.iend(en);
          for (typename EdgeFunction::IntersectionIterator is = gv.ibegin(en);
               is!=isend; ++is)
          {
            const typename EdgeFunction::Intersection &inter = *is;
            if (inter.neighbor())
            {
              int index = inter.indexInInside();
              for (size_t k=0;k<NZ;++k) {
                (*edgeCoeffs_[l])(en,index)[k] = 0.0;
              }
            }
          }
        }
        // Set edge function
        it = gv.template begin<0,Dune::Interior_Partition>();
        for (;it!=endIt;++it)
        {
          const typename AreaFunction::Entity &en = *it;
          setup(AreaScale,en,true);
          for (size_t k=0;k<NZ;++k) {
            (*areaCoeffs_[l])[en][3*k+0] = beta_T[k];
            (*areaCoeffs_[l])[en][3*k+1] = alpha_rr_T[k];
            (*areaCoeffs_[l])[en][3*k+2] = alpha_r_T[k];
          }
          (*areaCoeffs_[l])[en][3*NZ+1] = alpha_rr_T[NZ];
          (*areaCoeffs_[l])[en][3*NZ+2] = alpha_r_T[NZ];

          const typename EdgeFunction::IntersectionIterator isend = gv.iend(en);
          nofNb=0;
          for (typename EdgeFunction::IntersectionIterator is = gv.ibegin(en);
               is!=isend; ++is)
          {
            const typename EdgeFunction::Intersection &inter = *is;
            if (inter.neighbor())
            {
              const typename EdgeFunction::EntityPointer nbPtr = inter.outside();
              const typename EdgeFunction::Entity &nb = *nbPtr;
              setup( EdgeScale, en, nb, inter,true );
              int index = inter.indexInInside();
              for (size_t k=0;k<NZ;++k)
                (*edgeCoeffs_[l]) (en,index)[k] += 0.5*alpha_hh_TT[nofNb][k];
              nofNb++;
            }
          }
        }
      }
    }
  }

/* ********************************************************** *
 * Calculate the coefficients a,b,c describing the vertical
 * coupling and the diagonal entry of the stencil the cells
 * at a given vertical level k.
 * ********************************************************** */
  void mainCoeffs(int k) const
  {
    b_tilde = -(alpha_rr_T[k+1]+alpha_r_T[k+1]);
    c_tilde = -(alpha_rr_T[k]-alpha_r_T[k]);
    a_tilde = beta_T[k] + alpha_hh_T[k] - (b_tilde + c_tilde);
  }

/* ********************************************************** *
 * Calculate the part of the horizontal coupling which is
 * independent of the horizontal geometry.
 * ********************************************************** */
  void offDiagCoeff(int k, int i) const
  {
    d_tilde = -alpha_hh_TT[i][k];
  }

/* ********************************************************** *
 * Setup all data on an element
 * ********************************************************** */
  template <class Entity>
  void setup(const double AreaScale, const Entity &en,
             bool compute=COMPUTE) const
  {
    if (compute)
    {
      const typename Entity::Geometry &geo = en.geometry();
      volume_ = geo.volume()/AreaScale;
      centerEn = geo.center();
      for (size_t k=0;k<NZ;++k) {
        double r_center = 0.5*(r_[k+1]+r_[k]);
        if ( k > 0 ) {
          alpha_rr_T[k] = 2.*volume_*geoDescr_.S(k)/(r_[k+1]-r_[k-1])
                        * profile_.a_rr(en,centerEn,r_[k]);
          alpha_r_T[k] = volume_*geoDescr_.S(k)
                        * (r_[k+1]-r_[k])/(r_[k+1]-r_[k-1])
                        * profile_.a_r(en,centerEn,r_[k]);
        } else {
          alpha_rr_T[k] = 0.0;
          alpha_r_T[k] = 0.0;
        }
        beta_T[k] = volume_*geoDescr_.v(k)*profile_.b(en,centerEn,r_center);
        alpha_hh_T[k] = 0.0;
      }
      alpha_rr_T[NZ] = 0.0;
      alpha_r_T[NZ] = 0.0;
    }
    else
    {
      int l = grid_.maxLevel()-en.level();
      const typename AreaFunction::LocalVector tmp = (*areaCoeffs_[l])[en];
      for (size_t k=0;k<NZ;++k) {
        beta_T[k] = tmp[3*k+0];
        alpha_rr_T[k] = tmp[3*k+1];
        alpha_r_T[k] = tmp[3*k+2];
        alpha_hh_T[k] = 0.0;
      }
      alpha_rr_T[NZ] = tmp[3*NZ+1];
      alpha_r_T[NZ] = tmp[3*NZ+2];
    }
  }

/* ********************************************************** *
 * Setup all data on an edge
 * ********************************************************** */
  template <class Entity,class Intersection>
  void setup(const double EdgeScale, const Entity &en, const Entity &nb, const Intersection &is,
             bool compute=COMPUTE) const
  {
    if (nofNb == alpha_hh_TT.size()) {
      typename DFType::LocalVector val;
      alpha_hh_TT.push_back(val);
    }
    if (compute)
    {
      typename Entity::Geometry::GlobalCoordinate
          distance = nb.geometry().center();
      distance -= centerEn;
      typename Entity::Geometry::GlobalCoordinate
          normal = is.centerUnitOuterNormal();

      double edgedivdist = (is.geometry().volume()/EdgeScale)
                         / distance.two_norm2();
      for (size_t k=0;k<NZ;++k) {
        double r_center = 0.5*(r_[k+1]+r_[k]);
        double val = normal*distance;
        val *= (r_[k+1]-r_[k]);
        val *= edgedivdist;
        val *= profile_.a_hh(en,is.geometry().center(),r_center);
        alpha_hh_T[k] += val;
        alpha_hh_TT[nofNb][k] = val;
      }
    }
    else
    {
      int l = grid_.maxLevel()-en.level();
      int index = is.indexInInside();
      const typename EdgeFunction::LocalVector tmp = (*edgeCoeffs_[l]) (en,index);
      for (size_t k=0;k<NZ;++k)
      {
        double val = tmp[k];
        alpha_hh_TT[nofNb][k] = val;
        alpha_hh_T[k] += val;
      }
    }
  }

  private:

  mutable typename DFType::LocalVector alpha_hh_T;
  mutable std::vector<typename DFType::LocalVector> alpha_hh_TT;
  mutable typename Dune::FieldVector<double,NZ+1> alpha_rr_T,alpha_r_T;
  mutable typename DFType::LocalVector beta_T;
  mutable double volume_;
  mutable typename DFType::Geometry::GlobalCoordinate centerEn;

  using Base::grid_;
  using Base::a_tilde;
  using Base::b_tilde;
  using Base::c_tilde;
  using Base::d_tilde;
  using Base::nofNb;
  using Base::geoDescr_;
  using Base::r_;
  using Base::profile_;
};

/* ********************************************************** *
 * Factorised model operator
 * ********************************************************** */
template <class df, class GD, class Profile>
class FactorizedModelOperator : public ModelOperator< df,GD,FactorizedModelOperator<df,GD,Profile>, Profile>
{
  typedef ModelOperator< df,GD,FactorizedModelOperator<df,GD,Profile>,Profile> Base;
  public:
  static const bool COMPUTE = Base::COMPUTE;
  typedef df DFType;
  typedef typename DFType::Grid Grid;
  typedef typename DFType::GridView GridView;
  typedef typename DFType::field_type field_type;
  static const unsigned int dim=DFType::dim;
  static const unsigned int NZ = DFType::nz;  // Number of cells in the
                                 // vertical direction

  typedef DF<1,GridView,1> EdgeFunctionHoriz;
  typedef DF<3,GridView,0> AreaFunctionHoriz;
  static std::vector< EdgeFunctionHoriz* > edgeCoeffsHoriz_;
  static std::vector< AreaFunctionHoriz* > areaCoeffsHoriz_;

  typedef DF<(df::nz+1),GridView,0> AreaFunction;
  static std::vector< AreaFunction* > areaCoeffs_;

  enum {category=Dune::SolverCategory::sequential};
/* ********************************************************** *
 * Create operator from a given problem description
 * ********************************************************** */
  FactorizedModelOperator(const Grid &grid,
                          const GridParameters<NZ> &grid_param,
                          const SmootherParameters &smoother_param,
                          const Profile &profile)
  : Base(grid,grid_param,smoother_param,*this,profile),
    alpha(0)
  {
    factorizeVertical_ = profile_.profile_param().factorizeVertical();
    // precompute vertical discretisation vectors
    // a,b,c and d
    precompute_vertical_discretisation();
    if (edgeCoeffsHoriz_.size() <= grid.maxLevel())
    {
      if (grid.comm().rank() == 0) {
        std::cout << "INIT EDGECOEFFS [Factorized] ..." << std::endl;
      }
      edgeCoeffsHoriz_.resize( grid.maxLevel()+1 );
      areaCoeffsHoriz_.resize( grid.maxLevel()+1 );
      areaCoeffs_.resize( grid.maxLevel()+1 );
      for (int l=0;l<=grid.maxLevel();++l)
      {
        const GridView gv = grid.levelView(grid.maxLevel()-l);
        edgeCoeffsHoriz_[l] = new EdgeFunctionHoriz(gv,l);
        areaCoeffsHoriz_[l] = new AreaFunctionHoriz(gv,l);
        areaCoeffs_[l] = new AreaFunction(gv,l);
        typename AreaFunctionHoriz::Iterator it =
          gv.template begin<0,Dune::Interior_Partition>();
        const typename AreaFunction::Iterator endIt =
          gv.template end<0,Dune::Interior_Partition>();
        double Rmin,Rmax,AreaScale,EdgeScale;
        AreaFunctionHoriz::calculateScalingFactors(*it,Rmin,Rmax,AreaScale,EdgeScale);
        // Initialise edge functions to zero
        for (;it!=endIt;++it)
        {
          const typename AreaFunctionHoriz::Entity &en = *it;
          const typename EdgeFunctionHoriz::IntersectionIterator isend = gv.iend(en);
          for (typename EdgeFunctionHoriz::IntersectionIterator is = gv.ibegin(en);
               is!=isend; ++is)
          {
            const typename EdgeFunctionHoriz::Intersection &inter = *is;
            if (inter.neighbor())
            {
              int index = inter.indexInInside();
              (*edgeCoeffsHoriz_[l]) (en,index) = 0.0;
            }
          }
        }
        it = gv.template begin<0,Dune::Interior_Partition>();
        for (;it!=endIt;++it)
        {
          const typename AreaFunctionHoriz::Entity &en = *it;
          setup(AreaScale,en,true);
          (*areaCoeffsHoriz_[l])[en][0] = b_h_;
          (*areaCoeffsHoriz_[l])[en][1] = ar_h_;
          (*areaCoeffsHoriz_[l])[en][2] = arr_h_;
          // Unfactorised vertical derivative
          for (size_t k=0;k<=NZ;++k) {
            (*areaCoeffs_[l])[en][k] = alpha_rr_T[k];
          }

          const typename EdgeFunctionHoriz::IntersectionIterator isend = gv.iend(en);
          nofNb=0;
          for (typename EdgeFunctionHoriz::IntersectionIterator is = gv.ibegin(en);
               is!=isend; ++is)
          {
            const typename EdgeFunctionHoriz::Intersection &inter = *is;
            if (inter.neighbor())
            {
              const typename EdgeFunctionHoriz::EntityPointer nbPtr = inter.outside();
              const typename EdgeFunctionHoriz::Entity &nb = *nbPtr;
              setup( EdgeScale, en, nb, inter,true );
              int index = inter.indexInInside();
              (*edgeCoeffsHoriz_[l]) (en,index) += 0.5*alpha[nofNb];
              nofNb++;
            }
          }
        }
      }
    }
  }

/* ********************************************************** *
 * Precompute vertical discretisation
 * ********************************************************** */
  void precompute_vertical_discretisation()
  {
    for (size_t k=0;k<NZ;++k)
    {
      double rc = 0.5*(r_[k+1]+r_[k]);
      a[k] = geoDescr_.v(k) * profile_.b_r( rc );
      d[k] = (r_[k+1] - r_[k]) * profile_.a_hh_r( rc );
      if (k>0) {
        b[k] = 2.*geoDescr_.S(k)/(r_[k+1]-r_[k-1]) *
                profile_.a_rr_r( r_[k] );
        c[k] = geoDescr_.S(k)*(r_[k+1]-r_[k])/(r_[k+1]-r_[k-1]) *
                profile_.a_r_r( r_[k] );
      } else {
        b[k] = 0.0;
        c[k] = 0.0;
      }
    }
    b[NZ] = 0.0;
    c[NZ] = 0.0;
  }

/* ********************************************************** *
 * Calculate the coefficients a,b,c describing the vertical
 * coupling and the diagonal entry of the stencil the cells
 * at a given vertical level k.
 * ********************************************************** */
  void mainCoeffs(int k) const
  {
    if (factorizeVertical_) {
      b_tilde = -(b[k+1]*arr_h_+c[k+1]*ar_h_);
      c_tilde = -(b[k]*arr_h_-c[k]*ar_h_);
    } else {
      b_tilde = -(alpha_rr_T[k+1]+c[k+1]*ar_h_);
      c_tilde = -(alpha_rr_T[k]-c[k]*ar_h_);
    }
    a_tilde = a[k]*b_h_ + d[k]*salpha - (b_tilde+c_tilde);
  }

/* ********************************************************** *
 * Calculate the part of the horizontal coupling which is
 * independent of the horizontal geometry.
 * ********************************************************** */
  void offDiagCoeff(int k, int i) const
  {
    d_tilde = -alpha[i]*d[k];
  }

/* ********************************************************** *
 * Setup all data on an element
 * ********************************************************** */
  template <class Entity>
  void setup(const double AreaScale, const Entity &en,
             bool compute=COMPUTE) const
  {
    if (compute)
    {
      const typename Entity::Geometry &geo = en.geometry();
      volume_ = geo.volume()/AreaScale;
      // Horizontal- only part of tensor-product operator
      centerEn = geo.center();
      b_h_ = volume_*profile_.b_h(en,centerEn);
      ar_h_  = volume_*profile_.a_r_h(en,centerEn);
      if (factorizeVertical_) {
        arr_h_ = volume_*profile_.a_rr_h(en,centerEn);
      } else {
        // Unfactorised contribution from vertical derivative
        for (size_t k=0;k<NZ;++k) {
          double r_center = 0.5*(r_[k+1]+r_[k]);
          if ( k > 0 ) {
            alpha_rr_T[k] = 2.*volume_*geoDescr_.S(k)/(r_[k+1]-r_[k-1])
                          * profile_.a_rr(en,centerEn,r_[k]);
          } else {
            alpha_rr_T[k] = 0.0;
          }
        }
        alpha_rr_T[NZ] = 0.0;
      }
    }
    else
    {
      // Horizontal- only part of tensor-product operator
      int l = grid_.maxLevel()-en.level();
      const typename AreaFunctionHoriz::LocalVector tmp2d=(*areaCoeffsHoriz_[l])[en];
      b_h_   = tmp2d[0];
      ar_h_  = tmp2d[1];
      if (factorizeVertical_) {
        arr_h_ = tmp2d[2];
      } else {
      // Unfactorised contribution from vertical derivative
        const typename AreaFunction::LocalVector tmp=(*areaCoeffs_[l])[en];
        for (size_t k=0;k<=NZ;++k) {
          alpha_rr_T[k] = tmp[k];
        }
      }
    }
    salpha = 0;
  }

/* ********************************************************** *
 * Setup all data on an edge
 * ********************************************************** */
  template <class Entity,class Intersection>
  void setup(const double EdgeScale, const Entity &en, const Entity &nb,
             const Intersection &is,bool compute=COMPUTE) const
  {
    double val;
    if (compute)
    {
      typename Entity::Geometry::GlobalCoordinate
          distance = nb.geometry().center();
      distance -= centerEn;
      typename Entity::Geometry::GlobalCoordinate
          normal = is.centerUnitOuterNormal();
      val = distance*normal;
      val *= profile_.a_hh_h(en,is.geometry().center());
      val *= (is.geometry().volume()/EdgeScale) / distance.two_norm2();
    }
    else
    {
      int l = grid_.maxLevel()-en.level();
      int index = is.indexInInside();
      assert( l < edgeCoeffsHoriz_.size() );
      assert( edgeCoeffsHoriz_[l] );
      val = (*edgeCoeffsHoriz_[l]) (en,index);
    }

    if (nofNb < alpha.size())
      alpha[nofNb] = val;
    else
      alpha.push_back(val);
    salpha += alpha[nofNb];
  }

  private:

  Dune::FieldVector<double,NZ>   a,d;
  Dune::FieldVector<double,NZ+1> c,b;
  mutable std::vector<double> alpha;
  mutable double salpha;
  mutable double volume_;
  mutable typename DFType::Geometry::GlobalCoordinate centerEn;
  mutable double b_h_, ar_h_, arr_h_;

  mutable typename Dune::FieldVector<double,NZ+1> alpha_rr_T;

  bool factorizeVertical_;

  using Base::grid_;
  using Base::a_tilde;
  using Base::b_tilde;
  using Base::c_tilde;
  using Base::d_tilde;
  using Base::nofNb;
  using Base::geoDescr_;
  using Base::r_;
  using Base::profile_;
};

// *** Static class variables ***

template <class df, class GD, class Profile>
std::vector< DF<df::nz,typename df::GridView,1>* > UnfactorizedModelOperator<df,GD,Profile>::edgeCoeffs_(0);
template <class df, class GD, class Profile>
std::vector< DF<3*(df::nz+1),typename df::GridView,0>* > UnfactorizedModelOperator<df,GD,Profile>::areaCoeffs_(0);

template <class df, class GD, class Profile>
std::vector< DF<1,typename df::GridView,1>* > FactorizedModelOperator<df,GD,Profile>::edgeCoeffsHoriz_(0);
template <class df, class GD, class Profile>
std::vector< DF<3,typename df::GridView,0>* > FactorizedModelOperator<df,GD,Profile>::areaCoeffsHoriz_(0);
template <class df, class GD, class Profile>
std::vector< DF<(df::nz+1),typename df::GridView,0>* > FactorizedModelOperator<df,GD,Profile>::areaCoeffs_(0);

#endif
