/*** COPYRIGHT STATEMENT ************************************ 
 *
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2013]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  This code has been developed by
 *  - Andreas Dedner (University of Warwick)
 *  - Eike Mueller (University of Bath)
 *
 *** COPYRIGHT STATEMENT ************************************/

#ifndef MACROGRIDGENERATOR_HH
#define MACROGRIDGENERATOR_HH MACROGRIDGENERATOR_HH
/* *************************************************************** *
 *
 * Construct macro grids
 *
 * *************************************************************** */

#include<iostream>
#include<vector>
#include<assert.h>
#include<dune/common/fvector.hh>

#include <dune/grid/common/boundarysegment.hh>
#include <dune/grid/common/gridfactory.hh>
#include <dune/grid/uggrid.hh>
#include "sphericalboundarysegment.hh"

/* *************************************************************** *
 * Abstract base class for generating macro grids
 * *************************************************************** */
class MacrogridGenerator {

  public:
    // Grid and world dimension
    static const unsigned int dim = 3;
    static const unsigned int dimworld = 3;

  // UG grid type
    typedef Dune::UGGrid<dim> Grid;

  // Constructor
    MacrogridGenerator(const double radius,
                       const double thickness,
                       const int refcount,
                       const Dune::GeometryType::BasicType basictype,
                       Dune::UG_NS<dim>::RefinementRule refinementrule,
                       const unsigned int nVertices=0,
                       const unsigned int nElements=0,
                       const unsigned int nVerticesPerElement=0) :
                         radius_(radius),
                         thickness_(thickness),
                         refcount_(refcount),
                         geometrytype(basictype,dim),
                         refinementrule_(refinementrule),
                         nVertices_(nVertices),
                         nElements_(nElements),
                         nVerticesPerElement_(nVerticesPerElement) {
      Rsphere_.push_back(radius_-0.5*thickness_);
      Rsphere_.push_back(radius_+0.5*thickness_);
    }

    // Return reference to grid
    Grid* getgridPtr() const { return gridPtr; }

  protected:

    // create grid
    virtual void createGrid() {
      assert(nVertices_ == vertices.size());
      assert(nElements_ == elements.size());
      Dune::GridFactory<Grid> factory;
      /* *** Insert vertices *** */
      // Loop over inner / outer shell
      for (int ishell=0;ishell<2;++ishell) {
        // Loop over vertices of 2d grid
        for (int i=0;i<nVertices_;++i) {
          Dune::FieldVector<double,dim> vertex;
          for (int j=0;j<dim;++j) vertex[j] = vertices[i][j];
          vertex *= Rsphere_[ishell];
          factory.insertVertex(vertex);
        }
      }
      /* *** Insert elements *** */
      for (int i=0;i<nElements_;++i) {
        std::vector<unsigned int> elvertices(2*nVerticesPerElement_);
        for (int ishell=0;ishell<2;++ishell) {
          for (int j=0;j<nVerticesPerElement_;++j) elvertices[nVerticesPerElement_*ishell+j] = elements[i][j]+nVertices_*ishell;
        }
        factory.insertElement(geometrytype,elvertices);
      }
      /* *** Insert boundary segments *** */
      // Loop over inner / outer shell
      for (int ishell=0;ishell<2;++ishell) {
        for (int i=0;i<nElements_;++i) {
          std::vector<unsigned int> element(nVerticesPerElement_);
          std::vector<Dune::FieldVector<double,dim> > x(nVerticesPerElement_);
          for (int j=0;j<nVerticesPerElement_;++j) {
            int iVertex = elements[i][j];
            element[j] = iVertex+ishell*nVertices_;
            for (int k=0;k<dim;++k) x[j][k] = vertices[iVertex][k];
          }
          std::shared_ptr<Dune::BoundarySegment<dim,dimworld> >
            ptrSeg(new SphericalBoundarySegment(x,Rsphere_[ishell]));
          factory.insertBoundarySegment(element,ptrSeg);
        }
      }
      /* *** Create grid *** */
      gridPtr = factory.createGrid();
      Grid& grid = *gridPtr;
      /* *** Refine grid *** */
      typedef Grid::LeafGridView GV ;
      GV gv = grid.leafView();
      typedef GV::Codim<0>::Iterator ElementIterator;
      ElementIterator ibegin = gv.begin<0>();
      ElementIterator iend = gv.end<0>();
      for (int iref =0;iref<refcount_;++iref) {
        for (ElementIterator it = ibegin;it!=iend;++it) {
          grid.mark(*it,refinementrule_,0);
        }
        grid.adapt();
        grid.postAdapt();
      }
    }

    // Number of refinement steps
    int refcount_;
    // Radius and thickness of spherical shell,
    // the shell is given by r \in [radius-thickness/2,radius+thickness/2]
    double radius_;
    double thickness_;
    // Radius of inner/outer shell
    std::vector<double> Rsphere_;
    // Pointer to grid
    Grid* gridPtr;
    // Coarsest grid description:
    // Number of vertices of the 2d grid
    unsigned int nVertices_;
    // Number of elements on the 2d grid
    unsigned int nElements_;
    // Geometry type of elements
    Dune::GeometryType geometrytype;
    // Refinement rule
    Dune::UG_NS<dim>::RefinementRule refinementrule_;
    // Number of vertices per boundary element (4 for quadrilaterals)
    int nVerticesPerElement_;
    // Vertices
    std::vector<Dune::FieldVector<double,dim> > vertices;
    // Elements of 2d grid
    std::vector<std::vector<unsigned int> > elements;
};

/* *************************************************************** *
 * Macro grid generator for cubed sphere grid
 * *************************************************************** */
class CubedSphereMacrogridGenerator : public MacrogridGenerator {

  // Constructor
  public:
    CubedSphereMacrogridGenerator(const double radius,
                                  const double thickness,
                                  const int refcount
                                 ) :
        MacrogridGenerator(radius,
                           thickness,
                           refcount,
                           Dune::GeometryType::cube, // Geometry type
                           UG::D3::HEX_QUADSECT_0,   // Refinement rule
                           8, // # Vertices
                           6, // # Elements
                           4  // # Vertices per element
                          ) {
      // Vertices of 2d grid
      double invsqrt3 = 1./sqrt(3.0);
      Dune::FieldVector<double,dim> x;
      x[0] = -invsqrt3; x[1] = -invsqrt3; x[2] = -invsqrt3;
      vertices.push_back(x);
      x[0] =  invsqrt3; x[1] = -invsqrt3; x[2] = -invsqrt3;
      vertices.push_back(x);
      x[0] = -invsqrt3; x[1] =  invsqrt3; x[2] = -invsqrt3;
      vertices.push_back(x);
      x[0] =  invsqrt3; x[1] =  invsqrt3; x[2] = -invsqrt3;
      vertices.push_back(x);
      x[0] = -invsqrt3; x[1] = -invsqrt3; x[2] =  invsqrt3;
      vertices.push_back(x);
      x[0] =  invsqrt3; x[1] = -invsqrt3; x[2] =  invsqrt3;
      vertices.push_back(x);
      x[0] = -invsqrt3; x[1] =  invsqrt3; x[2] =  invsqrt3;
      vertices.push_back(x);
      x[0] =  invsqrt3; x[1] =  invsqrt3; x[2] =  invsqrt3;
      vertices.push_back(x);
      // Elements of 2d grid
      std::vector<unsigned int> elem(nVerticesPerElement_);
      elem[0] = 5; elem[1] = 1; elem[2] = 7; elem[3] = 3;
      elements.push_back(elem);
      elem[0] = 2; elem[1] = 6; elem[2] = 3; elem[3] = 7;
      elements.push_back(elem);
      elem[0] = 0; elem[1] = 4; elem[2] = 2; elem[3] = 6;
      elements.push_back(elem);
      elem[0] = 0; elem[1] = 1; elem[2] = 4; elem[3] = 5;
      elements.push_back(elem);
      elem[0] = 4; elem[1] = 5; elem[2] = 6; elem[3] = 7;
      elements.push_back(elem);
      elem[0] = 1; elem[1] = 0; elem[2] = 3; elem[3] = 2;
      elements.push_back(elem);
      // Create grid
      createGrid();
    }
};

/* *************************************************************** *
 * Macro grid generator for cubed sphere grid
 * *************************************************************** */
class IcosahedralMacrogridGenerator : public MacrogridGenerator {

  // Constructor
  public:
    IcosahedralMacrogridGenerator(const double radius,
                                  const double thickness,
                                  const int refcount
                                 ) :
        MacrogridGenerator(radius,
                           thickness,
                           refcount,
                           Dune::GeometryType::prism, // Geometry type
                           UG::D3::PRISM_QUADSECT,    // Refinement rule
                           12, // # Vertices
                           20, // # Elements
                           3   // # Vertices per element
                          ) {
      // Vertices of 2d grid
      double phi = 0.5*(1.+sqrt(5.));
      double Cscale = sqrt(2./(5.+sqrt(5.)));
      Dune::FieldVector<double,dim> x;
      x[0] =  phi; x[1] =  1.; x[2] = 0.; x *= Cscale;
      vertices.push_back(x);
      x[0] = -phi; x[1] =  1.; x[2] = 0.; x *= Cscale;
      vertices.push_back(x);
      x[0] =  phi; x[1] = -1.; x[2] = 0.; x *= Cscale;
      vertices.push_back(x);
      x[0] = -phi; x[1] = -1.; x[2] = 0.; x *= Cscale;
      vertices.push_back(x);
      x[0] =  1.; x[1] = 0.; x[2] =  phi; x *= Cscale;
      vertices.push_back(x);
      x[0] =  1.; x[1] = 0.; x[2] = -phi; x *= Cscale;
      vertices.push_back(x);
      x[0] = -1.; x[1] = 0.; x[2] =  phi; x *= Cscale;
      vertices.push_back(x);
      x[0] = -1.; x[1] = 0.; x[2] = -phi; x *= Cscale;
      vertices.push_back(x);
      x[0] = 0.; x[1] =  phi; x[2] =  1.; x *= Cscale;
      vertices.push_back(x);
      x[0] = 0.; x[1] = -phi; x[2] =  1.; x *= Cscale;
      vertices.push_back(x);
      x[0] = 0.; x[1] =  phi; x[2] = -1.; x *= Cscale;
      vertices.push_back(x);
      x[0] = 0.; x[1] = -phi; x[2] = -1.; x *= Cscale;
      vertices.push_back(x);
      // Elements of 2d grid
      /*
       * Connectivity structure:
       *
       *       9      9      9      9      9
       *     /  \   /  \   /  \   /  \   /  \
       *    /    \ /    \ /    \ /    \ /    \
       *   4----- 6----- 3-----11----- 2----- 4
       *    \    / \    / \    / \    / \    / \
       *     \  /   \  /   \  /   \  /   \  /   \
       *       8----- 1----- 7----- 5----- 0----- 8
       *        \   /  \   /  \   /  \   /  \   /
       *         \ /    \ /    \ /    \ /    \ /
       *         10     10     10     10     10
       */

      std::vector<unsigned int> elem(nVerticesPerElement_);
      elem[0] = 4; elem[1] = 6; elem[2] = 9;
      elements.push_back(elem);
      elem[0] = 6; elem[1] = 3; elem[2] = 9;
      elements.push_back(elem);
      elem[0] = 3; elem[1] = 11; elem[2] = 9;
      elements.push_back(elem);
      elem[0] = 11; elem[1] = 2; elem[2] = 9;
      elements.push_back(elem);
      elem[0] = 2; elem[1] = 4; elem[2] = 9;
      elements.push_back(elem);
      elem[0] = 4; elem[1] = 8; elem[2] = 6;
      elements.push_back(elem);
      elem[0] = 8; elem[1] = 1; elem[2] = 6;
      elements.push_back(elem);
      elem[0] = 6; elem[1] = 1; elem[2] = 3;
      elements.push_back(elem);
      elem[0] = 1; elem[1] = 7; elem[2] = 3;
      elements.push_back(elem);
      elem[0] = 3; elem[1] = 7; elem[2] = 11;
      elements.push_back(elem);
      elem[0] = 7; elem[1] = 5; elem[2] = 11;
      elements.push_back(elem);
      elem[0] = 11; elem[1] = 5; elem[2] = 2;
      elements.push_back(elem);
      elem[0] = 5; elem[1] = 0; elem[2] = 2;
      elements.push_back(elem);
      elem[0] = 2; elem[1] = 0; elem[2] = 4;
      elements.push_back(elem);
      elem[0] = 0; elem[1] = 8; elem[2] = 4;
      elements.push_back(elem);
      elem[0] = 8; elem[1] = 10; elem[2] = 1;
      elements.push_back(elem);
      elem[0] = 1; elem[1] = 10; elem[2] = 7;
      elements.push_back(elem);
      elem[0] = 7; elem[1] = 10; elem[2] = 5;
      elements.push_back(elem);
      elem[0] = 5; elem[1] = 10; elem[2] = 0;
      elements.push_back(elem);
      elem[0] = 0; elem[1] = 10; elem[2] = 8;
      elements.push_back(elem);
      // Create grid
      createGrid();
    }
};
#endif // MACROGRIDGENERATOR_HH

