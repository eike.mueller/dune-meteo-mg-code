/*** COPYRIGHT STATEMENT ************************************ 
 *
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2013]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  This code has been developed by
 *  - Andreas Dedner (University of Warwick)
 *  - Eike Mueller (University of Bath)
 *
 *** COPYRIGHT STATEMENT ************************************/

#ifndef SPHERICALBOUNDARYSEGMENT_HH
#define SPHERICALBOUNDARYSEGMENT_HH SPHERICALBOUNDARYSEGMENT_HH

#include<vector>
#include<assert.h>
#include<dune/common/fvector.hh>
#include <dune/grid/common/boundarysegment.hh>

/* *************************************************************** *
 * Boundary segment on a sphere of given radius Rsphere.
 * The segment is specified by three vectors x_0, x_1, x_2
 * and local coordinates (xi_1,xi_2) are mapped to global
 * coordinates r(xi_1,xi_2) on the sphere.
 *
 * *************************************************************** */
class SphericalBoundarySegment : public Dune::BoundarySegment<3,3> {
  public:
    static const int dim = 3;
    static const int dimworld = 3;
  // Constructor
  SphericalBoundarySegment(const std::vector<Dune::FieldVector<double,dim>> x,
                           const double Rsphere) :
    x_(x), Rsphere_(Rsphere) {
      nVertices = x_.size();
      assert((nVertices==3) || (nVertices==4));
    };
  // Destructor
  virtual ~SphericalBoundarySegment() {}
  // Operator()
  virtual Dune::FieldVector<double,dimworld> operator() (
    const Dune::FieldVector<double,dim-1> &local) const {
    Dune::FieldVector<double,dimworld> x_global(0.0);
    switch (nVertices) {
      case 3: {
        std::vector<Dune::FieldVector<double,dimworld> > x_s(3);
        for (int i=0;i<3;++i) x_s[i] = x_[i];
        x_global = x_s[0];
        for (int i=1;i<3;++i) {
          x_s[i] -= x_s[0];
          x_s[i] *= local[i-1];
          x_global += x_s[i];
        }
        // project back onto sphere
        x_global *= Rsphere_/x_global.two_norm();
      } break;
      case 4: {
        // Calculate the centre vector to determine which
        // face of the cubed sphere to project on
        Dune::FieldVector<double,dimworld> x_c(0.0);
        for (int i=0;i<4;++i) x_c += x_[i];
        x_c *= 0.25;
        // Work out which dimension to scale with
        int sdim = -1;
        if ( (fabs(x_c[0]) > fabs(x_c[1])) && (fabs(x_c[0]) > fabs(x_c[2])) ) {
          sdim = 0;
        }
        if ( (fabs(x_c[1]) > fabs(x_c[2])) && (fabs(x_c[1]) > fabs(x_c[0])) ) {
          sdim = 1;
        }
        if ( (fabs(x_c[2]) > fabs(x_c[0])) && (fabs(x_c[2]) > fabs(x_c[1])) ) {
          sdim = 2;
        }
        // Calculate
        // x_0/x_0[sdim] + xi_1*(x_1/x_1[sdim]) - x_0/x_0[sdim])
        //               + xi_2*(x_2/x_2[sdim]) - x_0/x_0[sdim])
        std::vector<Dune::FieldVector<double,dimworld> > x_s(4);
        for (int i=0;i<4;++i) {
          x_s[i] = x_[i];
          x_s[i] /= fabs(x_[i][sdim]);
        }
        x_global = x_s[0];
        for (int i=1;i<3;++i) {
          x_s[i] -= x_s[0];
          x_s[i] *= local[i-1];
          x_global += x_s[i];
        }
        // project back onto sphere
        x_global *= Rsphere_/x_global.two_norm();
      } break;
      default:
        break;
    }
    return x_global;
  }

  private:
    // Vector with edges of element (x_0, x_1, x_2)
    std::vector<Dune::FieldVector<double,dim> > x_;
    // Radius of sphere to project on
    double Rsphere_;
    // Number of vertices (4 or 3)
    int nVertices;
};

#endif // SPHERICALBOUNDARYSEGMENT_HH

