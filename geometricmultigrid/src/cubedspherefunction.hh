/*** COPYRIGHT STATEMENT ************************************ 
 *
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2013]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  This code has been developed by
 *  - Andreas Dedner (University of Warwick)
 *  - Eike Mueller (University of Bath)
 *
 *** COPYRIGHT STATEMENT ************************************/

/* *************************************************** *
 *
 *   Map from unit cube to 1/6th of cubed sphere, to
 *   be used with geometrygrid.
 *
 * *************************************************** */


#ifndef CUBEDSPHEREFUNCTION_HH
#define CUBEDSPHEREFUNCTION_HH CUBEDSPHEREFUNCTION_HH

#include<dune/grid/geometrygrid/coordfunction.hh>

/* *************************************************** *
 * Class for mapping from a unit square [0,1] to one
 * sixth of a spherical shell, using a cubed sphere
 * transformation.
 * *************************************************** */

class CubedSphereFunction
  : public Dune::AnalyticalCoordFunction<double,2,3,CubedSphereFunction> {
    typedef CubedSphereFunction This;
    typedef Dune::AnalyticalCoordFunction<double,2,3,This> Base;
  public:
    CubedSphereFunction(const double& Rsphere_=1.0
                       ) : Rsphere(Rsphere_) {};
    typedef Base::DomainVector DomainVector;
    typedef Base::RangeVector RangeVector;
    void evaluate( const DomainVector& x, RangeVector& y) const {
      double r = Rsphere;
      double theta = atan((2.*x[0]-1.)/sqrt(2.-4.*x[1]+4.*x[1]*x[1]));
      double phi = atan(2.*x[1]-1.);
      y[0] = r*sin(theta);
      y[1] = r*cos(theta)*sin(phi);
      y[2] = r*cos(theta)*cos(phi);
    }
  private:
    double Rsphere; // Radius of sphere
};

#endif
