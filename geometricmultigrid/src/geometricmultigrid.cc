/*** COPYRIGHT STATEMENT ************************************ 
 *
 *  (c) The copyright relating to this work is owned jointly 
 *  by the Crown, Met Office and NERC [2013]. However, it
 *  has been created with the help of the GungHo Consortium,
 *  whose members are identified at
 *  https://puma.nerc.ac.uk/trac/GungHo/wiki
 *  
 *  This code has been developed by
 *  - Andreas Dedner (University of Warwick)
 *  - Eike Mueller (University of Bath)
 *
 *** COPYRIGHT STATEMENT ************************************/
/* *************************************************************** *
 *
 *   Main program for parallel matrix free geometric multigrid 
 *   code.
 *
 * *************************************************************** */

#ifdef HAVE_CONFIG_H
# include "config.h"     
#endif
#include<iostream>
#include<fstream>
#include<vector>
#include<dune/common/mpihelper.hh> // An initializer of MPI
#include<dune/common/exceptions.hh> // We use exceptions
#include<dune/common/fvector.hh>
#include<dune/common/timer.hh>
#include<dune/common/mpicollectivecommunication.hh>
#include<dune/grid/yaspgrid.hh>
#include<dune/grid/alugrid.hh>
#include<dune/grid/io/file/dgfparser/dgfalu.hh>
#include<dune/grid/geometrygrid/grid.hh>
#include<dune/grid/geometrygrid/coordfunction.hh>

// #include <dune/grid/cacheitgrid/dgfparser.hh>

#include"rhs.hh"
#include"gridfunction.hh"     
#include"multigrid.hh"  
#include"profileparameters.hh"
#include"gridparameters.hh"
#include"generalparameters.hh"
#include"coarsegridsolverparameters.hh"
#include"iterativesolver.hh"
#include"modeloperator.hh"
#include"istloperator.hh"
#include"istloperator_cgjac.hh"
#include"cubedspherefunction.hh"
#if defined UGGRID
  #include"sphericalgridgenerator.hh"
#endif // UGGRID
#include"profiles.hh"
#include"netcdffunctions.hh"
const int nz=128;

#if defined UGGRID 
const int dim = 2;
const int dimworld = 3;
#else // defined UGGRID
const int dim = 2;
const int dimworld = 3;
#endif // defined UGGRID

/* *************************************************************** *
 * Measure time for preconditioner/operator
 * *************************************************************** */
template <class Grid,
          class MG,
          class Operator,
          class FRHS,
          class UMG,
          class Helper>
void applyoperators(Grid &grid,
                    UMG &umg,
                    MG &mg,
                    Operator &op,
                    FRHS &fRHS,
                    Helper &helper,
                    std::ofstream &logfile) {
  typedef HierarchicDF<nz,Grid> HDF;
  typedef typename HDF::LevelDF DF;
  typedef typename Grid::LevelGridView GV;
  const GV& gv = grid.levelView(grid.maxLevel());
  // Create hierachic grid functions
  DF b(gv,0);
  DF u(gv,0);
  u.dofs() = 0.0;
  DF res(gv,0);

  // Initialise RHS
  op.project(fRHS,b);
  op.convert_avg2int(b);

  const int nApplyUmg=20;
  const int nApplyMg=20;
  const int nPrecUmg=10;
  const int nPrecMg=10;
  const int nHaloExchange=1000;
  double nrm;

  if (helper.rank() == 0) {
    std::cout << "********************************" << std::endl;
    std::cout << " Operator application and preconditioner" << std::endl;
    std::cout << "********************************" << std::endl;
    std::cout << " [times given for ONE call, number in brackets is the number of measurements]" << std::endl;
  }

  Dune::CollectiveCommunication<MPI_Comm> comm(MPI_COMM_WORLD);

  Dune::Timer haloExchangeTimer(true);
  for (int i=0;i<nHaloExchange;++i)
    u.haloexchange();
  double haloExchangeTime = haloExchangeTimer.stop();
  haloExchangeTime = comm.max(haloExchangeTime)/nHaloExchange;
  if (helper.rank() == 0) {
    std::cout << std::fixed << std::setprecision(6)
              << " haloexchange = " << haloExchangeTime 
              << " s [" << nHaloExchange << "x ]" << std::endl;
  }

#ifndef FACTORISING_OPERATOR
  Dune::Timer applyUmgTimer(true);
  for (int i=0;i<nApplyUmg;++i)
    umg.apply(b,u);
  double applyUmgTime = applyUmgTimer.stop();
  applyUmgTime = comm.max(applyUmgTime)/nApplyUmg;
  if (helper.rank() == 0) {
    std::cout << std::fixed << std::setprecision(6)
              << " Apply<umg> = " << applyUmgTime
              << " s [" << nApplyUmg << "x ]" << std::endl;
  }
  nrm = mg.norm(u);
  if (helper.rank() == 0) {
    std::cout << std::scientific << std::setprecision(12) << "||u|| = "
              << nrm << std::endl;
  }
#endif // FACTORISING_OPERATOR

  Dune::Timer applyMgTimer(true);
  for (int i=0;i<nApplyMg;++i)
    mg.apply(b,u);
  double applyMgTime = applyMgTimer.stop();
  applyMgTime = comm.max(applyMgTime)/nApplyMg;
  if (helper.rank() == 0) {
    std::cout << std::fixed << std::setprecision(6)
              << " Apply<mg> = " << applyMgTime 
              << " s [" << nApplyMg << "x ]" << std::endl;
  }
  nrm = mg.norm(u);
  if (helper.rank() == 0) {
    std::cout << std::scientific << std::setprecision(12) << "||u|| = "
              << nrm << std::endl;
  }

#ifndef FACTORISING_OPERATOR
  Dune::Timer precUmgTimer(true);
  for (int i=0;i<nPrecUmg;++i)
    umg.solveApprox(b,u);
  double precUmgTime = precUmgTimer.stop();
  precUmgTime = comm.max(precUmgTime)/nPrecUmg;
  if (helper.rank() == 0) {
    std::cout << std::fixed << std::setprecision(6)
              << " Prec<umg> = " << precUmgTime 
              << " s [" << nPrecUmg << "x ]" << std::endl;
  }
  nrm = mg.norm(u);
  if (helper.rank() == 0) {
    std::cout << std::scientific << std::setprecision(12) << "||u|| = "
              << nrm << std::endl;
  }

#endif // FACTORISING_OPERATOR

  Dune::Timer precMgTimer(true);
  for (int i=0;i<nPrecMg;++i)
    mg.solveApprox(b,u);
  double precMgTime = precMgTimer.stop();
  precMgTime = comm.max(precMgTime)/nPrecMg;
  if (helper.rank() == 0) {
    std::cout << std::fixed << std::setprecision(6)
              << " Prec<mg> = " << precMgTime 
              << " s [" << nPrecMg << "x ]" << std::endl;
  }
  nrm = mg.norm(u);
  if (helper.rank() == 0) {
    std::cout << std::scientific << std::setprecision(12) << "||u|| = "
              << nrm << std::endl;
  }

  if (helper.rank() == 0) {
    logfile << std::scientific << std::setprecision(6)
            << haloExchangeTime << " # haloexchange" << std::endl;
#ifndef FACTORISING_OPERATOR
    logfile << std::scientific << std::setprecision(6)
            << applyUmgTime << " # apply<umg>" << std::endl;
#endif // FACTORISING_OPERATOR
    logfile << std::scientific << std::setprecision(6)
            << applyMgTime << " # apply<mg>" << std::endl;
#ifndef FACTORISING_OPERATOR
    logfile << std::scientific << std::setprecision(6)
            << precUmgTime << " # Vcycle<umg>" << std::endl;
#endif // FACTORISING_OPERATOR
    logfile << std::scientific << std::setprecision(6)
            << precMgTime << " # Vcycle<mg>" << std::endl;
  }
  if (helper.rank() == 0) {
    std::cout << std::endl;
  }
}

/* *************************************************************** *
 * Carry out one linear solve
 * *************************************************************** */
template <class Solver,
          class Grid, 
          class Op,
          class MG, 
          class UMG,
          class FRHS, 
          class Helper,class Param,class SParam>   
void solve(Grid &grid, 
           Op &oppresmooth,
           UMG &umg, MG &mg,
           FRHS &fRHS,
           Helper &helper,
           Param &general_param,
           SParam &solver_param,
           const std::string label,
           std::ofstream &logfile)
{
  typedef HierarchicDF<nz,Grid> HDF;
  typedef typename HDF::LevelDF DF;
  typedef typename Grid::LevelGridView GV;
  const GV& gv = grid.levelView(grid.maxLevel());
  // Create hierachic grid functions
  DF b(gv,0);
  DF u(gv,0);
  u.dofs() = 0.0;
  DF res(gv,0);
  if (helper.rank() == 0) {
    std::cout << "********************************" << std::endl;
    std::cout << "* " << label << std::endl;
    std::cout << "********************************" << std::endl;
  }

  // Initialise RHS
  oppresmooth.project(fRHS,b);

  if (general_param.savetovtk()) {
    b.savetovtk("rhs",general_param.vtkoutputtype());
  }

  // Convert RHS to integral over cells
  oppresmooth.convert_avg2int(b);

  Solver solver(helper,umg,mg,solver_param);
   
  solver.solve(b,u);
  SolveInfo solveinfo = solver.solveInfo();
  if (helper.rank()==0) {
    std::cout << " Number of iterations     = "
              << solveinfo.iterations_ << std::endl;
    std::cout << " Total solve time (max)   = "
              << std::fixed << std::setprecision(6)
              << solveinfo.tTotal_ << " s" << std::endl;
    std::cout << " Loop time (max)          = "
              << std::fixed << std::setprecision(6)
              << solveinfo.tLoop_ << " s" << std::endl;
    std::cout << " Time per iteration (max) = "
              << std::fixed << std::setprecision(6)
              << solveinfo.tIter_ << " s" << std::endl;
    std::cout << std::endl;
    logfile   << solveinfo.iterations_ << " "
              << std::scientific << std::setprecision(6)
              << solveinfo.tTotal_ << " "
              << std::scientific << std::setprecision(6)
              << solveinfo.tIter_
              << " # result " << label << std::endl;
  }

  // Calculate residual
  solver.residual(b,u,res);

  // Convert residual to avg. value in grid cells
  oppresmooth.convert_int2avg(res);

  if (general_param.savetovtk()) {
    u.savetovtk("solution",general_param.vtkoutputtype());
    res.savetovtk("residual",general_param.vtkoutputtype());
  }
}

/* *************************************************************** *
 * Main program
 * *************************************************************** */
int main(int argc, char** argv)
{
  try{
    //Maybe initialize Mpi
    Dune::MPIHelper& helper = Dune::MPIHelper::instance(argc, argv);
    if(Dune::MPIHelper::isFake)
      std::cout<< "Matrix-free geometric multigrid [sequential version]" << std::endl;
    else
      if (helper.rank() == 0) {
        std::cout<< "Matrix-free geometric multigrid running on "<< helper.size()<<" processes."<<std::endl;
      }

    if (argc != 2) {
      if (helper.rank() == 0) {
        std::cout << "Usage: " << argv[0] << " parametefile>" << std::endl;
      }
      return 0;
    }
    Dune::CollectiveCommunication<MPI_Comm> comm(MPI_COMM_WORLD);
    // Read command line parameters
    std::string parameterFilename = argv[1];

    // Read parameters from file
    GeneralParameters general_param;
    ProfileParameters profile_param;
    GridParameters<nz> grid_param;
    SolverParameters solver_param;
    CoarsegridSolverParameters coarsegridsolver_param;
    MultigridParameters multigrid_param;
    SmootherParameters smoother_param;

    if (general_param.readFile(parameterFilename)) return 1;
    if (profile_param.readFile(parameterFilename)) return 1;
    if (grid_param.readFile(parameterFilename)) return 1;
    if (solver_param.readFile(parameterFilename)) return 1;
    if (multigrid_param.readFile(parameterFilename)) return 1;
    if (smoother_param.readFile(parameterFilename)) return 1;
    if (coarsegridsolver_param.readFile(parameterFilename)) return 1;
    if (helper.rank() == 0) {
      std::cout << std::endl;
#ifdef RECONSTR_SINGLE
      std::cout << " Prologation: use single reconstruction on father element ";
  #ifdef RECONSTR_CONS
      std::cout << "(conservative).";
  #else // RECONSTR_CONS
      std::cout << "(non-conservative).";
  #endif // RECONSTR_CONS
  std::cout << std::endl;
#else
      std::cout << " Prologation: childwise reconstruction." << std::endl;
#endif // RECONSTR_SINGLE
#ifdef UGGRID
      std::cout << " Grid: UG" << std::endl;
#endif
      std::cout << std::endl;
      std::cout << "------------------ " << std::endl;
      std::cout << "--- Parameters --- " << std::endl;
      std::cout << "------------------ " << std::endl;
      std::cout << general_param << std::endl;
      std::cout << grid_param << std::endl;
      std::cout << profile_param << std::endl;
      std::cout << solver_param << std::endl;
      std::cout << multigrid_param << std::endl;
      std::cout << coarsegridsolver_param << std::endl;
      std::cout << smoother_param << std::endl;
    }
    // Construct grid
#if defined UGGRID 
    typedef Dune::UGGrid<dimworld> Grid;
    Grid::setDefaultHeapSize(grid_param.ugheapsize());
    SphericalGridGenerator gridgenerator(grid_param.gridFilename(),
                                         grid_param.refcount());
    Grid& grid = *(gridgenerator.getgridPtr());
#else // defined UGGRID

    typedef Dune::GridSelector::GridType HGrid;
    typedef HGrid Grid;
    Dune::GridPtr<HGrid> gridPtr(grid_param.gridFilename());
    HGrid& hgrid = *gridPtr;
    // Grid grid(hgrid);
    Grid &grid = hgrid;
#endif // defined UGGRID
#ifndef UGGRID
    grid.loadBalance();             
    grid.globalRefine(grid_param.refcount());
#endif
    if (helper.rank() == 0) {
      std::cout << " Number of grid cells on horizontal host grid = " 
                << grid.levelView(grid.maxLevel()).size(0) << std::endl;
      std::cout << " Number of vertical grid cells = " << nz << std::endl;
      std::cout << " Total number of grid cells = "
                << (nz*grid.levelView(grid.maxLevel()).size(0)) << std::endl;
    }
    
    typedef HierarchicDF<nz,Grid> HDF;
    typedef HDF::LevelDF DF;

    // Choose profile
#ifdef AQUAPLANET_PROFILE
    if (helper.rank() == 0) 
      std::cout << "Profile: Aquaplanet" << std::endl;
    typedef AtmosphericProfile Profile;
    typedef Profile UnfacProfile;
#endif // AQUAPLANET_PROFILE

#ifdef BALANCEDFLOW_PROFILE
    if (helper.rank() == 0) {
      std::cout << "Profile: Balanced flow" << std::endl;
    }
    typedef FactorisingZonalFlowProfile Profile;
    typedef ZonalFlowProfile UnfacProfile;
#endif // BALANCEDFLOW_PROFILE

#ifdef CONSTANT_PROFILE
    if (helper.rank() == 0) 
      std::cout << "Profile: Constant" << std::endl;
    typedef ConstantProfile Profile;
#ifndef FACTORISING_OPERATOR
    typedef ToUnfacProfile<Profile> UnfacProfile;
#else
    if (helper.rank() == 0) 
      std::cout << "Operator factorises" << std::endl;
    typedef Profile UnfacProfile;
#endif // FACTORISING_OPERATOR
#endif // CONSTANT_PROFILE

    Profile profile(profile_param);
    UnfacProfile uprofile(profile_param);

#ifdef BALANCEDFLOW_PROFILE
    if (helper.rank() == 0) {
      std::cout << "epsilon = " << uprofile.epsilon() << std::endl;
    }
#endif // BALANCEDFLOW_PROFILE
    
    // Construct model operator
    typedef SphericalGeometryDescr<nz> ModelGeometryDescr;
    typedef FactorizedModelOperator<DF,ModelGeometryDescr,Profile>
      ModelOp; 
    typedef FactorizedModelOperator<DF,ModelGeometryDescr,Profile>
      CoarseModelOp; 
    ModelOp oppresmooth(grid,grid_param,smoother_param,profile);
    oppresmooth.setdirection(SmootherParameters::DirectionForward);
    ModelOp oppostsmooth(grid,grid_param,smoother_param,profile);
    oppostsmooth.setdirection(SmootherParameters::DirectionForward);  
    CoarseModelOp coarseop(grid,grid_param,smoother_param,profile);
    coarseop.setsolveSteps(coarsegridsolver_param.niter());

#ifdef FACTORISING_OPERATOR
    typedef FactorizedModelOperator<DF,ModelGeometryDescr,Profile>
      UModelOp; 
    typedef FactorizedModelOperator<DF,ModelGeometryDescr,Profile>
      UCoarseModelOp;
#else
    typedef UnfactorizedModelOperator<DF,ModelGeometryDescr,UnfacProfile>
      UModelOp; 
    typedef UnfactorizedModelOperator<DF,ModelGeometryDescr,UnfacProfile>
      UCoarseModelOp;
#endif // FACTORISING_OPERATOR
    UModelOp uoppresmooth(grid,grid_param,smoother_param,uprofile);
    uoppresmooth.setdirection(SmootherParameters::DirectionForward);
    UModelOp uoppostsmooth(grid,grid_param,smoother_param,uprofile);
    uoppostsmooth.setdirection(SmootherParameters::DirectionForward);  
    UCoarseModelOp ucoarseop(grid,grid_param,smoother_param,uprofile);
    ucoarseop.setsolveSteps(coarsegridsolver_param.niter());

    // Solve using multigrid algorithm
    Multigrid<nz,Grid> mg( grid, multigrid_param, 
                           oppresmooth,
                           oppostsmooth,
                           coarseop);
    Multigrid<nz,Grid> umg( grid, multigrid_param, 
                            uoppresmooth,
                            uoppostsmooth,
                            ucoarseop);

    const FSpherical fRHS(grid_param.H_bottom(),grid_param.H());

    double runtime;
    int iter;
    std::ofstream logfile;
    if (helper.rank() == 0) {  
      logfile.open(general_param.timinglogfile());
      logfile << "### Apply operators and preconditioners ###" << std::endl;
    }
    applyoperators(grid,umg,mg,oppresmooth,fRHS,helper,logfile);
    if (helper.rank() == 0) {  
      logfile << std::endl;
      logfile << "### Solvers ###" << std::endl;
      logfile << "# output format: (iterations tSolve tIter)" << std::endl;
    }
#ifdef FACTORISING_OPERATOR
    solve< LoopSolver<DF> >( grid, oppresmooth,mg,mg,fRHS,helper,
        general_param,solver_param,
        "LoopSolver<mg,mg>",logfile);
    solve< PBiCGStabSolver<DF> >( grid, oppresmooth,mg,mg,fRHS,helper,
        general_param,solver_param,
        "PBiCGStabSolve<mg,mg>",logfile);
    solve< PCGSolver<DF> >( grid, oppresmooth,mg,mg,fRHS,helper,
        general_param,solver_param,
        "PCGSolve<mg,mg>",logfile);
#else
    solve< LoopSolver<DF> >( grid, oppresmooth,umg,mg,fRHS,helper,
        general_param,solver_param,
        "LoopSolver<umg,mg>",logfile);
    solve< LoopSolver<DF> >( grid, oppresmooth,umg,umg,fRHS,helper,
        general_param,solver_param,
        "LoopSolver<umg,umg>",logfile);
    solve< PBiCGStabSolver<DF> >( grid, oppresmooth,umg,mg,fRHS,helper,
        general_param,solver_param,
        "PBiCGStabSolve<umg,mg>",logfile);
    solve< PBiCGStabSolver<DF> >( grid, oppresmooth,umg,umg,fRHS,helper,
        general_param,solver_param,
        "PBiCGStabSolve<umg,umg>",logfile);
    solve< PCGSolver<DF> >( grid, oppresmooth,umg,mg,fRHS,helper,
        general_param,solver_param,
        "PCGSolve<umg,mg>",logfile);
    solve< PCGSolver<DF> >( grid, oppresmooth,umg,umg,fRHS,helper,
        general_param,solver_param,
        "PCGSolve<umg,umg>",logfile);
#endif // FACTORISING_OPERATOR
    if (helper.rank() == 0) {  
      logfile.close();
    }
  }
  catch (Dune::Exception &e){
    std::cerr << "Dune reported error: " << e << std::endl;
  }
  catch (...){
    std::cerr << "Unknown exception thrown!" << std::endl;
  }
}
